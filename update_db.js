//version compatibility: 2.0.0-SNAPSHOT

//function createOtherResearchProducts() {
//	db.entity.save({"pid" : "orp", "name" : "Other Research Products"});
//	orpId = db.entity.find( { pid: "orp" }).map( function(entity) { return entity._id.str; } ).toString();
//
//	db.page.save({"name" : "Other Research Products Landing", "route" : "/search/other", "type" : "landing", "connect":true,"openaire":true,"entities" : [orpId]});
//	db.page.save({"name" : "Search Other Research Products", "route" : "/search/find/other", "type" : "search", "connect":true,"openaire":true,"entities" : [orpId]});
//	db.page.save({"name" : "Advanced Search Other Research Products", "route" : "/search/advanced/other", "type" : "search", "connect":true,"openaire":true,"entities" : [orpId]});
//
//	orpLanding = db.page.find( { route: "/search/other" }).map( function(page) { return page._id.str; } ).toString();
//	searchOrp = db.page.find( { route: "/search/find/other" }).map( function(page) { return page._id.str; } ).toString();
//	advancedSearchOrp = db.page.find( { route: "/search/advanced/other" }).map( function(page) { return page._id.str; } ).toString();
//
//	communities = db.community.find().map( function(community) { return community; } );
//
//	for (var i = 0; i < communities.length; i++) {
//		community_pages = communities[i].pages;
//
//		community_pages[orpLanding] = true;
//		community_pages[searchOrp] = true;
//		community_pages[advancedSearchOrp] = true;
//
//		community_pid = communities[i].pid;
//		db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
//        print("Update pages for " + community_pid);
//
//		community_entities = communities[i].entities;
//
//		community_entities[orpId] = true;
//
//		community_pid = communities[i].pid;
//		db.community.update({ "pid" : community_pid },{$set: { "entities": community_entities}});
//        print("Update entities for " + community_pid);
//	}
//}

// function createNotificationsCollection() {
// 	db.createCollection("notifications");
// }
//
// function addDivHelpContentsForCommunity(communityPid) {
// 	link_context_form = db.divId.find( { name: "link-context-form" }).map( function(divId) { return divId._id.str; } ).toString();
//     link_project_form = db.divId.find( { name: "link-project-form" }).map( function(divId) { return divId._id.str; } ).toString();
//     link_result_form = db.divId.find( { name: "link-result-form" }).map( function(divId) { return divId._id.str; } ).toString();
//     link_result_bulk = db.divId.find( { name: "link-result-bulk" }).map( function(divId) { return divId._id.str; } ).toString();
//     link_metadata = db.divId.find( { name: "link-metadata" }).map( function(divId) { return divId._id.str; } ).toString();
//
//     link_context_form_content = '<div> <div><span class="uk-text-bold"><span uk-icon="icon: info">&nbsp;</span> Information:</span> Select a research community and/or a category and search for a community concept, or browse to the community tree through the categories</div> </div>';
//     link_project_form_content = '<div> <div><span class="uk-text-bold"><span uk-icon="icon: info">&nbsp;</span> Information:</span> Search for projects using project name or grant id. Limit results filtering by funder.</div> </div>';
//     link_result_form_content = '<div> <div><span class="uk-text-bold"><span uk-icon="icon: info">&nbsp;</span> Information:</span></div> Search for research results in OpenAIRE information space, Datacite, CrossRef or ORCID. <div class="uk-text-small">Use keywords, DOI (more than one - space separated), author&#39;s ORCID</div> </div> ';
//     link_result_bulk_content = '<div> <div><span class="uk-text-bold"><span uk-icon="icon: info">&nbsp;</span> Information:</span> Upload a csv file containing a list of DOIs. For each DOI found in the file, metadata will be fetched from CrossRef or Datacite and will be added to your selected research results.</div> <div class="uk-margin-top uk-text-small"><span class="uk-text-bold">CSV format:</span> <ul class="uk-list"> <li>The format of CSV file should be &quot;DOI&quot;,&quot;ACCESS_MODE&quot;,&quot;DATE&quot;.</li> <li>The value &quot;DOI&quot; is required</li> <li>Access mode column should have values: &quot;OPEN&quot;,&quot;CLOSED&quot; or &quot;EMBARGO&quot;.</li> <li>Date column valid format is YYYY-MM-DD and is required when access mode has value EMBARGO.</li> <li>In case access mode is not available default value is &quot;OPEN&quot;.</li> </ul> </div> </div> ';
//     link_metadata_content = '<div> <div><span class="uk-text-bold"><span uk-icon="icon: info">&nbsp;</span> Information:</span> Manage access mode &amp; type of selected research results. For OpenAIRE this functionality isn&#39;t available.</div> </div>';
//
//     communityID = db.community.find({ "pid" : communityPid}).map( function(community) { return community._id.str.toString(); } );
//
//     db.divHelpContent.save({ "divId" : link_context_form, "community" : communityID, "content" : link_context_form_content, "isActive" : false });
//     db.divHelpContent.save({ "divId" : link_project_form, "community" : communityID, "content" : link_project_form_content, "isActive" : false });
//     db.divHelpContent.save({ "divId" : link_result_form, "community" : communityID, "content" : link_result_form_content, "isActive" : true });
//     db.divHelpContent.save({ "divId" : link_result_bulk, "community" : communityID, "content" : link_result_bulk_content, "isActive" : true });
//     db.divHelpContent.save({ "divId" : link_metadata, "community" : communityID, "content" : link_metadata_content, "isActive" : false });
// }
//
// function addDefaultHtmlToHtmlPagesForCommunity(communityPid) {
// 	about = db.page.find( { route: "/about" }).map( function(page) { return page._id.str; } ).toString()
// 	organizations = db.page.find( { route: "/organizations" }).map( function(page) { return page._id.str; } ).toString()
//
//     communityID = db.community.find({ "pid" : communityPid}).map( function(community) { return community._id.str.toString(); } );
//
// 	db.htmlPageContent.save({"page" : about, "community" : communityID, "content" : '<div><div class="uk-article-title custom-article-title"> About the community </div> <p> This is an introductory text. To be updated... </p> </div>'})
// 	db.htmlPageContent.save({"page" : organizations, "community" : communityID, "content" : '<div><div class="uk-article-title custom-article-title"> Organizations related to the community </div> <p> This is an introductory text. Here follows the list of organizations... </p> <div class="uk-child-width-1-3@m uk-text-center uk-grid-match " uk-grid > <div class="uk-card uk-card-default uk-margin-bottom uk-padding-remove"> <div class="uk-card-media-top"> <img src="https://upload.wikimedia.org/wikipedia/el/2/2b/Logo_uoa_blue.png" alt="" class="uk-height-small uk-responsive-height "> </div> <div class="uk-card-body"> <h3 class="uk-card-title"> <a class="wk-link-reset" href="https://www.uoa.gr/">University of Athens</a> </h3> </div> </div> <div class="uk-card uk-card-default uk-margin-bottom uk-padding-remove"> <div class="uk-card-media-top"> <img src="https://pbs.twimg.com/profile_images/631127495933165569/ElbqhHK0_400x400.jpg" alt="" class="uk-height-small uk-responsive-height "> </div> <div class="uk-card-body"> <h3 class="uk-card-title"> <a class="wk-link-reset" href="https://www.athena-innovation.gr/en">Athena Research & Innovation center</a> </h3> </div> </div> <div class="uk-card uk-card-default uk-margin-bottom uk-padding-remove"> <div class="uk-card-media-top"> <img src="" alt="Logo 1" class="uk-height-small uk-responsive-height "> </div> <div class="uk-card-body"> <h3 class="uk-card-title"> <a class="wk-link-reset" href="">Organization 1</a> </h3> </div> </div> <div class="uk-card uk-card-default uk-margin-bottom uk-padding-remove"> <div class="uk-card-media-top"> <img src="" alt="Logo 2" class="uk-height-small uk-responsive-height "> </div> <div class="uk-card-body"> <h3 class="uk-card-title"> <a class="wk-link-reset" href="">Organization 2</a> </h3> </div> </div> <div class="uk-card uk-card-default uk-margin-bottom uk-padding-remove"> <div class="uk-card-media-top"> <img src="" alt="Logo 3" class="uk-height-small uk-responsive-height "> </div> <div class="uk-card-body"> <h3 class="uk-card-title"> <a class="wk-link-reset" href="">Organization 3</a> </h3> </div> </div> </div></div>'})
// }
//
// function addORPInStatistics() {
//
//     communitiesStatistics = db.statistics.find().map( function(community) { return community; } );
//
// 	for (var i = 0; i < communitiesStatistics.length; i++) {
//
//         stats =communitiesStatistics[i]
//     print("stats " + stats.pid);
//         var numbers_map = {};
//         numbers_map["total"] = { "showInMonitor" : true, "showInDashboard" : false };
//         numbers_map["project"] = { "showInMonitor" : true, "showInDashboard" : false };
//         numbers_map["open"] = { "showInMonitor" : true, "showInDashboard" : false };
//         numbers_map["closed"] = { "showInMonitor" : true, "showInDashboard" : false };
//         numbers_map["embargo"] = { "showInMonitor" : true, "showInDashboard" : false };
//         numbers_map["restricted"] = { "showInMonitor" : true, "showInDashboard" : false };
//         var numbers = {"map":numbers_map};
//         var charts_map = {};
//         charts_map["timeline"] = { "showInMonitor" : true, "showInDashboard" : false };
//         charts_map["graph"] = { "showInMonitor" : true, "showInDashboard" : false };
//         charts_map["projectTable"] = { "showInMonitor" : true, "showInDashboard" : false };
//         charts_map["projectColumn"] = { "showInMonitor" : true, "showInDashboard" : false };
//         charts_map["projectPie"] = { "showInMonitor" : true, "showInDashboard" : false };
//         var charts = {"map":charts_map};
//
//         var statistics_entities = {"charts":charts,"numbers":numbers}
//         var orp ="orp";
//         stats.entities[orp]=statistics_entities;
//
//         db.statistics.update({ "pid" : stats.pid },{$set: { "entities": stats.entities}});
//         // db.statistics.update(stats);
//
//     }
// }
//
// function createShareInZenodoPage() {
// 	db.page.save({"name" : "Share in Zenodo", "route" : "/participate/share-zenodo", "type" : "share", "connect":true,"openaire":false,"entities" : []});
//     shareInZenodo = db.page.find( { route: "/participate/share-zenodo" }).map( function(page) { return page._id.str; } ).toString();
// 	communities = db.community.find().map( function(community) { return community; } );
//
// 	for (var i = 0; i < communities.length; i++) {
// 		community_pages = communities[i].pages;
//
// 		community_pages[shareInZenodo] = true;
//
// 		community_pid = communities[i].pid;
// 		db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
//         print("Update pages for " + community_pid);
//     }
// }
//
function addCommunityLayout() {
    //db.createCollection("layout");
    communities = db.community.find().map( function(community) { return community; } );
    for (var i = 0; i < communities.length; i++) {
        var layoutId = db.layout.insertOne({"color": "#EBB13E"}).insertedId.str;
        community_pid = communities[i].pid;
        db.community.update({ "pid" : community_pid }, {$set: {"layout": layoutId}});
        print("Creating layout for " + community_pid);
    }
}
//
// function addCuratorsPage() {
//     curatorsId = db.page.insertOne({"name" : "Curators", "route" : "/curators", "type" : "other", "connect":true,"openaire":false,"entities" : []}).insertedId.str;
//     print("Creating curators page with id " + curatorsId);
//     communities = db.community.find().map( function(community) { return community; } );
//     for (var i = 0; i < communities.length; i++) {
//         community_pages = communities[i].pages;
//
//         community_pages[curatorsId] = false;
//
//         community_pid = communities[i].pid;
//         db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
//         print("Add curators page with id " + curatorsId + " on community " + community_pid);
//     }
// }
//
// function removeMonitorPage() {
//     monitor = db.page.find( { route: "/monitor" }).map( function(page) { return page._id.str; } ).toString()
//     db.page.remove({"route" : "/monitor"});
//     print("Remove Monitor page with  id " + monitor);
//     communities = db.community.find().map( function(community) { return community; } );
//     for (var i = 0; i < communities.length; i++) {
//         community_pages = communities[i].pages;
//
//         delete community_pages[monitor];
//
//         community_pid = communities[i].pid;
//         db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
//         print("Remove monitor page with id " + monitor + " on community " + community_pid);
//     }
// }
//
// function pageOptionConnectAsCommunities() {
//   pages = db.page.find().map( function(page) { return page; } );
//   for (var i = 0; i < pages.length; i++) {
//     page = pages[i];
//
//     db.page.update(
//       { "_id" : page._id },
//       { $set: {"communities": page.connect, "connect": false}}
//     );
//   }
// }
//
// function organizationsPageTypeFromHtmlToOther() {
//   db.page.update(
//     { "route" : "/organizations" },
//     { $set: {"type": "other"}}
//   );
// }
//
//
// function removeHtmlPageContentsFromOrganizationsPage() {
//   organizations = db.page.find( { route: "/organizations" }).map( function(page) { return page._id.str; } ).toString();
//   db.htmlPageContent.remove({"page" : organizations});
// }
//
// function addOpenaireConnectCommunitiesOptionsInDivIds() {
//   db.divId.updateMany(
//     { },
//     { $set: {"connect":false,"communities":false,"openaire":true}}
//   );
// }
//
// function addHomeNewsAndOrganizationsDivIds() {
//   home = db.page.find( { route: "/" }).map( function(page) { return page._id.str; } ).toString();
//   organizations = db.page.find( { route: "/organizations" }).map( function(page) { return page._id.str; } ).toString()
//
//   db.divId.save({ "name" : "home-news", "pages" : [home], "connect":true,"communities":false,"openaire":true});
//   print("divId home-news saved");
//   db.divId.save({ "name" : "organizations", "pages" : [organizations], "connect":false,"communities":true,"openaire":false});
//   print("divId organizations saved");
//
//   organizations_class = db.divId.find( { name: "organizations" }).map( function(divId) { return divId._id.str; } ).toString();
//   organizations_class_content = '<div> <p>Here you can write more details about the organizations related to your community.</p> </div>';
//
//   communities = db.community.find().map( function(community) { return community; } );
//   for (var i = 0; i < communities.length; i++) {
//     community = communities[i];
//     if(community.pid != "openaire" && community.pid != "connect") {
//       //print(community.pid);
//       db.divHelpContent.save({
//         "divId": organizations_class,
//         "community": community._id.str,
//         "content": organizations_class_content,
//         "isActive": false
//       });
//     }
//   }
// }
//
// function removeLinkingDivHelpContentsForCommunitiesAndConnect() {
//   link_context_form = db.divId.find( { name: "link-context-form" }).map( function(divId) { return divId._id.str; } ).toString();
//   link_project_form = db.divId.find( { name: "link-project-form" }).map( function(divId) { return divId._id.str; } ).toString();
//   link_result_form = db.divId.find( { name: "link-result-form" }).map( function(divId) { return divId._id.str; } ).toString();
//   link_result_bulk = db.divId.find( { name: "link-result-bulk" }).map( function(divId) { return divId._id.str; } ).toString();
//   link_metadata = db.divId.find( { name: "link-metadata" }).map( function(divId) { return divId._id.str; } ).toString();
//
//   communities = db.community.find().map( function(community) { return community; } );
//   for (var i = 0; i < communities.length; i++) {
//     community = communities[i];
//     if(community.pid != "openaire") {
//       db.divHelpContent.remove({"divId" : link_context_form, "community" : community._id.str});
//       db.divHelpContent.remove({"divId" : link_project_form, "community" : community._id.str});
//       db.divHelpContent.remove({"divId" : link_result_form, "community" : community._id.str});
//       db.divHelpContent.remove({"divId" : link_result_bulk, "community" : community._id.str});
//       db.divHelpContent.remove({"divId" : link_metadata, "community" : community._id.str});
//     }
//   }
// }

function addPositionsInPages() {
  print("Add positions in Pages\n");

  // Other Pages
  db.page.update({ route: "/" },{ $set: {"top":true,"bottom":true,"right":false,"left":true}});
  print("Positions for 'Other Pages' updated");

  // Other Pages for Connect
  db.page.update({ route: "/about/learn-how" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/about/learn-in-depth" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/contact-us" },{ $set: {"top":false,"bottom":false,"right":true,"left":false}});
  db.page.update({ route: "/content" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});

  db.page.update({ route: "/curators" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/organizations" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route : "/invite" }, { $set: {"top":true,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Other Pages for Connect' updated");

  // Testing Page for help contents (Connect)
  db.page.update({ route : "/helper-test"}, { $set: {"top":true,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Testing Page for help contents (Connect)' updated");

  // Other Pages for Explore
  db.page.update({ route: "/mail-preferences" },{ $set: {"top":true,"bottom":true,"right":true,"left":true}});
  print("Positions for 'Other Pages for Explore' updated");

  // Landing Pages
  db.page.update({ route: "/search/publication" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/dataset" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/software" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/other" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/project" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/organization" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/dataprovider" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});

  db.page.update({ route: "/project-report" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Landing Pages' updated");

  // Search Pages
  db.page.update({ route: "/search/find" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});

  db.page.update({ route: "/search/find/communities" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});

  db.page.update({ route: "/search/find/publications" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/find/datasets" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/find/software" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/find/other" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/find/projects" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/find/organizations" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/find/dataproviders" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Search Pages' updated");

  // Advanced Search Pages
  db.page.update({ route: "/search/advanced/publications" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/advanced/datasets" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/advanced/software" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/advanced/other" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/advanced/projects" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/advanced/organizations" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/advanced/dataproviders" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Advanced Search Pages' updated");

  // Search Content Provider Pages
  db.page.update({ route: "/search/journals" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/entity-registries" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/content-providers" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/journals-table" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/entity-registries-table" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/search/content-providers-table" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Search Content Providers Pages' updated");

  // Deposit Pages
  db.page.update({ route: "/participate/deposit-publications" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit-datasets" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit-publications-result" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit-datasets-result" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit-subject-result" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit/zenodo" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit/learn-how" },{ $set: {"top":false,"bottom":true,"right":false,"left":false}});
  db.page.update({ route: "/participate/deposit-search" },{ $set: {"top":true,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Deposit Pages' updated");

  // Linking Pages
  db.page.update({ route: "/participate/claim" },{ $set: {"top":false,"bottom":true,"right":false,"left":false}});
  db.page.update({ route: "/participate/direct-claim" },{ $set: {"top":false,"bottom":true,"right":false,"left":false}});
  db.page.update({ route: "/myclaims" },{ $set: {"top":true,"bottom":true,"right":true,"left":false}});
  db.page.update({ route: "/claims" },{ $set: {"top":true,"bottom":true,"right":true,"left":false}});
  db.page.update({ route: "/claims-project-manager" },{ $set: {"top":false,"bottom":false,"right":false,"left":false}});
  print("Positions for 'Linking Pages' updated");
}

function addMissingAndNewPages() {
  print("Add missing and new Pages\n");

  db.page.save({"name" : "Search Communities", "route" : "/search/find/communities", "type" : "search", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});

  db.page.save({"name" : "Deposit your research - Learn How", "route" : "/participate/deposit/learn-how", "type" : "share", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});
  db.page.save({"name" : "Deposit Browse & Search repositories", "route" : "/participate/deposit/search", "type" : "share", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});

  db.page.save({"name" : "About - Learn in depth", "route" : "/about/learn-in-depth", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});
  db.page.save({"name" : "Contact us", "route" : "/contact-us", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});

  db.page.save({"name" : "Content Policy", "route" : "/content", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});

  db.page.save({"name" : "Invite", "route" : "/invite", "type" : "other", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":false,"left":false});

  db.page.save({"name" : "Mail Preferences", "route" : "/mail-preferences", "type" : "other", "connect":false,"communities":false,"openaire":true,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});

  // Testing Page for help contents (Connect)
  db.page.save({"name" : "Helper Test", "route" : "/helper-test", "type" : "other", "connect":true, "communities": false, "openaire": false, "entities": [], "top": true, "bottom": false, "left": false, "right": false});
}

function addNewRoutesInCommunities() {
  searchCommunities = db.page.find( { route: "/search/find/communities" }).map( function(page) { return page._id.str; } ).toString()

  depositLearnHow = db.page.find( { route: "/participate/deposit/learn-how" }).map( function(page) { return page._id.str; } ).toString();
  depositSearch = db.page.find( { route: "/participate/deposit/search" }).map( function(page) { return page._id.str; } ).toString();

  about = db.page.find( { route: "/about/learn-how" }).map( function(page) { return page._id.str; } ).toString()
  aboutLearnInDepth = db.page.find( { route: "/about/learn-in-depth" }).map( function(page) { return page._id.str; } ).toString()

  contactUs = db.page.find( { route: "/contact-us" }).map( function(page) { return page._id.str; } ).toString()
  contentPolicy = db.page.find( { route: "/content" }).map( function(page) { return page._id.str; } ).toString()
  invite = db.page.find( { route: "/invite" }).map( function(page) { return page._id.str; } ).toString()
  helperTest = db.page.find( { route: "/helper-test" }).map( function(page) { return page._id.str; } ).toString()
  mailPreferences = db.page.find( { route: "/mail-preferences" }).map( function(page) { return page._id.str; } ).toString()

  communities = db.community.find().map( function(community) { return community; } );

	for (var i = 0; i < communities.length; i++) {
		community_pages = communities[i].pages;

		community_pages[searchCommunities] = true;
		community_pages[depositLearnHow] = true;
		community_pages[depositSearch] = true;
    community_pages[about] = true;
    community_pages[aboutLearnInDepth] = true;
    community_pages[contactUs] = true;
    community_pages[contentPolicy] = true;
    community_pages[invite] = true;
    community_pages[helperTest] = true;
    community_pages[mailPreferences] = true;

		community_pid = communities[i].pid;
		db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
		print("Update pages for " + community_pid);
	}
}

function changePageRoutesAboutAndZenodo() {
  print("change Page Routes '/about' and '/participate/share-zenodo'\n");

  db.page.update({ route: "/about" },{ $set: {"route": "/about/learn-how", "name" : "About - Learn How", "type" : "other", "connect":true,"communities":false }});
  db.page.update({ route: "/participate/share-zenodo" },{ $set: {"route": "/participate/deposit/zenodo"}});
}

function enableProjectReportPageInCommunities() {
    print("Enable project report page in communities \n");

  db.page.update({ route : "/project-report" },{ $set: {"communities": true}});
}

function addDepositPoliciesHelpContentInAllCommunities() {
    print("add deposit policies in help content in all communitites except 'openaire' and 'connect' in Deposit-Learn How page\n");

  deposit_first_page = db.page.find( { route: "/participate/deposit/learn-how" }).map( function(page) { return page._id.str; } ).toString();
  deposit_first_page_content = "" +
    "<div class=\"uk-width-3-5 uk-align-center\"> " +
    " <div class=\"uk-text-bold\">How to comply with funder Open Access policies</div> " +
    " <ul class=\"uk-list uk-list-bullet\"> " +
    "   <li>Read the <a class=\"custom-external\" href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-publications\" target=\"_blank\"> OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on publications </a></li> " +
    "   <li>Read the <a class=\"custom-external\" href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-data\" target=\"_blank\"> OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on research data </a></li> " +
    "   <li>If you want to know about National Open Access policies, please check them out <a class=\"custom-external\" href=\"https://www.openaire.eu/frontpage/country-pages\" target=\"_blank\">here</a></li> " +
    "   <li>All OpenAIRE guides can be found <a class=\"custom-external\" href=\"https://www.openaire.eu/guides\" target=\"_blank\">here</a></li> " +
    " </ul> " +
    "</div>";

  communities = db.community.find().map( function(community) { return community; } );
  for (var i = 0; i < communities.length; i++) {
    community = communities[i];
    if (community.pid != "connect" && community.pid != "openaire") {
      db.pageHelpContent.save({
        "page" : deposit_first_page,
        "community" : community._id.str,
        "placement" : "bottom",
        "order" : 1,
        "content" : deposit_first_page_content,
        "isActive" : true,
        "isPriorTo" : false
      });

    }
  }
}

function moveOrganizationsDivHelpContentsToPageHelpContents() {
  print("move organizations div help contents to page help contents for all communities\n ");

  organizations_page = db.page.find( { route: "/organizations" }).map( function(page) { return page._id.str; } ).toString();

  organizations_divId = db.divId.find( { name: "organizations" }).map( function(divId) { return divId._id.str; } ).toString();
  print("organizations divId: "+organizations_divId);

  communities = db.community.find().map(function (community) {
    return community;
  });
  for (var i = 0; i < communities.length; i++) {
    community = communities[i];
    print("community pid: "+community.pid);

    divHelpContents = db.divHelpContent.find({"community": community._id.str, "divId": organizations_divId}).map(function (divHelpContent) {
      return divHelpContent;
    });

    for (var j = 0; j < divHelpContents.length; j++) {
      divHelpContent = divHelpContents[j];

      db.pageHelpContent.save({
        "page": organizations_page,
        "community": community._id.str,
        "placement": "top",
        "order": j+1,
        "content": divHelpContent.content,
        "isActive": divHelpContent.isActive,
        "isPriorTo": false
      });

      db.divHelpContent.remove({"_id" : divHelpContent._id});
    }
  }
}

function deleteHomeDivHelpContents() {
    print("delete div help contents for divId 'home-news'\n");

  home_divId = db.divId.find( { name: "home-news" }).map( function(divId) { return divId._id.str; } ).toString();
  connect_communityId = db.community.find( { pid: "connect" }).map( function(community) { return community._id.str; } ).toString();

  divHelpContents = db.divHelpContent.find({"community": connect_communityId, "divId": home_divId}).map(function (divHelpContent) {
    return divHelpContent;
  });

  for (var i = 0; i < divHelpContents.length; i++) {
    divHelpContent = divHelpContents[i]

    db.divHelpContent.remove({"_id" : divHelpContent._id});
  }
}

function deleteHomeAndOrganizationsDivIds() {
  print("delete 'home-news' and 'organizations' divIds\n");

  db.divId.remove({"name" : "home-news"});
  db.divId.remove({"name" : "organizations"});
}

function addHelpTextsInHomePage() {
  print("add help contents in Home Page for community connect\n");

  communityID = db.community.find( { pid: "connect" }).map( function(community) { return community._id.str; } ).toString();
  home_page = db.page.find( { route: "/" }).map( function(page) { return page._id.str; } ).toString();

  bottom_1_content = "<div>Home news section</div>";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "bottom",
    "order" : 1,
    "content" : bottom_1_content,
    "isActive" : false,
    "isPriorTo" : false
  });

  bottom_2_content = "<div style=\"background-color: #CFDEF1;\"\n" +
    "     class=\"uk-background-norepeat uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color uk-margin-large-top\">\n" +
    "  <div class=\"uk-container uk-container-large uk-section\">\n" +
    "    <div class=\"uk-flex uk-flex-middle uk-padding uk-grid\" uk-grid=\"\">\n" +
    "      <div class=\"uk-text-center uk-width-1-1@s uk-width-1-3@m uk-first-column\"><img src=\"assets/connect-assets/contact/1.png\"\n" +
    "                                                                                     width=\"237\" height=\"250\"></div>\n" +
    "      <div class=\"uk-width-expand\">\n" +
    "        <div class=\"uk-text-bold uk-h4\">\n" +
    "          <div>Let us help you develop a collaborative Open Science Gateway for your community. It is fast. It is\n" +
    "            reliable.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-margin-medium\">Get in touch with our team to find out how.</div>\n" +
    "        <div class=\"uk-inline\"><a class=\"uk-button portal-button\" routerlink=\"/contact-us\"\n" +
    "                                  routerlinkactive=\"router-link-active\" ng-reflect-router-link=\"/contact-us\"\n" +
    "                                  ng-reflect-router-link-active=\"router-link-active\" href=\"/contact-us\"> CONTACT\n" +
    "          US</a>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "bottom",
    "order" : 2,
    "content" : bottom_2_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  left_content = "<div style=\"background-image: url('assets/connect-assets/home/banner.jpg') !important;\"\n" +
    "     class=\"uk-section uk-background-norepeat uk-background-bottom-center uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\">\n" +
    "  <div class=\" uk-section uk-padding-remove-bottom\">\n" +
    "    <div class=\"uk-position-cover\"></div>\n" +
    "    <div class=\"uk-position-relative uk-panel\">\n" +
    "      <div class=\"uk-container uk-container-large uk-section\">\n" +
    "        <div class=\"uk-grid\">\n" +
    "          <div class=\"uk-width-1-2@m uk-width-1-1@s\">\n" +
    "            <div class=\"uk-h1\">Build an Open Research <b>Gateway</b> for your <b>Community</b></div>\n" +
    "            <h4 class=\"uk-margin-remove-top\">Turn Open Science into practice</h4>\n" +
    "            <div class=\"uk-h5 uk-margin-top\">Share and link your research results.<br> Across organizations, across\n" +
    "              borders.<br>Customized to your needs.\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"uk-width-1-1 uk-inline uk-margin-medium-top uk-margin-medium-bottom\"><a\n" +
    "            class=\"uk-button portal-button\" routerlink=\"/about/learn-how\" routerlinkactive=\"router-link-active\"\n" +
    "            ng-reflect-router-link=\"/about/learn-how\" ng-reflect-router-link-active=\"router-link-active\"\n" +
    "            href=\"/about/learn-how\"> LEARN HOW</a></div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "left",
    "order" : 1,
    "content" : left_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  top_1_content = "<div class=\"uk-container uk-container-large uk-margin-medium-top\">\n" +
    "  <div class=\"uk-text-center uk-text-bold uk-h4\">Open and FAIR science is our mission</div>\n" +
    "  <div class=\"uk-margin-medium-top\">\n" +
    "    <div class=\"uk-child-width-1-3@m uk-child-width-1-1@s uk-grid-match uk-grid-medium uk-grid-margin uk-grid\"\n" +
    "         uk-grid=\"\"\n" +
    "         uk-height-match=\".target\">\n" +
    "      <div class=\"uk-first-column\">\n" +
    "        <div class=\"uk-card uk-card-default uk-padding-small connectInfoCard\">\n" +
    "          <div class=\"uk-card-media-top uk-flex uk-flex-middle uk-flex-center\">\n" +
    "            <div class=\"target\" style=\"min-height: 108.1px;\"><img src=\"assets/connect-assets/home/1.png\" width=\"100\" height=\"89\"></div>\n" +
    "          </div>\n" +
    "          <div class=\"uk-card-body uk-padding-remove-horizontal uk-padding-remove-bottom\">\n" +
    "            <div class=\"target\" style=\"min-height: 137.6px;\"><h5 class=\"uk-text-bold uk-text-center\">A Virtual Research\n" +
    "              Environment</h5>\n" +
    "              <div>An overlay platform making it easy to share, link, disseminate and monitor all your publications,\n" +
    "                data,\n" +
    "                software, methods. In one place.\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <hr>\n" +
    "            <div>\n" +
    "              <div class=\"uk-text-uppercase text-center\">Features</div>\n" +
    "              <ul class=\"uk-list\">\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div>\n" +
    "                  Access\n" +
    "                  to OpenAIRE resources\n" +
    "                </li>\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div>\n" +
    "                  Moderated, front-end linking\n" +
    "                </li>\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div>\n" +
    "                  Cross-platform search\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div>\n" +
    "        <div class=\"uk-card uk-card-default uk-padding-small connectInfoCard\">\n" +
    "          <div class=\"uk-card-media-top uk-flex uk-flex-middle uk-flex-center\">\n" +
    "            <div class=\"target\"><img src=\"assets/connect-assets/home/2.png\" width=\"100\" height=\"108\"></div>\n" +
    "          </div>\n" +
    "          <div class=\"uk-card-body uk-padding-remove-horizontal uk-padding-remove-bottom\">\n" +
    "            <div class=\"target\" style=\"min-height: 137.6px;\"><h5 class=\"uk-text-bold uk-text-center\">Open Science in\n" +
    "              action</h5>\n" +
    "              <div>A time-saving bundle of services for researchers to effortlessly practice open science. An integral\n" +
    "                part of the European Open Science Cloud.\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <hr>\n" +
    "            <div>\n" +
    "              <div class=\"uk-text-uppercase text-center\">Features</div>\n" +
    "              <ul class=\"uk-list\">\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div> Use\n" +
    "                  of\n" +
    "                  OpenAIRE Guidelines\n" +
    "                </li>\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div> DOIs\n" +
    "                  via Zenodo\n" +
    "                </li>\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div> EOSC\n" +
    "                  Single Sign-On\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div>\n" +
    "        <div class=\"uk-card uk-card-default uk-padding-small connectInfoCard\">\n" +
    "          <div class=\"uk-card-media-top uk-flex uk-flex-middle uk-flex-center\">\n" +
    "            <div class=\"target\" style=\"min-height: 108.1px;\"><img src=\"assets/connect-assets/home/3.png\" width=\"120\" height=\"104\">\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"uk-card-body uk-padding-remove-horizontal uk-padding-remove-bottom\">\n" +
    "            <div class=\"target\"><h5 class=\"uk-text-bold uk-text-center\">Customized to your needs</h5>\n" +
    "              <div>A Science Gateway with your own brand, rules for aggregation, text &amp; data mining, and\n" +
    "                presentation.\n" +
    "                Run by you via a simple, yet powerful backend administration tool.\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <hr>\n" +
    "            <div>\n" +
    "              <div class=\"uk-text-uppercase text-center\">Features</div>\n" +
    "              <ul class=\"uk-list\">\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div>\n" +
    "                  Access\n" +
    "                  control\n" +
    "                </li>\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div>\n" +
    "                  Analytics: rich set of indicators\n" +
    "                </li>\n" +
    "                <li><div\n" +
    "                  class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\"\n" +
    "                  uk-icon=\"check\"><svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "                                       data-svg=\"check\"><polyline fill=\"none\" stroke=\"#000\" stroke-width=\"1.1\"\n" +
    "                                                                  points=\"4,10 8,15 17,4\"></polyline></svg></div> Look\n" +
    "                  &amp; feel to match your brand\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "top",
    "order" : 1,
    "content" : top_1_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  top_2_content = "<div style=\"background-image: url('assets/connect-assets/home/background.png') !important;\"\n" +
    "     class=\"uk-margin-large-top uk-section uk-background-norepeat uk-background-top-center uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\">\n" +
    "  <div class=\"uk-container uk-container-large\">\n" +
    "    <div tabindex=\"-1\" uk-slider=\"velocity: 0;autoplay: true;autoplay-interval: 4000;pause-on-hover: false;center: true\"\n" +
    "         class=\"uk-slider\">\n" +
    "      <div class=\"uk-slider-container\">\n" +
    "        <ul class=\"uk-slider-items uk-child-width-1-1\" style=\"transform: translateX(-1300px);\">\n" +
    "          <li class=\"ng-star-inserted uk-active\" tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/home/gifs/deposit.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Find a repository to deposit your research outcome</div>\n" +
    "                  <div class=\"uk-margin-medium\"> This is OpenAIRE’s key service for research communities, both\n" +
    "                    established and emerging ones. Our service helps you reach out and engage all your researchers to\n" +
    "                    practice open science out-of-the-box.\n" +
    "                  </div>\n" +
    "                  <div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\"\n" +
    "                                            routerlinkactive=\"router-link-active\"\n" +
    "                                            ng-reflect-router-link=\"/about/learn-how\"\n" +
    "                                            ng-reflect-router-link-active=\"router-link-active\"\n" +
    "                                            href=\"/about/learn-how\">learn more</a></div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/home/gifs/link.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Link your research output with your community, funding, and other\n" +
    "                    research products\n" +
    "                  </div>\n" +
    "                  <div class=\"uk-margin-medium\"> This is OpenAIRE’s key service for research communities, both\n" +
    "                    established and emerging ones. Our service helps you reach out and engage all your researchers to\n" +
    "                    practice open science out-of-the-box.\n" +
    "                  </div>\n" +
    "                  <div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\"\n" +
    "                                            routerlinkactive=\"router-link-active\"\n" +
    "                                            ng-reflect-router-link=\"/about/learn-how\"\n" +
    "                                            ng-reflect-router-link-active=\"router-link-active\"\n" +
    "                                            href=\"/about/learn-how\">learn more</a></div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/home/gifs/overview.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">View community's overview at a glance</div>\n" +
    "                  <div class=\"uk-margin-medium\"> This is OpenAIRE’s key service for research communities, both\n" +
    "                    established and emerging ones. Our service helps you reach out and engage all your researchers to\n" +
    "                    practice open science out-of-the-box.\n" +
    "                  </div>\n" +
    "                  <div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\"\n" +
    "                                            routerlinkactive=\"router-link-active\"\n" +
    "                                            ng-reflect-router-link=\"/about/learn-how\"\n" +
    "                                            ng-reflect-router-link-active=\"router-link-active\"\n" +
    "                                            href=\"/about/learn-how\">learn more</a></div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/home/gifs/results.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Search &amp; browse your community's research products.</div>\n" +
    "                  <div class=\"uk-margin-medium\"> This is OpenAIRE’s key service for research communities, both\n" +
    "                    established and emerging ones. Our service helps you reach out and engage all your researchers to\n" +
    "                    practice open science out-of-the-box.\n" +
    "                  </div>\n" +
    "                  <div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\"\n" +
    "                                            routerlinkactive=\"router-link-active\"\n" +
    "                                            ng-reflect-router-link=\"/about/learn-how\"\n" +
    "                                            ng-reflect-router-link-active=\"router-link-active\"\n" +
    "                                            href=\"/about/learn-how\">learn more</a></div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li tabindex=\"-1\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/home/gifs/graph-analysis.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">View statistics for your community's research products.</div>\n" +
    "                  <div class=\"uk-margin-medium\"> This is OpenAIRE’s key service for research communities, both\n" +
    "                    established and emerging ones. Our service helps you reach out and engage all your researchers to\n" +
    "                    practice open science out-of-the-box.\n" +
    "                  </div>\n" +
    "                  <div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\"\n" +
    "                                            routerlinkactive=\"router-link-active\"\n" +
    "                                            ng-reflect-router-link=\"/about/learn-how\"\n" +
    "                                            ng-reflect-router-link-active=\"router-link-active\"\n" +
    "                                            href=\"/about/learn-how\">learn more</a></div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "        </ul>\n" +
    "        <ul class=\"uk-slider-nav uk-dotnav uk-flex-center uk-margin\">\n" +
    "          <li uk-slider-item=\"0\" class=\"uk-active\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"1\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"2\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"3\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"4\" class=\"\"><a href=\"#\"></a></li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "top",
    "order" : 2,
    "content" : top_2_content,
    "isActive" : true,
    "isPriorTo" : false
  });
}

function addHelpTextsInAboutLearnHowPage() {
  print("add help contents in About - Learn How Page for community connect\n");

  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  about_page = db.page.find( { route: "/about/learn-how" }).map( function(page) { return page._id.str; } ).toString()

  top_1_content = "<div\n" +
    "  class=\"uk-section uk-background-norepeat uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\"\n" +
    "  style=\" min-height: calc(7.89999px + 60vh);  background-image: url('assets/connect-assets/about/background.png') !important;\">\n" +
    "  <div class=\"uk-container uk-container-large uk-section uk-margin-top uk-padding-remove-top\">\n" +
    "    <div class=\"uk-margin-large-top uk-grid\"><h1 class=\"uk-width-1-1 font-41 uk-text-bold\">Learn the process</h1>\n" +
    "      <div class=\"uk-width-1-2@l uk-width-1-1@s uk-h5 uk-margin-top\">Build a <b>Gateway to your community's</b> open\n" +
    "        and linked research outcomes. Customized to your needs.\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"uk-margin-large-top uk-flex uk-child-width-1-3@m uk-child-width-1-1@s uk-flex-center uk-grid\"\n" +
    "         uk-grid=\"\">\n" +
    "      <div class=\"uk-flex uk-child-width-1-1@m uk-child-width-1-2@s uk-grid uk-first-column uk-grid-stack\" uk-grid=\"\">\n" +
    "        <div class=\"uk-first-column\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">1. Understanding your needs</h5>\n" +
    "          <div > First, we learn about your requirements and challenges. We help you\n" +
    "            understand Open Science practices within EOSC and together we’ll talk about how OpenAIRE RCD fits as a\n" +
    "            solution.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-visible@m uk-grid-margin uk-first-column\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">4. Roll out the service</h5>\n" +
    "          <div > We jointly roll out your new Community Gateway. You take over the business\n" +
    "            operations and start engaging your researchers, we take care of the smooth operation of the e-service.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-hidden@m\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">2. Develop a pilot</h5>\n" +
    "          <div > How do you work today, and how would you like to work tomorrow? We translate\n" +
    "            your needs into rules and processes and we configure operational OpenAIRE services. By the end of this\n" +
    "            phase, we’ll have defined the basic configuration of your Community Gateway.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"uk-text-center\"><img src=\"assets/connect-assets/about/cycle.png\"></div>\n" +
    "      <div class=\"uk-flex uk-child-width-1-1@m uk-child-width-1-2@s uk-grid uk-grid-stack\" uk-grid=\"\">\n" +
    "        <div class=\"uk-visible@m uk-first-column\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">2. Develop a pilot</h5>\n" +
    "          <div > How do you work today, and how would you like to work tomorrow? We translate\n" +
    "            your needs into rules and processes and we configure operational OpenAIRE services. By the end of this\n" +
    "            phase, we’ll have defined the basic configuration of your Community Gateway.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-hidden@m uk-visible@s\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">4. Roll out the service</h5>\n" +
    "          <div > We jointly roll out your new Community Gateway. You take over the business\n" +
    "            operations and start engaging your researchers, we take care of the smooth operation of the e-service.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-grid-margin uk-first-column\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">3. Test and Validate</h5>\n" +
    "          <div > You validate and test your new Community Gateway (portal) with your experts\n" +
    "            and community to ensure all workflows are in place and quality of data meets your standards. If needed, we\n" +
    "            work together in another iteration to further refine and adapt to your needs.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-hidden@s uk-visible@xs\">\n" +
    "          <h5 class=\"uk-text-bold uk-margin-small-bottom\">4. Roll out the service</h5>\n" +
    "          <div > We jointly roll out your new Community Gateway. You take over the business\n" +
    "            operations and start engaging your researchers, we take care of the smooth operation of the e-service.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"uk-width-1-1 uk-text-center uk-text-large uk-margin-large-top\"><a\n" +
    "      class=\"uk-button portal-button uk-text-uppercase\" routerlink=\"/about/learn-in-depth\"\n" +
    "      routerlinkactive=\"router-link-active\" ng-reflect-router-link=\"/about/learn-in-depth\"\n" +
    "      ng-reflect-router-link-active=\"router-link-active\" href=\"/about/learn-in-depth\"> Learn more details</a></div>\n" +
    "  </div>\n" +
    "</div>";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 1,
    "content": top_1_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_2_content = "<div style=\"background-image: url('assets/connect-assets/cloud/abstract.png') !important;\"\n" +
    "  class=\"uk-background-norepeat uk-section uk-background-center uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\">\n" +
    "  <div class=\"uk-container uk-container-large\">\n" +
    "    <div tabindex=\"-1\"\n" +
    "         uk-slider=\"velocity: 0;autoplay: true;autoplay-interval: 4000;pause-on-hover: false;center: true\"\n" +
    "         class=\"uk-slider\">\n" +
    "      <div class=\"uk-slider-container\">\n" +
    "        <ul class=\"uk-slider-items uk-child-width-1-1\" style=\"transform: translateX(-1300px);\">\n" +
    "          <li class=\"ng-star-inserted uk-active\" tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/about/gifs/profile.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Profile</div>\n" +
    "                  <div class=\"uk-margin-medium\"> Edit community information, change logo url, add community managers\n" +
    "                    or organizations related to community.\n" +
    "                  </div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li  tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/about/gifs/content.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Content</div>\n" +
    "                  <div class=\"uk-margin-medium\"> Manage projects, content providers, subjects and zenodo communities\n" +
    "                    that are related to the research community.\n" +
    "                  </div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li  tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/about/gifs/statistics.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Statistics &amp; Charts</div>\n" +
    "                  <div class=\"uk-margin-medium\"> Manage statistical numbers &amp; charts that will be displayed in\n" +
    "                    the community overview and graph analysis views.\n" +
    "                  </div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li  tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/about/gifs/links.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Links</div>\n" +
    "                  <div class=\"uk-margin-medium\"> Manage user claims related to the research community.</div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li  tabindex=\"-1\" style=\"order: 1;\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/about/gifs/help.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\"> Help texts</div>\n" +
    "                  <div class=\"uk-margin-medium\"> Add or edit help text in research community pages.</div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "          <li  tabindex=\"-1\">\n" +
    "            <div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
    "              <div class=\"uk-first-column\"><img class=\"uk-box-shadow-large uk-border-rounded\"\n" +
    "                                                src=\"assets/connect-assets/about/gifs/users.gif\"></div>\n" +
    "              <div class=\"uk-margin-top\" ng-reflect-klass=\"uk-margin-top\" ng-reflect-ng-class=\"\">\n" +
    "                <div>\n" +
    "                  <div class=\"uk-text-bold uk-h4\">Users</div>\n" +
    "                  <div class=\"uk-margin-medium\"> Invite more users to subscribe, manage community subscribers, your\n" +
    "                    personal info and notification settings.\n" +
    "                  </div>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </li>\n" +
    "        </ul>\n" +
    "        <ul class=\"uk-slider-nav uk-dotnav uk-flex-center uk-margin\">\n" +
    "          <li uk-slider-item=\"0\" class=\"uk-active\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"1\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"2\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"3\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"4\" class=\"\"><a href=\"#\"></a></li>\n" +
    "          <li uk-slider-item=\"5\" class=\"\"><a href=\"#\"></a></li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 2,
    "content": top_2_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_3_content = "<div style=\"background-color: #CFDEF1;\"\n" +
    "  class=\"uk-background-norepeat uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\">\n" +
    "  <div class=\"uk-container uk-container-large uk-section\">\n" +
    "    <div class=\"uk-flex uk-flex-middle uk-padding uk-grid\" uk-grid=\"\">\n" +
    "      <div class=\"uk-width-expand uk-first-column\">\n" +
    "        <div class=\"uk-text-bold uk-h4\">We look forward to working together and helping you unlock the full potential\n" +
    "          of your research community through open science.\n" +
    "        </div>\n" +
    "        <div class=\"uk-margin-medium\">Get in touch with our team to find out how.</div>\n" +
    "        <div class=\"uk-inline\"><a class=\"uk-button portal-button\" routerlink=\"/contact-us\"\n" +
    "                                  routerlinkactive=\"router-link-active\" ng-reflect-router-link=\"/contact-us\"\n" +
    "                                  ng-reflect-router-link-active=\"router-link-active\" href=\"/contact-us\"> CONTACT\n" +
    "          US</a></div>\n" +
    "      </div>\n" +
    "      <div class=\"uk-text-center uk-width-1-1@s uk-width-1-3@m\"><img src=\"assets/connect-assets/contact/3.png\" width=\"263\"\n" +
    "                                                                     height=\"250\"></div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 3,
    "content": top_3_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_4_content = "<div>\n" +
    "  FAQs section\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 4,
    "content": top_4_content,
    "isActive": false,
    "isPriorTo": false
  });
}

function addHelpTextsInAboutLearnInDepthPage() {
  print("add help contents in About - Learn In Depth for community connect\n");

  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  aboutLearnInDepth_page = db.page.find({route: "/about/learn-in-depth"}).map(function (page) { return page._id.str; }).toString();

  top_1_content = "<div\n" +
    "  class=\"uk-background-norepeat uk-background-cover uk-section uk-padding-remove-bottom uk-section-overlap uk-position-relative uk-preserve-color\"\n" +
    "  style=\"min-height: calc(7.89999px + 60vh); background-image: url('assets/connect-assets/cloud/background.png') !important;\">\n" +
    "  <div class=\"uk-container uk-container-large uk-section uk-margin-top uk-padding-remove-top\">\n" +
    "    <div uk-grid=\"\" class=\"uk-grid\">\n" +
    "      <div class=\"uk-margin-large-top uk-width-3-4@m uk-width-1-1@s uk-first-column\"><h1\n" +
    "        class=\"uk-width-3-4  uk-margin-medium-bottom\">Let’s set up a Gateway for your Community <b>Together</b></h1>\n" +
    "        <div class=\"uk-width-4-5@m uk-width-1-1@s uk-h5\">\n" +
    "          <div class=\"uk-margin-bottom\">You don’t have to go alone.</div>\n" +
    "          <div> We work with you in <b>4 collaborative steps</b> to identify your needs, putting in practice our\n" +
    "            expertise on open science so you get the most out of OpenAIRE’s operational services.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"uk-margin-large-top uk-width-expand\"><img src=\"assets/connect-assets/banner/together.png\" width=\"308\" height=\"285\">\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page": aboutLearnInDepth_page,
    "community": communityID,
    "placement": "top",
    "order": 1,
    "content": top_1_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_2_content = "<div class=\"uk-container uk-container-large uk-section uk-padding-remove-top\">\n" +
    "  <ul class=\"uk-breadcrumb\">\n" +
    "    <li><a routerlink=\"/about/learn-how\" routerlinkactive=\"router-link-active\" ng-reflect-router-link=\"/about/learn-how\"\n" +
    "           ng-reflect-router-link-active=\"router-link-active\" href=\"/about/learn-how\" class=\"router-link-active\">About</a></li>\n" +
    "    <li><span class=\"active\">Learn in-depth</span></li>\n" +
    "  </ul>\n" +
    "  <div uk-grid=\"\" class=\"uk-grid\">\n" +
    "    <div class=\"uk-width-1-6 uk-position-relative uk-flex uk-flex-column uk-first-column\"><img class=\"uk-align-center\"\n" +
    "                                                                                               src=\"assets/connect-assets/about/1.png\"\n" +
    "                                                                                               width=\"100\"><img\n" +
    "      class=\"uk-align-center\" src=\"assets/connect-assets/sketch_line_arrow.svg\"></div>\n" +
    "    <div class=\"uk-width-expand\">\n" +
    "      <div class=\"uk-margin-medium uk-text-bold uk-h4\">1. Analyse your needs</div>\n" +
    "      <div style=\"font-style: italic;\">Identify the scope and goals. Understand open science practices within EOSC and the OpenAIRE services</div>\n" +
    "      <p> In this stage, you get to talk to the OpenAIRE team. Share your expectations with us and let us give you all\n" +
    "        the details about the operational OpenAIRE services, which will be integrated into the Gateway for your\n" +
    "        community. </p>\n" +
    "      <p> Here are the most important questions that the OpenAIRE team will ask you, in order to understand your scope\n" +
    "        and goals: </p>\n" +
    "      <ul class=\"uk-list uk-list-bullet\">\n" +
    "        <li> Do you want a gateway, where researchers can have access to all research products of a discipline? Do you\n" +
    "          want a gateway that gathers any research outcome, produced thanks to the funding and services of a given\n" +
    "          research infrastructure?\n" +
    "        </li>\n" +
    "        <li> Is your community (in)formally organized in sub-communities? Would you like to browse research products and\n" +
    "          get statistics also for these sub-communities? For example, the European Grid Infrastructure (EGI) features\n" +
    "          “virtual organizations” that represent discipline-specific communities and/or specific research projects. The\n" +
    "          research infrastructure DARIAH, on the other hand, is organised in national nodes (e.g. DARIAH-IT, DARIAH-DE).\n" +
    "        </li>\n" +
    "        <li> How can the OpenAIRE team identify the research products of your community, among all those available in\n" +
    "          the OpenAIRE Graph? Through a series of steps: set of keywords, acknowledgment statements, set of projects,\n" +
    "          set of repositories, etc. This can be partial and provisional information that will serve as a starting point\n" +
    "          to the OpenAIRE team. You will be able to refine and update this information, in the second phase “Develop a\n" +
    "          pilot”.\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "  <div uk-grid=\"\" uk-height-match=\"\" class=\"uk-grid\">\n" +
    "    <div class=\"uk-width-1-6 uk-position-relative uk-flex uk-flex-column uk-first-column\"><img class=\"uk-align-center\"\n" +
    "                                                                                               src=\"assets/connect-assets/about/2.png\"\n" +
    "                                                                                               width=\"100\"><img\n" +
    "      class=\"uk-align-center\" src=\"assets/connect-assets/sketch_line_arrow.svg\"></div>\n" +
    "    <div class=\"uk-width-expand\">\n" +
    "      <div class=\"uk-margin-medium uk-text-bold uk-h4\">2. Develop a pilot</div>\n" +
    "      <div style=\"font-style: italic;\">We translate your needs into rules and processes and we configure operational OpenAIRE services.</div>\n" +
    "      <p> Based on the information gathered in phase 1 “Analyse your needs”, the OpenAIRE team will set up a pilot\n" +
    "        Gateway. We will configure the OpenAIRE mining algorithms to identify research products of the OpenAIRE Graph\n" +
    "        that are relevant to your community. Those, together with some basic statistics, will be available in the pilot\n" +
    "        version of the Community Gateway that will be deployed on the OpenAIRE BETA infrastructure. </p>\n" +
    "      <p> The OpenAIRE team will give you a demo of the Community Gateway, with details on how to refine and update the\n" +
    "        configuration of the Community Gateway, both in terms of criteria for including research products and in terms\n" +
    "        of logo and visible portal pages. </p></div>\n" +
    "  </div>\n" +
    "  <div uk-grid=\"\" uk-height-match=\"\" class=\"uk-grid\">\n" +
    "    <div class=\"uk-width-1-6 uk-position-relative uk-flex uk-flex-column uk-first-column\"><img class=\"uk-align-center\"\n" +
    "                                                                                               src=\"assets/connect-assets/about/3.png\"\n" +
    "                                                                                               width=\"100\"><img\n" +
    "      class=\"uk-align-center\" src=\"assets/connect-assets/sketch_line_arrow_large.svg\"></div>\n" +
    "    <div class=\"uk-width-expand\">\n" +
    "      <div class=\"uk-margin-medium uk-text-bold uk-h4\">3. Test and Validate</div>\n" +
    "      <div style=\"font-style: italic;\">You validate and test your new Community Gateway (portal). If needed, we further refine and adapt to your\n" +
    "        needs</div>\n" +
    "      <p> Upon the completion of phase 2, take the time you need to test all its features, from search and browse for\n" +
    "        research products, to addition/removal of statistics from the portal. You can report any issue you might find\n" +
    "        and ask questions directly to the dedicated OpenAIRE team, via a specially designed collaboration tool. </p>\n" +
    "      <p> Typically, this phase takes some months, as you will have to go through certain procedures. Change the\n" +
    "        configuration of the criteria to include research products, wait for the new configuration to be applied on the\n" +
    "        OpenAIRE graph and validate the results, before you actually decide that the coverage of research products for\n" +
    "        your community is adequate. </p>\n" +
    "      <p> For some communities, the OpenAIRE team may also be able to implement dedicated mining algorithms (e.g. to\n" +
    "        find acknowledgement statements to your community/infrastructure in the full-texts of research articles) that\n" +
    "        may require several rounds of application, validation, and fine-tuning, before it reaches a high precision and\n" +
    "        recall. Your feedback is very important to minimize the effort and time needed for this process to\n" +
    "        complete. </p>\n" +
    "      <div class=\"uk-width-1-1 uk-text-center uk-margin-medium \"><img src=\"assets/connect-assets/OpenAIRE-RCD_howtos.png\" width=\"auto\"\n" +
    "                                                                      height=\"auto\"></div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "  <div uk-grid=\"\" uk-height-match=\"\" class=\"uk-grid\">\n" +
    "    <div class=\"uk-width-1-6 uk-position-relative uk-first-column\"><img class=\"uk-align-center\" src=\"assets/connect-assets/about/4.png\"\n" +
    "                                                                        width=\"100\"></div>\n" +
    "    <div class=\"uk-width-expand\">\n" +
    "      <div class=\"uk-margin-medium  uk-text-bold uk-h4\">4. Roll out the service</div>\n" +
    "      <div style=\"font-style: italic;\">We jointly roll out your new portal. You take over the business operations and start engaging your\n" +
    "        researchers</div>\n" +
    "      <p> Here we are: the coverage of research products is good, interesting statistics and charts have been selected,\n" +
    "        and the portal pages available for end-users are ready. We can roll out the Community Gateway and make it\n" +
    "        available to all the researchers of the community! </p>\n" +
    "      <p> You, as a Community manager, become the main “promoter” of the Community Gateway. Engage the researchers of\n" +
    "        your community and, when applicable, inform the managers of the research infrastructure about the availability\n" +
    "        of tools for impact monitoring. </p>\n" +
    "      <p> Remember that you will still be able to change the configuration of the Community Gateway in order to address\n" +
    "        any issue that may arise and to follow the evolution of the community (e.g. a new project or a new content\n" +
    "        provider that was not previously available in OpenAIRE). </p>\n" +
    "      <p> Remember that you don’t have to go alone: the dedicated issue tracker you used in the “Test and Validate”\n" +
    "        phase is always available for you to contact the OpenAIRE team and ask for support. </p></div>\n" +
    "  </div>\n" +
    "</div>";

  db.pageHelpContent.save({
    "page": aboutLearnInDepth_page,
    "community": communityID,
    "placement": "top",
    "order": 2,
    "content": top_2_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_3_content = "<div\n" +
    "  class=\"uk-background-norepeat uk-background-cover uk-section-secondary uk-section-overlap uk-position-relative uk-preserve-color\"\n" +
    "  style=\"background-color: #CFDEF1;\">\n" +
    "  <div class=\"uk-container uk-container-large uk-section\">\n" +
    "    <div class=\"uk-flex uk-flex-middle uk-padding uk-grid\" uk-grid=\"\">\n" +
    "      <div class=\"uk-text-center uk-width-1-1@s uk-width-1-2@m uk-first-column\"><img src=\"assets/connect-assets/contact/2.png\"\n" +
    "                                                                                     width=\"329\" height=\"250\"></div>\n" +
    "      <div class=\"uk-width-expand\">\n" +
    "        <div class=\"uk-text-bold uk-h4\">\n" +
    "          <div>Let us help you develop a collaborative Open Science Gateway for your community. It is fast. It is\n" +
    "            reliable.\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"uk-margin-medium\">Get in touch with our team to find out how.</div>\n" +
    "        <div class=\"uk-inline\"><a class=\"uk-button portal-button\" routerlink=\"/contact-us\"\n" +
    "                                  routerlinkactive=\"router-link-active\" ng-reflect-router-link=\"/contact-us\"\n" +
    "                                  ng-reflect-router-link-active=\"router-link-active\" href=\"/contact-us\"> CONTACT US</a>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page": aboutLearnInDepth_page,
    "community": communityID,
    "placement": "top",
    "order": 3,
    "content": top_3_content,
    "isActive": true,
    "isPriorTo": false
  });
}

function addHelpTextsInContactUsPage() {
  print("add help contents in Contact Us Page for community connect\n");

  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  contactUs_page = db.page.find({route: "/contact-us"}).map(function (page) { return page._id.str; }).toString();

  right_content = "<div class=\"uk-margin-auto-top uk-margin-remove-bottom  uk-text-bold uk-h4\">OpenAIRE gives you the virtual\n" +
    "environment and services designed for your community to:\n" +
    "</div>\n" +
    "<ul class=\"uk-list uk-list-divider uk-padding uk-padding-remove-left uk-margin-auto-top\">\n" +
    "  <li><h5><span class=\"uk-text-bold\">Create and Manage</span> your\n" +
    "    Community Gateway</h5></li>\n" +
    "  <li><h5><span class=\"uk-text-bold\">Access, share and link</span> together all your research</h5></li>\n" +
    "  <li><h5><span class=\"uk-text-bold\">Monitor and report</span> your community's progress</h5></li>\n" +
    "</ul>\n";

  db.pageHelpContent.save({
    "page": contactUs_page,
    "community": communityID,
    "placement": "right",
    "order": 1,
    "content": right_content,
    "isActive": true,
    "isPriorTo": false
  });
}

function addHelpTextsInContentPolicyPage() {
  print("add help contents in Content Policy Page for community connect\n");

  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  content_page = db.page.find({route: "/content"}).map(function (page) { return page._id.str; }).toString();

  top_content = "<div class=\"uk-margin-large-top tm-middle uk-container\" id=\"tm-main\">\n" +
    "  <div class=\"uk-container  uk-margin-bottom\">\n" +
    "    <div class=\"uk-article-title custom-article-title\"> Content policy</div>\n" +
    "    <div><p> OpenAIRE builds an open scholarly graph of research products (publications, datasets, software and other\n" +
    "      types of research products), linked to each other and to fundings, projects and organizations. The information\n" +
    "      used to build the graph is collected from the OpenAIRE network of content providers and is inferred by OpenAIRE\n" +
    "      algorithms. OpenAIRE algorithms are capable of detecting duplicates and mining information from the full-texts of\n" +
    "      Open Access publications. For more details about the construction of the graph, please check <a\n" +
    "        href=\"https://beta.openaire.eu/aggregation-and-content-provision-workflows\" target=\"_blank\">here</a>. </p>\n" +
    "      <p> Thanks to the Research Community Dashboard (RCD), researchers have a dedicated view of the OpenAIRE graph\n" +
    "        where only the research products relevant to the community are searchable and browsable.<br> There are several\n" +
    "        ways to decide if a research product is relevant to a given community: </p>\n" +
    "      <p class=\"uk-margin-left\"><b>Links:</b> Users can \"claim\" that a research result is relevant to a community via\n" +
    "        the <a routerlink=\"/participate/claim\" routerlinkactive=\"uk-link\" ng-reflect-router-link=\"/participate/claim\"\n" +
    "               ng-reflect-router-link-active=\"uk-link\" href=\"/participate/claim\">Link</a> functionality of the Research\n" +
    "        community dashboard. <br> For more details about linking functionality, please check <a\n" +
    "          href=\"https://beta.openaire.eu/linking\" target=\"_blank\">here</a>. </p>\n" +
    "      <div class=\"uk-margin-left uk-margin-top\"><b>OpenAIRE algorithms:</b> Community managers can configure the\n" +
    "        OpenAIRE algorithms, in order to automatically assign research products to a community based on:\n" +
    "        <div class=\"uk-margin-left\">\n" +
    "          <ul class=\" uk-list uk-list-bullet  \">\n" +
    "            <li> The content providers they have been collected from. <br>Examples: all research products collected from\n" +
    "              \"NeuroVault\" are relevant to the Neuroinformatics community; all research products collected from the\n" +
    "              LINDAT/CLARIN repository are relevant to the CLARIN research initiative.\n" +
    "            </li>\n" +
    "            <li> The projects they have been produced in. <br>Examples: all research products of the project \"Tara\n" +
    "              Mediterranee\" are relevant to the European Marine Science community; all research products of the project\n" +
    "              \"ARIADNE\" are relevant to the Digital Humanities and Cultural Heritage community.\n" +
    "            </li>\n" +
    "            <li> The subjects and keywords (only for RCD serving research communities). <br>Examples: all research\n" +
    "              products having \"SDG11 - Sustainable cities and communities\" among the subjects are relevant to the Greek\n" +
    "              Sustainable Development Solutions Network community; all research products with subject \"agriculture\" are\n" +
    "              relevant to the AgInfra community\n" +
    "            </li>\n" +
    "          </ul>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"uk-margin-left uk-margin-top\"><b>Acknowledgement statements:</b> Acknowledgement statements found in\n" +
    "        full-texts (only for RCDs serving research infrastructures/initiatives)\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n";

  db.pageHelpContent.save({
    "page": content_page,
    "community": communityID,
    "placement": "top",
    "order": 1,
    "content": top_content,
    "isActive": true,
    "isPriorTo": false
  });
}

function removeFaultyEmptyPageFromCommunities() {
  communities = db.community.find().map( function(community) { return community; } );

  for (var i = 0; i < communities.length; i++) {
    community_pages = communities[i].pages;

    delete community_pages[""];

    community_pid = communities[i].pid;
    db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
    print("Update pages for " + community_pid);
  }
}

function addMyCommunitiesPage() {
    db.page.save({
        "name": "My Communities",
        "route": "/myCommunities",
        "type": "other",
        "connect": true,
        "communities": false,
        "openaire": false,
        "entities": [],
        "top": false,
        "bottom": false,
        "right": false,
        "left": false
    });
    myCommunities = db.page.find( { route: "/myCommunities" }).map( function(page) { return page._id.str; } ).toString();
    communities = db.community.find().map( function(community) { return community; } );
    for (var i = 0; i < communities.length; i++) {
        community_pages = communities[i].pages;

        community_pages[myCommunities] = true;
        community_pid = communities[i].pid;
        db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
        print("Update pages for " + community_pid);
    }
}

function removeOldLayouts() {
  db.community.updateMany({}, {$unset: {layout: ""}});
  db.layout.deleteMany({});
}

function addSearchResearchOutcomesPages() {
  publicationId = db.entity.find( { pid: "publication" }).map( function(entity) { return entity._id.str; } ).toString()
  datasetId = db.entity.find( { pid: "dataset" }).map( function(entity) { return entity._id.str; } ).toString()
  softwareId = db.entity.find( { pid: "software" }).map( function(entity) { return entity._id.str; } ).toString()
  orpId = db.entity.find( { pid: "orp" }).map( function(entity) { return entity._id.str; } ).toString();

  searchResearchOutcomesId = db.page.insertOne({"name" : "Search Research Outcomes", "route" : "/search/find/research-outcomes", "type" : "search", "connect":false,"openaire":true, "communities": true, "entities" : [publicationId, datasetId, softwareId, orpId], "top":true,"bottom":false,"right":false,"left":false}).insertedId.str;
  avancedSearchResearchOutcomesId = db.page.insertOne({"name" : "Advanced Search Research Outcomes", "route" : "/search/advanced/research-outcomes", "type" : "search", "connect":false,"openaire":true, "communities": true, "entities" : [publicationId, datasetId, softwareId, orpId], "top":true,"bottom":false,"right":false,"left":false}).insertedId.str;

  print("Creating Search Research Outcomes page with id " + searchResearchOutcomesId);
  print("Creating Advanced Search Research Outcomes page with id " + avancedSearchResearchOutcomesId);

  communities = db.community.find().map( function(community) { return community; } );
  for (var i = 0; i < communities.length; i++) {
    community_pages = communities[i].pages;

    community_pages[searchResearchOutcomesId] = true;
    community_pages[avancedSearchResearchOutcomesId] = true;

    community_pid = communities[i].pid;
    db.community.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
    print("Add Search Research Outcomes page with id " + searchResearchOutcomesId + " on community " + community_pid);
    print("Add Advanced Search Research Outcomes page with id " + avancedSearchResearchOutcomesId + " on community " + community_pid);
  }
}

function addPortalType() {
  communities = db.community.find().map( function(community) { return community; } );
  for (var i = 0; i < communities.length; i++) {
    community = communities[i];
    if(community.pid == "openaire") {
      community['type'] = "explore";
    } else if(community.pid == "connect") {
      community['type'] = "connect";
    } else {
      community['type'] = "community";
    }
    db.community.save(community);
  }
}

function createPagesForEachPortalType() {
  pages = db.page.find().map( function(page) { return page; } );
  for (var i = 0; i < pages.length; i++) {
    page = pages[i];

    print("createPagesForEachPortalType: page["+i+"] = "+page.name + " ( "+page._id.str+" ) -> openaire: "+page.openaire+" - connect: "+page.connect + " - communities: "+page.communities);

    if(page.openaire == false) {  // do not include this page in portal profile
      communityOpenaire = db.community.findOne({pid: "openaire"});
      removePageFromPortal(communityOpenaire, page._id, communityOpenaire.pages);
    }
    if(page.connect == false) {  // do not include this page in portal profile
      communityConnect = db.community.findOne({pid: "connect"});
      removePageFromPortal(communityConnect, page._id, communityConnect.pages);
    }
    if(page.communities == false) {
      communities = db.community.find({ "type" : "community" }).map( function(community) { return community; } );
      for (var j = 0; j < communities.length; j++) {
        removePageFromPortal(communities[j], page._id, communities[j].pages);
      }
    }

    if(page.openaire == true && page.connect == true && page.communities == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "community", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for communities");

      createAndUpdatePageAndContentsForPortal(page, "openaire", "explore");
      //print("Delete and create other for explore");

      createAndUpdatePageAndContentsForPortal(page, "connect", "connect");
      //print("Delete and create other for connect");

    } else if(page.openaire == true && page.connect == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "connect", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for connect");

      createAndUpdatePageAndContentsForPortal(page, "openaire", "explore");
      //print("Delete and create other for explore");

    } else if(page.openaire == true && page.communities == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "community", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for community");

      createAndUpdatePageAndContentsForPortal(page, "openaire", "explore");
      //print("Delete and create other for explore");

    } else if(page.connect == true && page.communities == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "community", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for community");

      createAndUpdatePageAndContentsForPortal(page, "connect", "connect");
      //print("Delete and create other for connect");

    } else if(page.openaire == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "explore", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for explore");

    } else if(page.connect == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "connect", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for connect");

    } else if(page.communities == true) {
      db.page.save({"_id": page._id, "name" : page.name, "route" : page.route, "type" : page.type, "portalType": "community", "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
      print("Keep for community");

    }
  }
}

function createAndUpdatePageAndContentsForPortal(page, pid, portalType) {
  db.page.save({"name" : page.name, "route" : page.route, "type" : page.type, "portalType": portalType, "entities" : page.entities, "top":page.top, "bottom":page.bottom, "right":page.right,"left":page.left});
  pageIdForPortalType = db.page.find({portalType: portalType, "route" : page.route}).map(function (page) { return page._id.str; }).toString();
  print("portalType: "+ portalType + " --> new page id: "+ pageIdForPortalType);

  community = db.community.findOne({pid: pid});

  community_pages = community.pages;
  community_pages[pageIdForPortalType] = community_pages[page._id.str];
  removePageFromPortal(community, page._id, community_pages);
  if(portalType == "explore") {
    print("    Delete page: "+page._id.str + " = (should be null) "+community.pages[page._id.str]
      + " - new page: "+pageIdForPortalType+" = "+community.pages[pageIdForPortalType]);
  }

  updatePageHelpContentsForPortalPage(page._id.str, pageIdForPortalType, community._id.str);
}

function removePageFromPortal(community, pageId, community_pages) {
  delete community_pages[pageId.str];
  db.community.update({"pid": community.pid}, {$set: {"pages": community_pages}});
}

function updatePageHelpContentsForPortalPage(pageId, newPageId, communityId) {
  pageHelpContents = db.pageHelpContent.find({"page" : pageId, "community" : communityId,}).map( function(pageHelpContent) { return pageHelpContent; } );
  for (var i = 0; i < pageHelpContents.length; i++) {
    pageHelpContent = pageHelpContents[i];
    db.pageHelpContent.save({
      "_id": pageHelpContent._id,
      "page": newPageId,
      "community": communityId,
      "placement": pageHelpContent.placement,
      "order": pageHelpContent.order,
      "content": pageHelpContent.content,
      "isActive": pageHelpContent.isActive,
      "isPriorTo": pageHelpContent.isPriorTo
    });
  }
}

function createDivIdsForEachPortalType() {
  divIds = db.divId.find().map( function(divId) { return divId; } );
  for (var i = 0; i < divIds.length; i++) {
    divId = divIds[i];

    print("createDivIdsForEachPortalType: divId["+i+"] = "+divId.name + " -> openaire: "+divId.openaire+" - connect: "+divId.connect + " - communities: "+divId.communities);

    if(divId.openaire == true && divId.connect == true && divId.communities == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "community", false);
      createAndUpdateDivIdAndContentsForPortal(divId, "openaire", "explore", true);
      createAndUpdateDivIdAndContentsForPortal(divId, "connect", "connect", true);

    } else if(divId.openaire == true && divId.connect == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "connect", false);
      createAndUpdateDivIdAndContentsForPortal(divId, "openaire", "explore", true);

    } else if(divId.openaire == true && divId.communities == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "community", false);
      createAndUpdateDivIdAndContentsForPortal(divId, "openaire", "explore", true);

    } else if(divId.connect == true && divId.communities == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "community", false);
      createAndUpdateDivIdAndContentsForPortal(divId, "connect", "connect", true);

    } else if(divId.openaire == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "explore", false);

    } else if(divId.connect == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "connect", false);

    } else if(divId.communities == true) {
      createAndUpdateDivIdAndContentsForPortal(divId, null, "community", false);
    }
  }
}

function updateDivIdPages(pages, portalType) {
  var newPages = [];
  for (var i = 0; i < pages.length; i++) {
    page = db.page.findOne({"_id": ObjectId(pages[i])});
    pageForPortalType = db.page.findOne({portalType: portalType, "route" : page.route});
    if(pageForPortalType) {
      print("page in divId: "+pages[i] + " will be replaced with "+pageForPortalType._id.str);
      newPages.push(pageForPortalType._id.str);
    } else {
      print("page in divId: "+pages[i] + " will be not be replaced. pageForPortalType: "+pageForPortalType);
    }
  }
  return newPages;
}

function createAndUpdateDivIdAndContentsForPortal(divId, pid, portalType, create) {
  var pages = updateDivIdPages(divId.pages, portalType);
  if(pages.length == 0) {
    db.divId.remove({"_id": divId._id});
    communities = db.community.find({ "type" : portalType }).map( function(community) { return community; } );
    for (var j = 0; j < communities.length; j++) {
      db.divHelpContent.remove({"divId": divId._id.str, "community": communities[j]._id.str});
    }
  } else {
    if (create) {
      db.divId.save({"name": divId.name, "pages": pages, "portalType": portalType});

      divIdForPortalType = db.divId.find({portalType: portalType, "name" : divId.name}).map(function (divId) { return divId._id.str; }).toString();

      communityId = db.community.find({pid: pid}).map(function (community) { return community._id.str; }).toString();

      updateDivIdHelpContentsForPortalPage(divId._id.str, divIdForPortalType, communityId);
    } else {
      db.divId.save({"_id": divId._id, "name": divId.name, "pages": pages, "portalType": portalType});
    }
  }
}

function updateDivIdHelpContentsForPortalPage(divIdId, newDivIdId, communityId) {
  divHelpContents = db.divHelpContent.find({"divId" : divIdId, "community" : communityId,}).map( function(divHelpContent) { return divHelpContent; } );
  for (var i = 0; i < divHelpContents.length; i++) {
    divHelpContent = divHelpContents[i];

    db.divHelpContent.save({
      "_id": divHelpContent._id,
      "divId" : newDivIdId,
      "community" : communityId,
      "content" : divHelpContent.content,
      "isActive" : divHelpContent.isActive
    });
  }
}

function renameCollectionAndFields() {
  db.community.renameCollection("portal");
  db.communitySubscribers.renameCollection("portalSubscribers");

  db.divHelpContent.update({"community": {$exists: true}}, {$rename:{"community":"portal"}}, false, true);
  db.pageHelpContent.update({"community": {$exists: true}}, {$rename:{"community":"portal"}}, false, true);
  db.notifications.update({"communityPid": {$exists: true}}, {$rename:{"communityPid":"portalPid"}}, false, true);
}

function addPortalPidInLayoutRecords() {
  communities = db.portal.find().map( function(community) { return community; } );
  for (var j = 0; j < communities.length; j++) {
    community = communities[j];
    if(community.layout) {
      layout = db.layout.findOne({"_id": ObjectId(community.layout)});
      db.layout.save({
        "_id": layout._id,
        "mainColor": layout.mainColor,
        "secondaryColor": layout.secondaryColor,
        "panel": layout.panel,
        "box": layout.box,
        "links": layout.links,
        "buttons": layout.buttons,
        "portalPid": community.pid
      })

      db.portal.update({"_id": community._id}, {$unset: {layout:""}});
    }
  }
}

// 22-10-2020
function addPiwik() {
  db.portal.update({"pid": "openaire"}, {$set: {piwik:"6"}});
  db.portal.update({"pid": "connect"}, {$set: {piwik:"80"}});
  db.portal.update({"pid": "dh-ch"}, {$set: {piwik:"81"}});
  db.portal.update({"pid": "ee"}, {$set: {piwik:"82"}});
  db.portal.update({"pid": "egi"}, {$set: {piwik:"83"}});
  db.portal.update({"pid": "elixir-gr"}, {$set: {piwik:"84"}});
  db.portal.update({"pid": "fam"}, {$set: {piwik:"85"}});
  db.portal.update({"pid": "instruct"}, {$set: {piwik:"86"}});
  db.portal.update({"pid": "mes"}, {$set: {piwik:"87"}});
  db.portal.update({"pid": "ni"}, {$set: {piwik:"88"}});
  db.portal.update({"pid": "oa-pg"}, {$set: {piwik:"89"}});
  db.portal.update({"pid": "rda"}, {$set: {piwik:"90"}});
  db.portal.update({"pid": "aginfra"}, {$set: {piwik:"93"}});
  db.portal.update({"pid": "clarin"}, {$set: {piwik:"100"}});
  db.portal.update({"pid": "dariah"}, {$set: {piwik:"103"}});
  db.portal.update({"pid": "epos"}, {$set: {piwik:"217"}});
  db.portal.update({"pid": "beopen"}, {$set: {piwik:"218"}});
  db.portal.update({"pid": "risis"}, {$set: {piwik:"219"}});
  db.portal.update({"pid": "science-innovation-policy"}, {$set: {piwik:"253"}});
  db.portal.update({"pid": "covid-19"}, {$set: {piwik:"267"}});
  db.portal.update({"pid": "rural-digital-europe"}, {$set: {piwik:"319"}});
  db.portal.update({"pid": "enermaps"}, {$set: {piwik:"318"}});

  db.portal.update({"pid": "galaxy"}, {$set: {piwik:"453"}});
  db.portal.update({"pid": "gotriple"}, {$set: {piwik:"490"}});
  db.portal.update({"pid": "neanias-underwater"}, {$set: {piwik:"474"}});
  // db.portal.update({"pid": "embrc"}, {$set: {piwik:"474"}});
}

// 03-06-2021
function addFooterDivIdForConnect() {
  connectHomePageID = db.page.find({"portalType": "connect", "route": "/"}).map( function(page) { return page._id.str; } ).toString();
  db.divId.save({ "name" : "footer", "pages" : [ connectHomePageID ], "portalType" : "connect" });
  print("divId 'footer' added for connect (home page id: "+connectHomePageID + ")");
}

function addFooterDivIdForCommunities() {
  // db.page.save({ "route" : "/", "name" : "Home", "type" : "other", "entities" : [ ], "portalType" : "community", "top" : false, "bottom" : false, "left" : false, "right" : false });
  // print("page 'Home' added for communities");

  communityHomePageID = db.page.find({"portalType": "community", "route": "/"}).map( function(community) { return community._id.str; } ).toString();
  db.divId.save({ "name" : "footer", "pages" : [ communityHomePageID ], "portalType" : "community" });
  print("divId 'footer' added for communities (home page id: "+communityHomePageID + ")");
}

function addHomePageInCommunities() {
  communityHomePageID = db.page.insertOne({ "route" : "/", "name" : "Home", "type" : "other", "entities" : [ ], "portalType" : "community", "top" : false, "bottom" : false, "left" : false, "right" : false }).insertedId.str;
  print("Creating Home page with id " + communityHomePageID);
  communities = db.portal.find({"type": "community"}).map( function(community) { return community; } );
  for (var i = 0; i < communities.length; i++) {
    community_pages = communities[i].pages;

    community_pages[communityHomePageID] = true;

    community_pid = communities[i].pid;
    db.portal.update({ "pid" : community_pid },{$set: { "pages": community_pages}});
    print("Add home page with id " + communityHomePageID + " on community " + community_pid);
  }
}

function addFooterHelpTextForConnect() {
  connectID = db.portal.find({ "pid" : "connect"}).map( function(portal) { return portal._id.str; } ).toString();
  connectFooterDivIdID = db.divId.find({"portalType": "connect", "name": "footer"}).map( function(divId) { return divId._id.str; } ).toString();

  db.divHelpContent.save(
    { "divId" : connectFooterDivIdID,
      "portal" : connectID,
      "content" : "<p class=\"uk-margin-remove-bottom\"><span style=\"font-size:8pt\">" +
        "OpenAIRE has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011 and 101017452" +
        "</span></p>",
      "isActive" : true
    }
  );

  print("div help text for divId 'footer' added for connect (connect id: "+ connectID + " - footer divId id: "+connectFooterDivIdID + ")");
}

function addFooterHelpTextForCommunities() {
  communityFooterDivIdID = db.divId.find({"portalType": "community", "name": "footer"}).map( function(divId) { return divId._id.str; } ).toString();

  communities = db.portal.find({"type": "community"}).map( function(community) { return community; } );
  for (var j = 0; j < communities.length; j++) {
    community = communities[j];

    var content = "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011 and 101017452";

    if(community.pid == "aginfra" || community.pid == "rural-digital-europe") {
      content = "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011 and 101017452";

    } else if(community.pid == "clarin" || community.pid == "covid-19" || community.pid == "dariah"
      || community.pid == "dh-ch" || community.pid == "egi" || community.pid == "mes" || community.pid == "epos"
      || community.pid == "elixir-gr" || community.pid == "fam" || community.pid == "ni"
      || community.pid == "sdsn-gr" || community.pid == "instruct" || community.pid == "instruct"
      || community.pid == "rda" || community.pid == "gotriple") {

      content = "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011 and 101017452";

    } else if(community.pid == "enermaps") {
      content = "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011, 101017452 and 884161";

    } else if(community.pid == "beopen") {
      content = "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011, 101017452 and 824323";

    } else if(community.pid == "risis" || community.pid == "science-innovation-policy") {
      content = "This OpenAIRE gateway is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541, 731011, 101017452 and 824091";
    }

    db.divHelpContent.save(
      { "divId" : communityFooterDivIdID,
        "portal" : community._id.str,
        "content" : "<p class=\"uk-margin-remove-bottom\"><span style=\"font-size:8pt\">"+content+"</span></p>",
        "isActive" : true
      }
    );

    print("div help text for divId 'footer' added for community (community id: "+ community._id + " - community pid: " + community.pid + " - footer divId id: "+communityFooterDivIdID + ")");
  }
}

// 04-06-2021 debug helper function
function cleanPagesFromPortals() {
  portals = db.portal.find().map( function(portal) { return portal; } );
  for (var j = 0; j < portals.length; j++) {
    portal = portals[j];
    portal_pages = portal.pages;

    for (var key in portal_pages) {
      // print(key);
      // db.page.findOne({"_id": ObjectId(+'"'+key+'"')});
      if(db.page.find({"_id": ObjectId(key)}).map( function(page) { return page; } ).length == 0) {
        delete portal_pages[key];
        print("deleted page: "+key + " from portal: "+portal.pid);
      }
    }
    portal_pid = portal.pid;
    db.portal.update({ "pid" : portal_pid },{$set: { "pages": portal_pages}});
  }
}

use openaireconnect;

//updatePages();

//updateTypeOfLandingPages();
//removeEntitiesFromSearchFindPage();
//initDivHelpContents();

//createContentPageWithTypeOther();
//createPagesWithTypeHtml();
//addContent_About_Organizations_PagesToCommunities();
//createCollectionForHtmlPageContents();
//addDefaultHtmlToHtmlPages();

//createMonitorPageWithTypeOther();
//addMonitorPageToCommunities();

//createOtherResearchProducts();

//addDefaultHtmlToHtmlPagesForCommunity("clarin");
//createNotificationsCollection();
//addDivHelpContentsForCommunity("clarin");
//addORPInStatistics();
//createShareInZenodoPage();
//addCommunityLayout();
//addCuratorsPage();
//removeMonitorPage();

//pageOptionConnectAsCommunities();

//organizationsPageTypeFromHtmlToOther();
//removeHtmlPageContentsFromOrganizationsPage();
//addOpenaireConnectCommunitiesOptionsInDivIds();
//addHomeNewsAndOrganizationsDivIds();
//removeLinkingDivHelpContentsForCommunitiesAndConnect();


//-----------------------------------------------------
// addMissingAndNewPages();
// changePageRoutesAboutAndZenodo();
// addDepositPoliciesHelpContentInAllCommunities();
// moveOrganizationsDivHelpContentsToPageHelpContents();
// deleteHomeDivHelpContents();
// deleteHomeAndOrganizationsDivIds();
// enableProjectReportPageInCommunities();
//
// addPositionsInPages();
//
//
// addHelpTextsInHomePage();
// addHelpTextsInAboutLearnHowPage();
// addHelpTextsInAboutLearnInDepthPage();
// addHelpTextsInContactUsPage();
//
// addHelpTextsInContentPolicyPage();
//
// addNewRoutesInCommunities();
//addMyCommunitiesPage();
//removeFaultyEmptyPageFromCommunities();

// /* 08-10-2019*/
// removeOldLayouts();

// 13-03-2020
//addSearchResearchOutcomesPages();

// 02-02-2020 - 19-03-2020
// addPortalType();
// createPagesForEachPortalType();
// createDivIdsForEachPortalType();
// renameCollectionAndFields();
// addPortalPidInLayoutRecords();

// 22-10-2020
// addPiwik();

// 03-06-2021
addFooterDivIdForConnect();
addFooterHelpTextForConnect();
addHomePageInCommunities();
addFooterDivIdForCommunities();
addFooterHelpTextForCommunities();

// 04-06-2021 debug helper function
// cleanPagesFromPortals();

// 07-06-2021 debug helper function
// cleanDivHelpTexts();