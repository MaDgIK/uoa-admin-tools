//version compatibility: 2.0.0-SNAPSHOT
print("here");

function dropDB(){
print("\n\n Drop DB\n\n")
	db.dropDatabase()
}
function createCollections(){
print("\n\n Create Collections \n\n")
	db.createCollection("community")
	db.createCollection("layout")
	db.createCollection("divHelpContent")
	db.createCollection("divId")
	db.createCollection("entity")
	db.createCollection("page")
	db.createCollection("pageHelpContent")
	db.createCollection("statistics")
	db.createCollection("communitySubscribers")
	db.createCollection("htmlPageContent")
	db.createCollection("notifications")
	db.createCollection("subscriber")
	db.createCollection("curator")
}
function initializeEntitiesPages(){
print("\n\n Initialize entities and pages\n\n")
	db.entity.save({"pid" : "publication", "name" : "Publication"})
	db.entity.save({"pid" : "dataset", "name" : "Research Data"})
	db.entity.save({"pid" : "software", "name" : "Software"})
	db.entity.save({"pid" : "orp", "name" : "Other Research Products"});
	db.entity.save({"pid" : "project", "name" : "Project"})
	db.entity.save({"pid" : "organization", "name" : "Organization"})
	db.entity.save({"pid" : "datasource", "name" : "Content Provider"})

	publicationId = db.entity.find( { pid: "publication" }).map( function(entity) { return entity._id.str; } ).toString()
	datasetId = db.entity.find( { pid: "dataset" }).map( function(entity) { return entity._id.str; } ).toString()
	softwareId = db.entity.find( { pid: "software" }).map( function(entity) { return entity._id.str; } ).toString()
	orpId = db.entity.find( { pid: "orp" }).map( function(entity) { return entity._id.str; } ).toString();
	projectId = db.entity.find( { pid: "project" }).map( function(entity) { return entity._id.str; } ).toString()
	organizationId = db.entity.find( { pid: "organization" }).map( function(entity) { return entity._id.str; } ).toString()
	datasourceId = db.entity.find( { pid: "datasource" }).map( function(entity) { return entity._id.str; } ).toString()

  // Other Pages
  db.page.save({"name" : "Home", "route" : "/", "type" : "other", "connect":true,"communities":false,"openaire":true,"entities" : [], "top":true,"bottom":true,"right":true,"left":true});

  // Other Pages for Connect
  db.page.save({"name" : "About - Learn How", "route" : "/about/learn-how", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});
  db.page.save({"name" : "About - Learn in depth", "route" : "/about/learn-in-depth", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});
  db.page.save({"name" : "About - FAQs", "route" : "/about/faq", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});
  db.page.save({"name" : "Contact us", "route" : "/contact-us", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":false,"bottom":false,"right":true,"left":false});
  db.page.save({"name" : "Content Policy", "route" : "/content", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});

  db.page.save({"name" : "Curators", "route" : "/curators", "type" : "other", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});
  db.page.save({"name" : "Organizations", "route" : "/organizations", "type" : "other", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});
  db.page.save({"name" : "Invite", "route" : "/invite", "type" : "other", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":true,"right":false,"left":false});
  db.page.save({"name" : "My Communities", "route": "/myCommunities", "type": "other", "connect": true, "communities": false, "openaire": false, "entities": [], "top": false, "bottom": false, "right": false, "left": false
    });
  // Testing Page for help contents (Connect)
  db.page.save({"name" : "Helper Test", "route" : "/helper-test", "type" : "other", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});

  // Other Pages for Explore
  db.page.save({"name" : "Mail Preferences", "route" : "/mail-preferences", "type" : "other", "connect":false,"communities":false,"openaire":true,"entities" : [], "top":false,"bottom":false,"right":false,"left":false});

  // Landing Pages
  db.page.save({"name" : "Publication Landing", "route" : "/search/publication", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [publicationId],"top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Research Data Landing", "route" : "/search/dataset", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [datasetId],"top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Software Landing", "route" : "/search/software", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [softwareId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Other Research Products Landing", "route" : "/search/other", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [orpId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Project Landing", "route" : "/search/project", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [projectId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Organization Landing", "route" : "/search/organization", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [organizationId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Content Provider Landing", "route" : "/search/dataprovider", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [datasourceId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Project Report", "route" : "/project-report", "type" : "landing", "connect":false,"communities":true,"openaire":true,"entities" : [projectId], "top":true,"bottom":false,"right":false,"left":false});

	// Search Pages
	db.page.save({"name" : "Search Find", "route" : "/search/find", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [], "top":false,"bottom":false,"right":false,"left":false});

  db.page.save({"name" : "Search Communities", "route" : "/search/find/communities", "type" : "search", "connect":true,"communities":false,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});

  db.page.save({"name" : "Search Research Outcomes", "route" : "/search/find/research-outcomes", "type" : "search", "connect":false,"communities":true,"openaire":true,"entities" : [publicationId, datasetId, softwareId, orpId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Projects", "route" : "/search/find/projects", "type" : "search", "connect":false,"communities":true,"openaire":true,"entities" : [projectId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Organizations", "route" : "/search/find/organizations", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [organizationId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Content Providers", "route" : "/search/find/dataproviders", "type" : "search", "connect":false,"communities":true,"openaire":true,"entities" : [datasourceId], "top":true,"bottom":false,"right":false,"left":false});

	// Advanced Search Pages
	db.page.save({"name" : "Advanced Search Research Outcomes", "route" : "/search/advanced/research-outcomes", "type" : "search", "connect":false,"communities":true,"openaire":true,"entities" : [publicationId, datasetId, softwareId, orpId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Advanced Search Projects", "route" : "/search/advanced/projects", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [projectId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Advanced Search Organizations", "route" : "/search/advanced/organizations", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [organizationId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Advanced Search Content Providers", "route" : "/search/advanced/dataproviders", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":true,"bottom":false,"right":false,"left":false});

	// Search Content Providers Pages
	db.page.save({"name" : "Search Journals", "route" : "/search/journals", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Entity Registries", "route" : "/search/entity-registries", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Compatible Content Providers", "route" : "/search/content-providers", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":true,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Journals - Table view", "route" : "/search/journals-table", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":false,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Entity Registries - Table view", "route" : "/search/entity-registries-table", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":false,"bottom":false,"right":false,"left":false});
	db.page.save({"name" : "Search Compatible Content Providers -Table view", "route" : "/search/content-providers-table", "type" : "search", "connect":false,"communities":false,"openaire":true,"entities" : [datasourceId], "top":false,"bottom":false,"right":false,"left":false});

  // Deposit Pages for Connect
  db.page.save({"name" : "Share in Zenodo", "route" : "/participate/deposit/zenodo", "type" : "share", "connect":false,"communities":true,"openaire":false,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});
  db.page.save({"name" : "Deposit your research - Learn How", "route" : "/participate/deposit/learn-how", "type" : "share", "connect":false,"communities":true,"openaire":true,"entities" : [], "top":false,"bottom":true,"right":false,"left":false});
  db.page.save({"name" : "Deposit Browse & Search repositories", "route" : "/participate/deposit/search", "type" : "share", "connect":false,"communities":true,"openaire":true,"entities" : [], "top":true,"bottom":false,"right":false,"left":false});

  // Linking Pages
  db.page.save({"name" : "Link", "route" : "/participate/claim", "type" : "link", "connect":false,"communities":true,"openaire":true,"entities" : [projectId], "top":false,"bottom":true,"right":false,"left":false});
	db.page.save({"name" : "Direct Link", "route" : "/participate/direct-claim", "type" : "link", "connect":false,"communities":true,"openaire":true,"entities" : [], "top":false,"bottom":true,"right":false,"left":false});
	db.page.save({"name" : "My claims", "route" : "/myclaims", "type" : "link", "connect":false,"communities":true,"openaire":true,"entities" : [], "top":true,"bottom":true,"right":true,"left":false});
	db.page.save({"name" : "Administrator claims", "route" : "/claims", "type" : "link", "connect":false,"communities":false,"openaire":true,"entities" : [], "top":true,"bottom":true,"right":true,"left":false});
	db.page.save({"name" : "Claims Project Manager", "route" : "/claims-project-manager", "type" : "link", "connect":false,"communities":false,"openaire":true,"entities" : [], "top":false,"bottom":false,"right":false,"left":false});

  link = db.page.find( { route: "/participate/claim" }).map( function(page) { return page._id.str; } ).toString()
	directLink = db.page.find( { route: "/participate/direct-claim" }).map( function(page) { return page._id.str; } ).toString()
}


function addHelpTextsInHomePage_connect() {
  communityID = db.community.find( { pid: "connect" }).map( function(community) { return community._id.str; } ).toString();
  home_page = db.page.find( { route: "/" }).map( function(page) { return page._id.str; } ).toString();

  bottom_1_content = "<div>Home news section</div>";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "bottom",
    "order" : 1,
    "content" : bottom_1_content,
    "isActive" : false,
    "isPriorTo" : false
  });

  bottom_2_content = "<div class=\"uk-background-norepeat uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color uk-margin-large-top\" style=\"background-color: #CFDEF1;\">\n" +
                "<div class=\"uk-container uk-section\">\n" +
                "<div class=\"uk-flex uk-flex-middle uk-padding uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-text-center uk-width-1-1@s uk-width-1-3@m uk-first-column\"><img src=\"assets/connect-assets/contact/1.png\" style=\"width:237px\" /></div>\n" +
                "\n" +
                "<div class=\"uk-width-expand\">\n" +
                "<div class=\"uk-text-bold uk-h4\">\n" +
                "<div>Let us help you develop a collaborative Open Science Gateway for your community. It is fast. It is reliable.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-medium\">Get in touch with our team to find out how.</div>\n" +
                "\n" +
                "<div class=\"uk-inline\"><a class=\"uk-button portal-button\" href=\"/contact-us\">CONTACT US</a></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "bottom",
    "order" : 2,
    "content" : bottom_2_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  left_content = "<div class=\"uk-section uk-background-norepeat uk-background-bottom-center uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\" style=\"background-image: url('assets/connect-assets/home/banner.jpg') !important;\">\n" +
                "<div class=\"uk-padding-remove-bottom uk-section\">\n" +
                "<div class=\"uk-position-cover\">&nbsp;</div>\n" +
                "\n" +
                "<div class=\"uk-position-relative uk-panel\">\n" +
                "<div class=\"uk-container uk-container-large uk-margin-top\">\n" +
                "<div class=\"uk-grid\">\n" +
                "<div class=\"uk-width-2-3@m uk-width-1-1@s\">\n" +
                "<div class=\"uk-h1\">Build an Open Research <strong>Gateway</strong> for your <strong>Community</strong></div>\n" +
                "\n" +
                "<h4 class=\"uk-margin-remove-top\">Turn Open Science into practice</h4>\n" +
                "\n" +
                "<div class=\"uk-h5 uk-margin-top\">Share and link your research results.<br />\n" +
                "Across organizations, across borders.<br />\n" +
                "Customized to your needs.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-width-1-1 uk-inline uk-margin-top\"><a class=\"uk-button portal-button\" href=\"/about/learn-how\">LEARN HOW</a></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "left",
    "order" : 1,
    "content" : left_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  top_1_content = "<div class=\"uk-section uk-background-muted\">\n" +
                "<div class=\"uk-container uk-container\">\n" +
                "<div class=\"uk-text-center uk-h2\">Open and FAIR science is our mission</div>\n" +
                "\n" +
                "<div class=\"uk-margin-medium-top\">\n" +
                "<div class=\"uk-child-width-1-3@m uk-child-width-1-1@s uk-grid-match uk-grid-small uk-grid\" uk-grid=\"\" uk-height-match=\".target\">\n" +
                "<div class=\"uk-first-column\">\n" +
                "<div class=\"uk-card uk-card-default uk-padding-small\">\n" +
                "<div class=\"uk-card-media-top uk-flex uk-flex-middle uk-flex-center uk-margin-small-top\">\n" +
                "<div class=\"target\" style=\"min-height: 108px;\"><img src=\"assets/connect-assets/home/1.png\" style=\"height:89px; width:100px\" /></div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-small-top\">\n" +
                "<div class=\"target\" style=\"min-height: 137.6px;\">\n" +
                "<h5 class=\"uk-text-center\">A Virtual Research Environment</h5>\n" +
                "\n" +
                "<div>An overlay platform making it easy to share, link, disseminate and monitor all your publications, data, software, methods. In one place.</div>\n" +
                "</div>\n" +
                "\n" +
                "<hr />\n" +
                "<div>\n" +
                "<div class=\"uk-text-uppercase text-center\">Features</div>\n" +
                "\n" +
                "<ul class=\"uk-list\">\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tAccess to OpenAIRE resources</li>\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tModerated, front-end linking</li>\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tCross-platform search</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div>\n" +
                "<div class=\"uk-card uk-card-default uk-padding-small\">\n" +
                "<div class=\"uk-card-media-top uk-flex uk-flex-middle uk-flex-center uk-margin-small-top\">\n" +
                "<div class=\"target\"><img src=\"assets/connect-assets/home/2.png\" style=\"height:108px; width:100px\" /></div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-small-top\">\n" +
                "<div class=\"target\" style=\"min-height: 137.6px;\">\n" +
                "<h5 class=\"uk-text-center\">Open Science in action</h5>\n" +
                "\n" +
                "<div>A time-saving bundle of services for researchers to effortlessly practice open science. An integral part of the European Open Science Cloud.</div>\n" +
                "</div>\n" +
                "\n" +
                "<hr />\n" +
                "<div>\n" +
                "<div class=\"uk-text-uppercase text-center\">Features</div>\n" +
                "\n" +
                "<ul class=\"uk-list\">\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"icon: check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tUse of OpenAIRE Guidelines</li>\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tDOIs via Zenodo</li>\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tEOSC Single Sign-On</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div>\n" +
                "<div class=\"uk-card uk-card-default uk-padding-small\">\n" +
                "<div class=\"uk-card-media-top uk-flex uk-flex-middle uk-flex-center uk-margin-small-top\">\n" +
                "<div class=\"target\" style=\"min-height: 108px;\"><img src=\"assets/connect-assets/home/3.png\" style=\"height:104px; width:120px\" /></div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-small-top\">\n" +
                "<div class=\"target\">\n" +
                "<h5 class=\"uk-text-center\">Customized to your needs</h5>\n" +
                "\n" +
                "<div>A Science Gateway with your own brand, rules for aggregation, text &amp; data mining, and presentation. Run by you via a simple, yet powerful backend administration tool.</div>\n" +
                "</div>\n" +
                "\n" +
                "<hr />\n" +
                "<div>\n" +
                "<div class=\"uk-text-uppercase text-center\">Features</div>\n" +
                "\n" +
                "<ul class=\"uk-list\">\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tAccess control</li>\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tAnalytics: rich set of indicators</li>\n" +
                "\t<li>\n" +
                "\t<div class=\"uk-border-circle uk-icon-button icon-button-small portal-icon-button uk-margin-small-right uk-icon\" uk-icon=\"check\"><br />\n" +
                "\t<svg data-svg=\"check\" height=\"20\" viewbox=\"0 0 20 20\" width=\"20\" xmlns=\"http://www.w3.org/2000/svg\"><polyline fill=\"none\" points=\"4,10 8,15 17,4\" stroke=\"#000\" stroke-width=\"1.1\"></polyline></svg></div>\n" +
                "\tLook &amp; feel to match your brand</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "top",
    "order" : 1,
    "content" : top_1_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  top_2_content = "<div class=\"uk-section uk-section-large uk-padding-remove-top uk-margin-top uk-background-norepeat uk-background-bottom-center uk-section-overlap uk-position-relative uk-preserve-color\" style=\"background-image: url('assets/connect-assets/home/background.png') !important;\">\n" +
                "<div class=\"uk-container uk-container-large\">\n" +
                "<div class=\"uk-slider\" uk-slider=\"velocity: 0;autoplay: true;autoplay-interval: 4000;pause-on-hover: false;center: true\">\n" +
                "<div class=\"uk-slider-container\">\n" +
                "<ul class=\"uk-slider-items uk-child-width-1-1\" style=\"transform: translateX(0px);\">\n" +
                "\t<li class=\"ng-star-inserted uk-active\" tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/home/gifs/deposit.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Find a repository to deposit your research outcome</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">This is OpenAIRE&rsquo;s key service for research communities, both established and emerging ones. Our service helps you reach out and engage all your researchers to practice open science out-of-the-box.</div>\n" +
                "\n" +
                "\t<div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\" href=\"/about/learn-how\">learn more</a></div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/home/gifs/link.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Link your research output with your community, funding, and other research products</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">This is OpenAIRE&rsquo;s key service for research communities, both established and emerging ones. Our service helps you reach out and engage all your researchers to practice open science out-of-the-box.</div>\n" +
                "\n" +
                "\t<div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\" href=\"/about/learn-how\">learn more</a></div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/home/gifs/overview.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">View community&#39;s overview at a glance</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">This is OpenAIRE&rsquo;s key service for research communities, both established and emerging ones. Our service helps you reach out and engage all your researchers to practice open science out-of-the-box.</div>\n" +
                "\n" +
                "\t<div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\" href=\"/about/learn-how\">learn more</a></div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/home/gifs/results.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Search &amp; browse your community&#39;s research products.</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">This is OpenAIRE&rsquo;s key service for research communities, both established and emerging ones. Our service helps you reach out and engage all your researchers to practice open science out-of-the-box.</div>\n" +
                "\n" +
                "\t<div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\" href=\"/about/learn-how\">learn more</a></div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/home/gifs/graph-analysis.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">View statistics for your community&#39;s research products.</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">This is OpenAIRE&rsquo;s key service for research communities, both established and emerging ones. Our service helps you reach out and engage all your researchers to practice open science out-of-the-box.</div>\n" +
                "\n" +
                "\t<div class=\"uk-inline\"><a class=\"uk-button portal-button uk-text-uppercase\" href=\"/about/learn-how\">learn more</a></div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "</ul>\n" +
                "\n" +
                "<ul class=\"uk-slider-nav uk-dotnav uk-flex-center uk-margin\">\n" +
                "\t<li class=\"uk-active\" uk-slider-item=\"0\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"1\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"2\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"3\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"4\">&nbsp;</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "top",
    "order" : 2,
    "content" : top_2_content,
    "isActive" : true,
    "isPriorTo" : false
  });
}

function addHelpTextsInHomePage_explore() {
  communityID = db.community.find( { pid: "openaire" }).map( function(community) { return community._id.str; } ).toString();
  home_page = db.page.find( { route: "/" }).map( function(page) { return page._id.str; } ).toString();

  bottom_1_content = "<div class=\"uk-section uk-section-small\" style=\"background: #FAFAFA 0% 0% no-repeat padding-box;\">\n" +
                "<div class=\"uk-container uk-container-large uk-margin-bottom\">\n" +
                "<div class=\"uk-flex uk-flex-middle uk-child-width-1-1@s uk-child-width-1-2@m uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-first-column\">\n" +
                "<div class=\"uk-text-secondary uk-h2 uk-margin-bottom\"><strong>Contribute</strong> to improve the<br />\n" +
                "<strong>OpenAIRE Research Graph</strong></div>\n" +
                "\n" +
                "<p>The graph is currently under <strong>pre-release consultation process</strong> which will last two months. Help us making the graph ready for its 1st production release by providing your feedback!</p>\n" +
                "\n" +
                "<p>You can provide feedback via <a href=\"https://trello.com/b/o1tEJ3rN/openaire-research-graph\" target=\"_blank\">Trello</a> or via opening a ticket in the <a href=\"https://www.openaire.eu/support/helpdesk\" target=\"_blank\">OpenAIRE Helpdesk</a> under the category OpenAIRE Services with the subject &quot;OpenAIRE Research Graph: &rdquo;. The graph can be explore and tested via this portal or via data dumps made available in <a href=\"https://zenodo.org/communities/openaire-research-graph\" target=\"_blank\">Zenodo.org</a>.</p>\n" +
                "\n" +
                "<p>Find information about the OpenAIRE Research Graph, how to test it and contribute to improving it on our <a href=\"https://www.openaire.eu/blogs/the-openaire-research-graph\" target=\"_blank\">blog</a>. You can also write to <a href=\"mailto:paolo.manghi@isti.cnr.it\">Paolo Manghi</a>, the OpenAIRE Technical Director, for additional details.</p>\n" +
                "\n" +
                "<div class=\"uk-flex uk-flex-middle uk-grid uk-child-width-1-2@s\"><a class=\"uk-margin-top\" href=\"https://www.openaire.eu/blogs/the-openaire-research-graph\" target=\"_blank\"><u>Instructions on providing feedback</u> </a>\n" +
                "\n" +
                "<div class=\"uk-margin-top uk-text-right uk-visible@l\"><a class=\"uk-button trello-button trello-button-blue uk-button-large\" href=\"https://trello.com/b/o1tEJ3rN/openaire-research-graph\" target=\"_blank\"><img class=\"trello-image\" src=\"assets/explore-assets/trello.svg\" /> <span class=\"uk-margin-small-left uk-margin-small-right\">Go to <strong>Trello</strong></span> &rarr; </a></div>\n" +
                "\n" +
                "<div class=\"uk-margin-top uk-text-right uk-hidden@m\"><a class=\"uk-button trello-button trello-button-blue uk-button-large\" href=\"https://trello.com/b/o1tEJ3rN/openaire-research-graph\" target=\"_blank\"><img class=\"trello-image\" src=\"assets/explore-assets/trello.svg\" /><span class=\"uk-margin-small-left uk-margin-small-right\">Go to <strong>Trello</strong></span> &rarr; </a></div>\n" +
                "\n" +
                "<div class=\"uk-margin-top uk-text-right uk-visible@m uk-hidden@l\"><a class=\"uk-button trello-button trello-button-blue uk-button-large\" href=\"https://trello.com/b/o1tEJ3rN/openaire-research-graph\" target=\"_blank\"><img class=\"trello-image uk-margin-right\" src=\"assets/explore-assets/trello.svg\" /> &rarr;</a></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div><img src=\"assets/explore-assets/trello-sc.png\" /></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "bottom",
    "order" : 1,
    "content" : bottom_1_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  bottom_2_content = "<div class=\"uk-section\" style=\"background: linear-gradient(180deg, #FAFAFA 50%, #FFFFFF 50%);\">\n" +
                "<div class=\"uk-container uk-container-small\">\n" +
                "<div class=\"uk-grid-margin uk-grid uk-grid-stack\" uk-grid=\"\">\n" +
                "<div class=\"uk-width-1-1@m uk-first-column\">\n" +
                "<div class=\"uk-margin uk-text-center uk-child-width-1-1 uk-grid-match uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-2@l uk-grid\" id=\"page#8\" uk-grid=\"\" uk-height-match=\"target: &gt; div &gt; div &gt; .card-text; row: false;\">\n" +
                "<div class=\"uk-first-column\">\n" +
                "<div class=\"el-item uk-card uk-card-default uk-card-medium uk-card-body\"><img alt=\"Share\" src=\"assets/explore-assets/share.svg\" />\n" +
                "<div class=\"el-content uk-margin uk-text-large\"><span class=\"uk-text-bold\">Deposit</span> your research</div>\n" +
                "\n" +
                "<div class=\"uk-margin-small-top uk-text-left card-text uk-flex uk-flex-center\">\n" +
                "<div class=\"uk-margin-left uk-margin-right\">Deposit in a repository of your choice.<br />\n" +
                "Select an OpenAIRE compatible<br />\n" +
                "repository (2.0 +) so that your research is linked to your funding information. Use Zenodo, a catch-all repository hosted by CERN to deposit all your research results (publications, data, software, etc.)</div>\n" +
                "</div>\n" +
                "<a class=\"uk-button portal-button uk-padding uk-padding-remove-vertical uk-margin-small-left ng-star-inserted uk-margin uk-margin-bottom uk-width-1-2\" href=\"/participate/deposit/learn-how\" type=\"submit\"> Deposit </a></div>\n" +
                "</div>\n" +
                "\n" +
                "<div>\n" +
                "<div class=\"el-item uk-card uk-card-default uk-card-medium uk-card-body\"><img alt=\"Link\" src=\"assets/explore-assets/link.svg\" />\n" +
                "<div class=\"el-content uk-margin uk-text-large\"><span class=\"uk-text-bold\">Link</span> your work</div>\n" +
                "\n" +
                "<div class=\"uk-margin-small-top uk-text-left card-text uk-flex uk-flex-center\">\n" +
                "<div class=\"uk-margin-left uk-margin-right\">Connect all your research.<br />\n" +
                "If you can&#39;t find your research results in OpenAIRE, don&#39;t worry! Use our Link Out service , that reaches out to many<br />\n" +
                "external sources via APIs, to connect<br />\n" +
                "your research results and claim them to your project.</div>\n" +
                "</div>\n" +
                "<a class=\"uk-button portal-button uk-padding uk-padding-remove-vertical uk-margin-small-left ng-star-inserted uk-margin uk-margin-bottom uk-width-1-2\" href=\"/participate/claim\" type=\"submit\"> Link </a></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "bottom",
    "order" : 2,
    "content" : bottom_2_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  left_content = "<div class=\"uk-section-muted uk-section uk-section-small\">\n" +
                "<div class=\"uk-container uk-container-large\">\n" +
                "<div class=\"uk-grid-margin uk-grid uk-grid-stack\" uk-grid=\"\">\n" +
                "<div class=\"uk-width-1-1@m uk-first-column\">\n" +
                "<div class=\"uk-margin uk-child-width-1-1 uk-grid-match uk-child-width-1-1@s uk-child-width-1-2@m\n" +
                "             uk-child-width-1-2@l uk-grid-large uk-grid uk-grid-divider ask-explore\" uk-grid=\"\">\n" +
                "<div class=\"uk-first-column uk-text-center\">\n" +
                "<div class=\"el-item uk-card uk-card-small uk-card-body\"><img alt=\"\" class=\"el-image\" src=\"assets/explore-assets/ask-explore.svg\" uk-svg=\"\" />\n" +
                "<h3 class=\"el-title uk-margin uk-h3\">Have more questions?</h3>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"questions\">\n" +
                "<div class=\"el-item uk-card uk-card-small uk-card-body\">\n" +
                "<ul class=\"uk-list uk-list-divider\">\n" +
                "\t<li><a href=\"https://www.openaire.eu/os-primers\" target=\"_blank\">Learn about open science policies and how to align</a></li>\n" +
                "\t<li><a href=\"https://www.openaire.eu/rdm-handbook\" target=\"_blank\">Learn more on how to manage your data in the open science era</a></li>\n" +
                "\t<li><a href=\"https://www.openaire.eu/guides\" target=\"_blank\">Find out how to use OpenAIRE to best serve your needs</a></li>\n" +
                "\t<li><a href=\"https://www.openaire.eu/webinars/\" target=\"_blank\">View our training material on a variety of related topics</a></li>\n" +
                "\t<li><a href=\"https://www.openaire.eu/contact-us/\" target=\"_blank\">Contact us</a></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "left",
    "order" : 1,
    "content" : left_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  right_content = "<div class=\"uk-h2\" style=\"color: white !important;\">Extracted <strong>Metadata Combined.</strong></div>\n" +
                "\n" +
                "<p>The OpenAIRE Research Graph is one of the largest open scholarly record collections worldwide, key in fostering Open Science and establishing its practices in the daily research activities. Conceived as a public and transparent good, populated out of data sources trusted by scientists, the Graph aims at bringing discovery, monitoring, and assessment of science back in the hands of the scientific community.<br />\n" +
                "<br />\n" +
                "Imagine a vast collection of research products all linked together, contextualised and openly available. For the past ten years OpenAIRE has been working to gather this valuable record. OpenAIRE is pleased to announce the beta release of its Research Graph, a massive collection of metadata and links between scientific products such as articles, datasets, software, and other research products, entities like organisations, funders, funding streams, projects, communities, and data sources.<br />\n" +
                "<br />\n" +
                "As of today, the OpenAIRE Research Graph aggregates around 450Mi metadata records with links collecting from 10,000 data sources trusted by scientists! After cleaning, deduplication, and fine-grained classification processes, they narrow down to ~100Mi publications, ~8Mi datasets, ~200K software research products, 8Mi other products linked together with semantic relations.<br />\n" +
                "<br />\n" +
                "More than 10Mi full-texts of Open Access publications are mined by algorithms to enrich metadata records with additional properties and links among research products, funders, projects, communities, and organizations. Thanks to the mining algorithm, the graph is completed with 480Mi semantic relations.</p>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "right",
    "order" : 1,
    "content" : right_content,
    "isActive" : true,
    "isPriorTo" : false
  });

  top_content = "<div class=\"uk-text-right uk-padding-small uk-padding-remove-vertical uk-margin-medium-top\"><a class=\"uk-button uk-button-large trello-button trello-button-white\" href=\"https://trello.com/b/o1tEJ3rN/openaire-research-graph\" target=\"_blank\"><img class=\"trello-image\" src=\"assets/explore-assets/trello-2.svg\" /> <span class=\"uk-margin-small-left\">Give us <strong>feedback</strong></span> </a></div>\n";

  db.pageHelpContent.save({
    "page" : home_page,
    "community" : communityID,
    "placement" : "top",
    "order" : 1,
    "content" : top_content,
    "isActive" : true,
    "isPriorTo" : false
  });
}

function addHelpTextsInAboutLearnHowPage() {
  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  about_page = db.page.find( { route: "/about/learn-how" }).map( function(page) { return page._id.str; } ).toString()

  top_1_content = "<div class=\"uk-section uk-section-large uk-padding-remove-top uk-background-norepeat uk-background-bottom-center uk-section-overlap uk-position-relative uk-preserve-color\" style=\" min-height: calc(7.89999px + 60vh);  background-image: url('assets/connect-assets/about/background.png') !important;\">\n" +
                "<div class=\"uk-container uk-container-large uk-section uk-margin-top\">\n" +
                "<div class=\"uk-margin-large-top uk-grid\">\n" +
                "<h1 class=\"uk-width-1-1 font-41 uk-text-bold\">Learn the process</h1>\n" +
                "\n" +
                "<div class=\"uk-width-1-2@l uk-width-1-1@s uk-h5 uk-margin-top\">Build a <strong>Gateway to your community&#39;s</strong> open and linked research outcomes. Customized to your needs.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-large-top uk-flex uk-child-width-1-3@m uk-child-width-1-1@s uk-flex-center uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-flex uk-child-width-1-1@m uk-child-width-1-2@s uk-grid uk-first-column uk-grid-stack\" uk-grid=\"\">\n" +
                "<div class=\"uk-first-column\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">1. Understanding your needs</h5>\n" +
                "\n" +
                "<div>First, we learn about your requirements and challenges. We help you understand Open Science practices within EOSC and together we&rsquo;ll talk about how OpenAIRE RCD fits as a solution.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-visible@m uk-grid-margin uk-first-column\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">4. Roll out the service</h5>\n" +
                "\n" +
                "<div>We jointly roll out your new Community Gateway. You take over the business operations and start engaging your researchers, we take care of the smooth operation of the e-service.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-hidden@m\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">2. Develop a pilot</h5>\n" +
                "\n" +
                "<div>How do you work today, and how would you like to work tomorrow? We translate your needs into rules and processes and we configure operational OpenAIRE services. By the end of this phase, we&rsquo;ll have defined the basic configuration of your Community Gateway.</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-text-center\"><img src=\"assets/connect-assets/about/cycle.png\" /></div>\n" +
                "\n" +
                "<div class=\"uk-flex uk-child-width-1-1@m uk-child-width-1-2@s uk-grid uk-grid-stack\" uk-grid=\"\">\n" +
                "<div class=\"uk-visible@m uk-first-column\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">2. Develop a pilot</h5>\n" +
                "\n" +
                "<div>How do you work today, and how would you like to work tomorrow? We translate your needs into rules and processes and we configure operational OpenAIRE services. By the end of this phase, we&rsquo;ll have defined the basic configuration of your Community Gateway.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-hidden@m uk-visible@s\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">4. Roll out the service</h5>\n" +
                "\n" +
                "<div>We jointly roll out your new Community Gateway. You take over the business operations and start engaging your researchers, we take care of the smooth operation of the e-service.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-grid-margin uk-first-column\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">3. Test and Validate</h5>\n" +
                "\n" +
                "<div>You validate and test your new Community Gateway (portal) with your experts and community to ensure all workflows are in place and quality of data meets your standards. If needed, we work together in another iteration to further refine and adapt to your needs.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-hidden@s uk-visible@xs\">\n" +
                "<h5 class=\"uk-text-bold uk-margin-small-bottom\">4. Roll out the service</h5>\n" +
                "\n" +
                "<div>We jointly roll out your new Community Gateway. You take over the business operations and start engaging your researchers, we take care of the smooth operation of the e-service.</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-width-1-1 uk-text-center uk-text-large uk-margin-large-top\"><a class=\"uk-button portal-button uk-text-uppercase\" href=\"/about/learn-in-depth\">Learn more details</a></div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 1,
    "content": top_1_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_2_content = "<div class=\"uk-background-norepeat uk-section uk-background-bottom-center uk-section-overlap uk-position-relative uk-preserve-color\">\n" +
                "<div class=\"uk-container uk-container-large\">\n" +
                "<div class=\"uk-slider\" uk-slider=\"velocity: 0;autoplay: true;autoplay-interval: 4000;pause-on-hover: false;center: true\">\n" +
                "<div class=\"uk-slider-container\">\n" +
                "<ul class=\"uk-slider-items uk-child-width-1-1\" style=\"transform: translateX(0px);\">\n" +
                "\t<li class=\"ng-star-inserted uk-active\" tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/about/gifs/profile.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Profile</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">Edit community information, change logo url, add community managers or organizations related to community.</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/about/gifs/content.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Content</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">Manage projects, content providers, subjects and zenodo communities that are related to the research community.</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/about/gifs/statistics.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Statistics &amp; Charts</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">Manage statistical numbers &amp; charts that will be displayed in the community overview and graph analysis views.</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/about/gifs/links.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Links</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">Manage user claims related to the research community.</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/about/gifs/help.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Help texts</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">Add or edit help text in research community pages.</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li tabindex=\"-1\">\n" +
                "\t<div class=\"uk-flex uk-padding uk-child-width-1-2@m uk-child-width-1-1@s uk-grid\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-first-column\"><img class=\"uk-border-rounded uk-box-shadow-large\" src=\"assets/connect-assets/about/gifs/users.gif\" /></div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-top\">\n" +
                "\t<div>\n" +
                "\t<div class=\"uk-text-bold uk-h4\">Users</div>\n" +
                "\n" +
                "\t<div class=\"uk-margin-medium\">Invite more users to subscribe, manage community subscribers, your personal info and notification settings.</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "</ul>\n" +
                "\n" +
                "<ul class=\"uk-slider-nav uk-dotnav uk-flex-center uk-margin\">\n" +
                "\t<li class=\"uk-active\" uk-slider-item=\"0\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"1\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"2\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"3\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"4\">&nbsp;</li>\n" +
                "\t<li uk-slider-item=\"5\">&nbsp;</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 2,
    "content": top_2_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_3_content = "<div class=\"uk-background-norepeat uk-background-cover uk-section-overlap uk-position-relative uk-preserve-color\" style=\"background-color: #CFDEF1;\">\n" +
                "<div class=\"uk-container uk-section\">\n" +
                "<div class=\"uk-flex uk-flex-middle uk-padding uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-width-expand uk-first-column\">\n" +
                "<div class=\"uk-text-bold uk-h4\">We look forward to working together and helping you unlock the full potential of your research community through open science.</div>\n" +
                "\n" +
                "<div class=\"uk-margin-medium\">Get in touch with our team to find out how.</div>\n" +
                "\n" +
                "<div class=\"uk-inline\"><a class=\"uk-button portal-button\" href=\"/contact-us\">CONTACT US</a></div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-text-center uk-width-1-1@s uk-width-1-3@m\"><img src=\"assets/connect-assets/contact/3.png\" style=\"width:263px\" /></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 3,
    "content": top_3_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_4_content = "<div class=\"uk-container uk-section-small\">\n" +
                "<div class=\"uk-margin-medium-bottom uk-text-bold uk-h4\">Frequently Asked Questions</div>\n" +
                "\n" +
                "<ul class=\"uk-accordion\" uk-accordion=\"\">\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">What is a &quot;Research community&quot;?</a>\n" +
                "\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>A research community is a community of practice that gathers together researchers and practitioners with common research interests (e.g. a research discipline or a specific research topic) and, possibly, a decisional board. In some cases, a community builds and grows in the context of a research infrastructure that provides services and tools supporting the research activities of the members of a community. In other cases, a community builds and grows in the context of one or more research projects, forming what we call a &ldquo;research initiative&rdquo;.</p>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">What is the difference between a Community Gateway and the Research Community Dashboard (RCD)?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>The RCD offers on demand Community Gateways that communities can customize to fit their sharing, publishing and discovery needs. Through the RCD, gateway managers can configure the respective Community Gateway by providing (i) the criteria identifying the subset of the OpenAIRE Research Graph that pertains to the community, and (ii) the community statistics to be made public or private.</p>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">How can Research Community thematic services integrate with the Research Community Dashboard (RCD)?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>Once a Community Gateway has been created, several thematic service integration patterns are possible:</p>\n" +
                "\n" +
                "\t<ol>\n" +
                "\t\t<li><span class=\"uk-text-bold\">Community-specific repository:</span> OpenAIRE can guide your repository managers on how to make the repository &ldquo;OpenAIRE-compliant&rdquo; [LINK to proper page in the OpenAIRE portal]. The gateway managers can configure the gateway, so that all metadata records collected from the repository are available in the gateway. If the repository manager wants to get the enrichments OpenAIRE applies to the records, he/she can activate the Broker service [link to the proper page in the portal] using the Content Provider Dashboard [link to provide].</li>\n" +
                "\t\t<li><span class=\"uk-text-bold\">Continuous publishing:</span> thematic services can, once authorised by the user, publish their outputs (e.g. datasets, digital experiments) on behalf of the user. Thematic services and digital experiment platforms can be easily enhanced with a publishing step by integrating the Zenodo API[link to zenodo]. To know more, <a class=\"uk-link\" href=\"https://doi.org/10.5281/zenodo.1314672\" target=\"_blank\"> check out the pilot we have designed with the EPOS-IT infrastructure. </a></li>\n" +
                "\t\t<li><span class=\"uk-text-bold\">Community web site:</span> you can use the OpenAIRE Search API [link to api doc] to show the research products relevant to your community in your own website.</li>\n" +
                "\t</ol>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">What does my Research Community gain in terms of Open Science?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content uk-grid\" hidden=\"\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-width-1-1 uk-first-column\"><span class=\"uk-text-bold\">Discovery and reproducibility:</span> Your community will have a single entry point where all research products, of any type (literature, datasets, software, workflows, protocols), are gathered together and linked with semantic relationships. Researchers can find a research product in the gateway (e.g. a research dataset) and easily find out everything related to it and that is needed to re-use it, but also to replicate and reproduce a research activity (e.g. software used to process the dataset, the journal article where the research is described).</div>\n" +
                "\n" +
                "\t<div class=\"uk-width-1-1 uk-grid-margin uk-first-column\"><span class=\"uk-text-bold\">Sharing:</span> scientific literature is only the tip of the iceberg. With a Community Gateway you say to the members of your community that everything they produce is relevant and that they should get credit for it.</div>\n" +
                "\n" +
                "\t<div class=\"uk-width-1-1 uk-grid-margin uk-first-column\"><span class=\"uk-text-bold\">Credit for all:</span> Making research data, software, methods and scientific workflows available &ldquo;as soon as possible&rdquo; is one of the &ldquo;mantra&rdquo; of Open Science. If your community has integrated Zenodo into a digital experiment platform, researchers no longer bear the burden of manually publishing their research products, while it is also easier for them to publish different versions, which reflect different stages of their activities.</div>\n" +
                "\n" +
                "\t<div class=\"uk-width-1-1 uk-grid-margin uk-first-column\"><span class=\"uk-text-bold\">Awareness:</span> Thanks to dedicated pages of the Community Gateway you can suggest best practices to the researchers of the community. Open Science experts of OpenAIRE will be available to assist you.</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">How is the service part of the European Open Science Cloud?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>OpenAIRE has onboarded in the EOSC Market Place both the RCD, as a service that offers on-demand Community Gateways, and the Gateways as services openly accessible by researchers to explore specific domains of interest.</p>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">Who owns the gateway?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>The community owns the portal, OpenAIRE operates it.</p>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">How is the gateway operated?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>The gateway is hosted on a virtual machine operated by OpenAIRE and hosted at the ICM data centre [link to ICM data centre web site]. The Gateway is built with Angular and uses RESTful APIs, built with Java and the Spring Framework, to access the OpenAIRE Research Graph. OpenAIRE is responsible to keep the back-end and front-end services up-to-date and monitors the uptime and usage of the gateway, via the matomo web analytics platform. Aggregated usage data is used by the OpenAIRE team for reporting purposes and does not include any personal data.</p>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">What is the SLA of the service?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<p>The RCD and the gateways are subject to the generic <a class=\"uk-link\" href=\"https://www.openaire.eu/service-level-agreement\" target=\"_blank\">OpenAIRE services SLA</a>.</p>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">What are the costs?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content\" hidden=\"\">\n" +
                "\t<div>Costs include hardware, hardware management, application administration, software maintenance, and text mining.</div>\n" +
                "\n" +
                "\t<div>Rates will be applied depending on the kind of functionality required, which may be limited to out-of-the-box text mining services or include custom text-mining services. For 2019-2020 the predicted cost is 17,000EUR per year for the basic implementation and 20,000EUR per year to include customized text mining.</div>\n" +
                "\n" +
                "\t<div>Researchers of the community shall not pay to access the Community Gateway.</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "\t<li><a class=\"uk-accordion-title\" href=\"#\">In what other areas can my Research community collaborate with OpenAIRE?</a>\n" +
                "\t<div aria-hidden=\"true\" class=\"uk-accordion-content uk-grid uk-grid-stack\" hidden=\"\" uk-grid=\"\">\n" +
                "\t<div class=\"uk-width-1-1\">\n" +
                "\t<div style=\"font-style: italic\">Open Science training</div>\n" +
                "\tHelp us training on Open Science students and researchers of your community by organising a workshop. <a class=\"uk-link\" href=\"https://www.openaire.eu/workshops-page\" target=\"_blank\">Check for ideas and materials</a>.</div>\n" +
                "\n" +
                "\t<div class=\"uk-width-1-1\">\n" +
                "\t<div style=\"font-style: italic\">Train the trainer</div>\n" +
                "\tJoin the <a class=\"uk-link\" href=\"https://www.openaire.eu/cop-training\" target=\"_blank\">OpenAIRE Community of Practice</a>, an informal network to share training experience on Open Science, research and e-infrastructures.</div>\n" +
                "\n" +
                "\t<div class=\"uk-width-1-1\">\n" +
                "\t<div style=\"font-style: italic\">Citizen Science</div>\n" +
                "\tDo you collaborate with schools? Learn more about the <a class=\"uk-link\" href=\"https://www.openaire.eu/citizen-science-activities-in-openaire\" target=\"_blank\">OpenAIRE activities for Citizen Science</a>.</div>\n" +
                "\n" +
                "\t<div class=\"uk-width-1-1\">\n" +
                "\t<div style=\"font-style: italic\">Open Innovation Programme</div>\n" +
                "\tSpread the word about the OpenAIRE <a class=\"uk-link\" href=\"https://www.openaire.eu/open-innovation-in-openaire\" target=\"_blank\">Open Innovation programme</a> for SMEs, start-ups and research labs. The programme&rsquo;s goal is to discover, support and fund innovative ideas and implementations of software in the Open Science domain.</div>\n" +
                "\t</div>\n" +
                "\t</li>\n" +
                "</ul>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": about_page,
    "community": communityID,
    "placement": "top",
    "order": 4,
    "content": top_4_content,
    "isActive": true,
    "isPriorTo": false
  });
}

function addHelpTextsInAboutLearnInDepthPage() {
  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  aboutLearnInDepth_page = db.page.find({route: "/about/learn-in-depth"}).map(function (page) { return page._id.str; }).toString();

  top_1_content = "<div class=\"uk-background-norepeat uk-background-cover uk-section uk-padding-remove-bottom uk-section-overlap uk-position-relative uk-preserve-color\" style=\"min-height: calc(7.89999px + 60vh); background-image: url('assets/connect-assets/cloud/background.png') !important;\">\n" +
                "<div class=\"uk-container uk-container-large uk-section uk-margin-top uk-padding-remove-top\">\n" +
                "<div class=\"uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-margin-large-top uk-width-3-4@m uk-width-1-1@s uk-first-column\">\n" +
                "<h1 class=\"uk-width-3-4  uk-margin-medium-bottom\">Let&rsquo;s set up a Gateway for your Community <strong>Together</strong></h1>\n" +
                "\n" +
                "<div class=\"uk-width-4-5@m uk-width-1-1@s uk-h5\">\n" +
                "<div class=\"uk-margin-bottom\">You don&rsquo;t have to go alone.</div>\n" +
                "\n" +
                "<div>We work with you in <strong>4 collaborative steps</strong> to identify your needs, putting in practice our expertise on open science so you get the most out of OpenAIRE&rsquo;s operational services.</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-large-top uk-width-expand\"><img src=\"assets/connect-assets/banner/together.png\" style=\"height:285px; width:308px\" /></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": aboutLearnInDepth_page,
    "community": communityID,
    "placement": "top",
    "order": 1,
    "content": top_1_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_2_content = "<div class=\"uk-container uk-container-large uk-section uk-padding-remove-top\">\n" +
                "<ul class=\"uk-breadcrumb\">\n" +
                "\t<li><a class=\"router-link-active\" href=\"/about/learn-how\">About</a></li>\n" +
                "\t<li><span class=\"active\">Learn in-depth</span></li>\n" +
                "</ul>\n" +
                "\n" +
                "<div class=\"uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-width-1-6 uk-position-relative uk-flex uk-flex-column uk-first-column\"><img class=\"uk-align-center\" src=\"assets/connect-assets/about/1.png\" style=\"width:100px\" /><img class=\"uk-align-center\" src=\"assets/connect-assets/sketch_line_arrow.svg\" /></div>\n" +
                "\n" +
                "<div class=\"uk-width-expand\">\n" +
                "<div class=\"uk-margin-medium uk-text-bold uk-h4\">1. Analyse your needs</div>\n" +
                "\n" +
                "<div style=\"font-style: italic;\">Identify the scope and goals. Understand open science practices within EOSC and the OpenAIRE services</div>\n" +
                "\n" +
                "<p>In this stage, you get to talk to the OpenAIRE team. Share your expectations with us and let us give you all the details about the operational OpenAIRE services, which will be integrated into the Gateway for your community.</p>\n" +
                "\n" +
                "<p>Here are the most important questions that the OpenAIRE team will ask you, in order to understand your scope and goals:</p>\n" +
                "\n" +
                "<ul class=\"uk-list uk-list-bullet\">\n" +
                "\t<li>Do you want a gateway, where researchers can have access to all research products of a discipline? Do you want a gateway that gathers any research outcome, produced thanks to the funding and services of a given research infrastructure?</li>\n" +
                "\t<li>Is your community (in)formally organized in sub-communities? Would you like to browse research products and get statistics also for these sub-communities? For example, the European Grid Infrastructure (EGI) features &ldquo;virtual organizations&rdquo; that represent discipline-specific communities and/or specific research projects. The research infrastructure DARIAH, on the other hand, is organised in national nodes (e.g. DARIAH-IT, DARIAH-DE).</li>\n" +
                "\t<li>How can the OpenAIRE team identify the research products of your community, among all those available in the OpenAIRE Graph? Through a series of steps: set of keywords, acknowledgment statements, set of projects, set of repositories, etc. This can be partial and provisional information that will serve as a starting point to the OpenAIRE team. You will be able to refine and update this information, in the second phase &ldquo;Develop a pilot&rdquo;.</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-grid\" uk-grid=\"\" uk-height-match=\"\">\n" +
                "<div class=\"uk-width-1-6 uk-position-relative uk-flex uk-flex-column uk-first-column\"><img class=\"uk-align-center\" src=\"assets/connect-assets/about/2.png\" style=\"width:100px\" /><img class=\"uk-align-center\" src=\"assets/connect-assets/sketch_line_arrow.svg\" /></div>\n" +
                "\n" +
                "<div class=\"uk-width-expand\">\n" +
                "<div class=\"uk-margin-medium uk-text-bold uk-h4\">2. Develop a pilot</div>\n" +
                "\n" +
                "<div style=\"font-style: italic;\">We translate your needs into rules and processes and we configure operational OpenAIRE services.</div>\n" +
                "\n" +
                "<p>Based on the information gathered in phase 1 &ldquo;Analyse your needs&rdquo;, the OpenAIRE team will set up a pilot Gateway. We will configure the OpenAIRE mining algorithms to identify research products of the OpenAIRE Graph that are relevant to your community. Those, together with some basic statistics, will be available in the pilot version of the Community Gateway that will be deployed on the OpenAIRE BETA infrastructure.</p>\n" +
                "\n" +
                "<p>The OpenAIRE team will give you a demo of the Community Gateway, with details on how to refine and update the configuration of the Community Gateway, both in terms of criteria for including research products and in terms of logo and visible portal pages.</p>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-grid\" uk-grid=\"\" uk-height-match=\"\">\n" +
                "<div class=\"uk-width-1-6 uk-position-relative uk-flex uk-flex-column uk-first-column\"><img class=\"uk-align-center\" src=\"assets/connect-assets/about/3.png\" style=\"width:100px\" /><img class=\"uk-align-center\" src=\"assets/connect-assets/sketch_line_arrow_large.svg\" /></div>\n" +
                "\n" +
                "<div class=\"uk-width-expand\">\n" +
                "<div class=\"uk-margin-medium uk-text-bold uk-h4\">3. Test and Validate</div>\n" +
                "\n" +
                "<div style=\"font-style: italic;\">You validate and test your new Community Gateway (portal). If needed, we further refine and adapt to your needs</div>\n" +
                "\n" +
                "<p>Upon the completion of phase 2, take the time you need to test all its features, from search and browse for research products, to addition/removal of statistics from the portal. You can report any issue you might find and ask questions directly to the dedicated OpenAIRE team, via a specially designed collaboration tool.</p>\n" +
                "\n" +
                "<p>Typically, this phase takes some months, as you will have to go through certain procedures. Change the configuration of the criteria to include research products, wait for the new configuration to be applied on the OpenAIRE graph and validate the results, before you actually decide that the coverage of research products for your community is adequate.</p>\n" +
                "\n" +
                "<p>For some communities, the OpenAIRE team may also be able to implement dedicated mining algorithms (e.g. to find acknowledgement statements to your community/infrastructure in the full-texts of research articles) that may require several rounds of application, validation, and fine-tuning, before it reaches a high precision and recall. Your feedback is very important to minimize the effort and time needed for this process to complete.</p>\n" +
                "\n" +
                "<div class=\"uk-width-1-1 uk-text-center uk-margin-medium \"><img src=\"assets/connect-assets/OpenAIRE-RCD_howtos.png\" style=\"height:auto; width:auto\" /></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-grid\" uk-grid=\"\" uk-height-match=\"\">\n" +
                "<div class=\"uk-width-1-6 uk-position-relative uk-first-column\"><img class=\"uk-align-center\" src=\"assets/connect-assets/about/4.png\" style=\"width:100px\" /></div>\n" +
                "\n" +
                "<div class=\"uk-width-expand\">\n" +
                "<div class=\"uk-margin-medium  uk-text-bold uk-h4\">4. Roll out the service</div>\n" +
                "\n" +
                "<div style=\"font-style: italic;\">We jointly roll out your new portal. You take over the business operations and start engaging your researchers</div>\n" +
                "\n" +
                "<p>Here we are: the coverage of research products is good, interesting statistics and charts have been selected, and the portal pages available for end-users are ready. We can roll out the Community Gateway and make it available to all the researchers of the community!</p>\n" +
                "\n" +
                "<p>You, as a Community manager, become the main &ldquo;promoter&rdquo; of the Community Gateway. Engage the researchers of your community and, when applicable, inform the managers of the research infrastructure about the availability of tools for impact monitoring.</p>\n" +
                "\n" +
                "<p>Remember that you will still be able to change the configuration of the Community Gateway in order to address any issue that may arise and to follow the evolution of the community (e.g. a new project or a new content provider that was not previously available in OpenAIRE).</p>\n" +
                "\n" +
                "<p>Remember that you don&rsquo;t have to go alone: the dedicated issue tracker you used in the &ldquo;Test and Validate&rdquo; phase is always available for you to contact the OpenAIRE team and ask for support.</p>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": aboutLearnInDepth_page,
    "community": communityID,
    "placement": "top",
    "order": 2,
    "content": top_2_content,
    "isActive": true,
    "isPriorTo": false
  });

  top_3_content = "<div class=\"uk-background-norepeat uk-background-cover uk-section-secondary uk-section-overlap uk-position-relative uk-preserve-color\" style=\"background-color: #CFDEF1;\">\n" +
                "<div class=\"uk-container uk-section\">\n" +
                "<div class=\"uk-flex uk-flex-middle uk-padding uk-grid\" uk-grid=\"\">\n" +
                "<div class=\"uk-text-center uk-width-1-1@s uk-width-1-3@m uk-first-column\"><img src=\"assets/connect-assets/contact/2.png\" style=\"width:329px\" /></div>\n" +
                "\n" +
                "<div class=\"uk-width-expand\">\n" +
                "<div class=\"uk-text-bold uk-h4\">\n" +
                "<div>Let us help you develop a collaborative Open Science Gateway for your community. It is fast. It is reliable.</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-medium\">Get in touch with our team to find out how.</div>\n" +
                "\n" +
                "<div class=\"uk-inline\"><a class=\"uk-button portal-button\" href=\"/contact-us\">CONTACT US</a></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": aboutLearnInDepth_page,
    "community": communityID,
    "placement": "top",
    "order": 3,
    "content": top_3_content,
    "isActive": true,
    "isPriorTo": false
  });
}

function addHelpTextsInContactUsPage() {
  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  contactUs_page = db.page.find({route: "/contact-us"}).map(function (page) { return page._id.str; }).toString();

  right_content = "<div class=\"uk-margin-auto-top uk-margin-remove-bottom  uk-text-bold uk-h4\">OpenAIRE gives you the virtual environment and services designed for your community to:</div>\n" +
                "\n" +
                "<ul class=\"uk-list uk-list-divider uk-padding uk-padding-remove-left uk-margin-auto-top\">\n" +
                "\t<li>\n" +
                "\t<h5><span class=\"uk-text-bold\">Create and Manage</span> your Community Gateway</h5>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t<h5><span class=\"uk-text-bold\">Access, share and link</span> together all your research</h5>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t<h5><span class=\"uk-text-bold\">Monitor and report</span> your community&#39;s progress</h5>\n" +
                "\t</li>\n" +
                "</ul>\n";

  db.pageHelpContent.save({
    "page": contactUs_page,
    "community": communityID,
    "placement": "right",
    "order": 1,
    "content": right_content,
    "isActive": true,
    "isPriorTo": false
  });
}

function addHelpTextsInContentPolicyPage() {
  print("add help contents in Content Policy Page for community connect\n");

  communityID = db.community.find({pid: "connect"}).map(function (community) { return community._id.str; }).toString();
  content_page = db.page.find({route: "/content"}).map(function (page) { return page._id.str; }).toString();

  top_content = "<div class=\"uk-margin-top tm-middle uk-container\" id=\"tm-main\">\n" +
                "<div class=\"uk-container  uk-margin-bottom\">\n" +
                "<div class=\"uk-article-title custom-article-title\">Content policy</div>\n" +
                "\n" +
                "<div>\n" +
                "<p>OpenAIRE builds an open scholarly graph of research products (publications, datasets, software and other types of research products), linked to each other and to fundings, projects and organizations. The information used to build the graph is collected from the OpenAIRE network of content providers and is inferred by OpenAIRE algorithms. OpenAIRE algorithms are capable of detecting duplicates and mining information from the full-texts of Open Access publications. For more details about the construction of the graph, please check <a href=\"https://beta.openaire.eu/aggregation-and-content-provision-workflows\" target=\"_blank\">here</a>.</p>\n" +
                "\n" +
                "<p>Thanks to the Research Community Dashboard (RCD), researchers have a dedicated view of the OpenAIRE graph where only the research products relevant to the community are searchable and browsable.<br />\n" +
                "There are several ways to decide if a research product is relevant to a given community:</p>\n" +
                "\n" +
                "<p class=\"uk-margin-left\"><strong>Links:</strong> Users can &quot;claim&quot; that a research result is relevant to a community via the <a href=\"/participate/claim\">Link</a> functionality of the Research community dashboard.<br />\n" +
                "For more details about linking functionality, please check <a href=\"https://beta.openaire.eu/linking\" target=\"_blank\">here</a>.</p>\n" +
                "\n" +
                "<div class=\"uk-margin-left uk-margin-top\"><strong>OpenAIRE algorithms:</strong> Community managers can configure the OpenAIRE algorithms, in order to automatically assign research products to a community based on:\n" +
                "\n" +
                "<div class=\"uk-margin-left\">\n" +
                "<ul class=\"uk-list uk-list-bullet  \">\n" +
                "\t<li>The content providers they have been collected from.<br />\n" +
                "\tExamples: all research products collected from &quot;NeuroVault&quot; are relevant to the Neuroinformatics community; all research products collected from the LINDAT/CLARIN repository are relevant to the CLARIN research initiative.</li>\n" +
                "\t<li>The projects they have been produced in.<br />\n" +
                "\tExamples: all research products of the project &quot;Tara Mediterranee&quot; are relevant to the European Marine Science community; all research products of the project &quot;ARIADNE&quot; are relevant to the Digital Humanities and Cultural Heritage community.</li>\n" +
                "\t<li>The subjects and keywords (only for RCD serving research communities).<br />\n" +
                "\tExamples: all research products having &quot;SDG11 - Sustainable cities and communities&quot; among the subjects are relevant to the Greek Sustainable Development Solutions Network community; all research products with subject &quot;agriculture&quot; are relevant to the AgInfra community</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"uk-margin-left uk-margin-top\"><strong>Acknowledgement statements:</strong> Acknowledgement statements found in full-texts (only for RCDs serving research infrastructures/initiatives)</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n";

  db.pageHelpContent.save({
    "page": content_page,
    "community": communityID,
    "placement": "top",
    "order": 1,
    "content": top_content,
    "isActive": true,
    "isPriorTo": false
  });
}



function createCommunity(name,communityPid){
  var communitySaved =  db.community.find( { pid: communityPid }).map( function(community) { return community._id.str; } ).toString()
  if(communitySaved){
    print("\n\n Community \'"+name +"\' exists\n\n");
    return ;
  }
  print("\n\n Create community \n\n");
  print("Name:"+name);
  print("communityPid:"+communityPid);

  // Other Pages
  home = db.page.find( { route: "/" }).map( function(page) { return page._id.str; } ).toString()

  // Other Pages for Connect
  about = db.page.find( { route: "/about/learn-how" }).map( function(page) { return page._id.str; } ).toString()
  aboutLearnInDepth = db.page.find( { route: "/about/learn-in-depth" }).map( function(page) { return page._id.str; } ).toString()
  contactUs = db.page.find( { route: "/contact-us" }).map( function(page) { return page._id.str; } ).toString()
  contentPolicy = db.page.find( { route: "/content" }).map( function(page) { return page._id.str; } ).toString()

  curators = db.page.find( { route: "/curators" }).map( function(page) { return page._id.str; } ).toString()
  organizations = db.page.find( { route: "/organizations" }).map( function(page) { return page._id.str; } ).toString()
  invite = db.page.find( { route: "/invite" }).map( function(page) { return page._id.str; } ).toString()
  myCommunities = db.page.find( { route: "/myCommunities" }).map( function(page) { return page._id.str; } ).toString();

  // Testing Page for help contents (Connect)
  helperTest = db.page.find( { route: "/helper-test" }).map( function(page) { return page._id.str; } ).toString()

  // Other Pages for Explore
  mailPreferences = db.page.find( { route: "/mail-preferences" }).map( function(page) { return page._id.str; } ).toString()

  // Landing Pages
  publicationLanding = db.page.find( { route: "/search/publication" }).map( function(page) { return page._id.str; } ).toString()
  datasetLanding = db.page.find( { route: "/search/dataset" }).map( function(page) { return page._id.str; } ).toString()
  softwareLanding = db.page.find( { route: "/search/software" }).map( function(page) { return page._id.str; } ).toString()
  orpLanding = db.page.find( { route: "/search/other" }).map( function(page) { return page._id.str; } ).toString();
  projectLanding = db.page.find( { route: "/search/project" }).map( function(page) { return page._id.str; } ).toString()
  organizationLanding = db.page.find( { route: "/search/organization" }).map( function(page) { return page._id.str; } ).toString()
  dataproviderLanding = db.page.find( { route: "/search/dataprovider" }).map( function(page) { return page._id.str; } ).toString()
  projectReport = db.page.find( { route: "/project-report" }).map( function(page) { return page._id.str; } ).toString()

  // Search Pages
  searchFind = db.page.find( { route: "/search/find" }).map( function(page) { return page._id.str; } ).toString()

  searchCommunities = db.page.find( { route: "/search/find/communities" }).map( function(page) { return page._id.str; } ).toString()

  searchResearchOutcomes = db.page.find( { route: "/search/find/research-outcomes" }).map( function(page) { return page._id.str; } ).toString()
  searchProject = db.page.find( { route: "/search/find/projects" }).map( function(page) { return page._id.str; } ).toString()
  searchOrganization = db.page.find( { route: "/search/find/organizations" }).map( function(page) { return page._id.str; } ).toString()
  searchDataprovider = db.page.find( { route: "/search/find/dataproviders" }).map( function(page) { return page._id.str; } ).toString()

  // Advanced Search Pages
  advancedSearchResearchOutcomes = db.page.find( { route: "/search/advanced/research-outcomes" }).map( function(page) { return page._id.str; } ).toString()
  advancedSearchProject = db.page.find( { route: "/search/advanced/projects" }).map( function(page) { return page._id.str; } ).toString()
  advancedSearchOrganization = db.page.find( { route: "/search/advanced/organizations" }).map( function(page) { return page._id.str; } ).toString()
  advancedSearchDataprovider = db.page.find( { route: "/search/advanced/dataproviders" }).map( function(page) { return page._id.str; } ).toString()

  // Search Content Providers Pages
  searchJournals = db.page.find( { route: "/search/journals" }).map( function(page) { return page._id.str; } ).toString()
  searchEntityRegistries = db.page.find( { route: "/search/entity-registries" }).map( function(page) { return page._id.str; } ).toString()
  searchContentProviders = db.page.find( { route: "/search/content-providers" }).map( function(page) { return page._id.str; } ).toString()
  searchJournalsTable = db.page.find( { route: "/search/journals-table" }).map( function(page) { return page._id.str; } ).toString()
  searchEntityRegistriesTable = db.page.find( { route: "/search/entity-registries-table" }).map( function(page) { return page._id.str; } ).toString()
  searchContentProvidersTable = db.page.find( { route: "/search/content-providers-table" }).map( function(page) { return page._id.str; } ).toString()

  // Deposit Pages
  shareInZenodo = db.page.find( { route: "/participate/deposit/zenodo" }).map( function(page) { return page._id.str; } ).toString();
  depositLearnHow = db.page.find( { route: "/participate/deposit/learn-how" }).map( function(page) { return page._id.str; } ).toString();
  depositSearch = db.page.find( { route: "/participate/deposit/search" }).map( function(page) { return page._id.str; } ).toString();

  // Linking Pages
  link = db.page.find( { route: "/participate/claim" }).map( function(page) { return page._id.str; } ).toString()
  directLink = db.page.find( { route: "/participate/direct-claim" }).map( function(page) { return page._id.str; } ).toString()
  myClaims = db.page.find( { route: "/myclaims" }).map( function(page) { return page._id.str; } ).toString()
  administratorClaims = db.page.find( { route: "/claims" }).map( function(page) { return page._id.str; } ).toString()
  claimsProjectManager = db.page.find( { route: "/claims-project-manager" }).map( function(page) { return page._id.str; } ).toString()

  //monitor = db.page.find( { route: "/monitor" }).map( function(page) { return page._id.str; } ).toString()

  var community_pages = {};
  community_pages[home] = true;

  community_pages[about] = true;
  community_pages[aboutLearnInDepth] = true;
  community_pages[contactUs] = true;
  community_pages[contentPolicy] = true;
  community_pages[myCommunities] = true;

  community_pages[curators] = false;
  community_pages[organizations] = false;
  community_pages[invite] = true;
  community_pages[helperTest] = true;
  community_pages[mailPreferences] = true;

  community_pages[publicationLanding] = true;
  community_pages[datasetLanding] = true;
  community_pages[softwareLanding] = true;
  community_pages[orpLanding] = true;
  community_pages[projectLanding] = true;
  community_pages[organizationLanding] = true;
  community_pages[dataproviderLanding] = true;
  community_pages[projectReport] = true;

  community_pages[searchFind] = true;
  community_pages[searchCommunities] = true;
  community_pages[searchResearchOutcomes] = true;
  community_pages[searchProject] = true;
  community_pages[searchOrganization] = true;
  community_pages[searchDataprovider] = true;

  community_pages[advancedSearchResearchOutcomes] = true;
  community_pages[advancedSearchProject] = true;
  community_pages[advancedSearchOrganization] = true;
  community_pages[advancedSearchDataprovider] = true;

  community_pages[searchJournals] = true;
  community_pages[searchEntityRegistries] = true;
  community_pages[searchContentProviders] = true;
  community_pages[searchJournalsTable] = true;
  community_pages[searchEntityRegistriesTable] = true;
  community_pages[searchContentProvidersTable] = true;

  community_pages[shareInZenodo] = true;
  community_pages[depositLearnHow] = true;
  community_pages[depositSearch] = true;

  community_pages[link] = true;
  community_pages[directLink] = true;
  community_pages[myClaims] = true;
  community_pages[administratorClaims] = true;
  community_pages[claimsProjectManager] = true;

  // community_pages[monitor] = true;

  publicationId = db.entity.find( { pid: "publication" }).map( function(entity) { return entity._id.str; } ).toString()
  datasetId = db.entity.find( { pid: "dataset" }).map( function(entity) { return entity._id.str; } ).toString()
  softwareId = db.entity.find( { pid: "software" }).map( function(entity) { return entity._id.str; } ).toString()
  orpId = db.entity.find( { pid: "orp" }).map( function(entity) { return entity._id.str; } ).toString();
  projectId = db.entity.find( { pid: "project" }).map( function(entity) { return entity._id.str; } ).toString()
  organizationId = db.entity.find( { pid: "organization" }).map( function(entity) { return entity._id.str; } ).toString()
  datasourceId = db.entity.find( { pid: "datasource" }).map( function(entity) { return entity._id.str; } ).toString();
  print("Pages:"+publicationId);
  var community_entities = {}
  community_entities[publicationId] = true;
  community_entities[datasetId] = true;
  community_entities[softwareId] = true;
  community_entities[orpId] = true;
  community_entities[projectId] = true;
  community_entities[organizationId] = true;
  community_entities[datasourceId] = true;
  db.community.save({	"name" : name,
    "pid" : communityPid, "pages" : community_pages, "entities" : community_entities})

  openaireCommunity = db.community.find( { pid: communityPid }).map( function(community) { return community._id.str; } ).toString()

  var numbers_map = {};
  numbers_map["total"] = { "showInMonitor" : true, "showInDashboard" : false };
  numbers_map["project"] = { "showInMonitor" : true, "showInDashboard" : false };
  numbers_map["open"] = { "showInMonitor" : true, "showInDashboard" : false };
  numbers_map["closed"] = { "showInMonitor" : true, "showInDashboard" : false };
  numbers_map["embargo"] = { "showInMonitor" : true, "showInDashboard" : false };
  numbers_map["restricted"] = { "showInMonitor" : true, "showInDashboard" : false };
  var numbers = {"map":numbers_map};
  var charts_map = {};
  charts_map["timeline"] = { "showInMonitor" : true, "showInDashboard" : false };
  charts_map["graph"] = { "showInMonitor" : true, "showInDashboard" : false };
  charts_map["projectTable"] = { "showInMonitor" : true, "showInDashboard" : false };
  charts_map["projectColumn"] = { "showInMonitor" : true, "showInDashboard" : false };
  charts_map["projectPie"] = { "showInMonitor" : true, "showInDashboard" : false };
  var charts = {"map":charts_map};

  var statistics_entities = {"charts":charts,"numbers":numbers}
  var entities = {};
  entities["publication"]=statistics_entities;
  entities["dataset"]=statistics_entities;
  entities["software"]=statistics_entities;
  entities["orp"]=statistics_entities;
  var statistics = {"pid" : communityPid, "entities" : entities};

  db.statistics.save(statistics);

  var subscribers = [];
  db.communitySubscribers.save({ "pid" : communityPid, "subscribers" : subscribers})

  depositLearnHow_community_content = "<div class=\"uk-width-3-5 uk-align-center\">\n" +
    "<div class=\"uk-text-bold\">How to comply with funder Open Access policies</div>\n" +
    "\n" +
    "<ul class=\"uk-list uk-list-bullet\">\n" +
    "\t<li>Read the <a class=\"custom-external\" href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-publications\" target=\"_blank\"> OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on publications </a></li>\n" +
    "\t<li>Read the <a class=\"custom-external\" href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-data\" target=\"_blank\"> OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on research data </a></li>\n" +
    "\t<li>If you want to know about National Open Access policies, please check them out <a class=\"custom-external\" href=\"https://www.openaire.eu/frontpage/country-pages\" target=\"_blank\">here</a></li>\n" +
    "\t<li>All OpenAIRE guides can be found <a class=\"custom-external\" href=\"https://www.openaire.eu/guides\" target=\"_blank\">here</a></li>\n" +
    "</ul>\n" +
    "</div>\n";

  depositLearnHow_explore_content = "<div class=\"uk-margin-top\" style=\"background: #EFEFEF 0% 0% no-repeat padding-box;\">\n" +
    "<div class=\"uk-width-3-5 uk-align-center uk-padding uk-padding-remove-horizontal uk-margin-remove-bottom\">\n" +
    "<div class=\"uk-text-bold\">How to comply with funder Open Access policies</div>\n" +
    "\n" +
    "<ul class=\"uk-list uk-list-bullet\">\n" +
    "\t<li>Read the <a class=\"custom-external\" href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-publications\" target=\"_blank\"> OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on publications </a></li>\n" +
    "\t<li>Read the <a class=\"custom-external\" href=\"https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-data\" target=\"_blank\"> OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on research data </a></li>\n" +
    "\t<li>If you want to know about National Open Access policies, please check them out <a class=\"custom-external\" href=\"https://www.openaire.eu/frontpage/country-pages\" target=\"_blank\">here</a></li>\n" +
    "\t<li>All OpenAIRE guides can be found <a class=\"custom-external\" href=\"https://www.openaire.eu/guides\" target=\"_blank\">here</a></li>\n" +
    "</ul>\n" +
    "</div>\n" +
    "</div>\n";

  organizations_content = '<div> <p>Here you can write more details about the organizations related to your community.</p> </div>';

  communityID = db.community.find({pid: communityPid}).map(function (community) { return community._id.str; }).toString();

  if (communityPid == "openaire") {
    db.pageHelpContent.save({
      "page" : depositLearnHow,
      "community" : communityID,
      "placement" : "bottom",
      "order" : 1,
      "content" : depositLearnHow_explore_content,
      "isActive" : true,
      "isPriorTo" : false
    });

    addHelpTextsInHomePage_explore();
  }
  print("openaire help texts added");

  if (communityPid == "connect") {
    addHelpTextsInHomePage_connect();
    addHelpTextsInAboutLearnHowPage();
    addHelpTextsInAboutLearnInDepthPage();
    addHelpTextsInContactUsPage();
    addHelpTextsInContentPolicyPage();
  }
  print("connect help texts added");

  if (communityPid != "connect" && communityPid != "openaire") {
    db.pageHelpContent.save({
      "page" : depositLearnHow,
      "community" : communityID,
      "placement" : "bottom",
      "order" : 1,
      "content" : depositLearnHow_community_content,
      "isActive" : true,
      "isPriorTo" : false
    });

    db.pageHelpContent.save({
      "page" : organizations,
      "community" : communityID,
      "placement" : "top",
      "order" : 1,
      "content" : organizations_content,
      "isActive" : false,
      "isPriorTo" : false
    });
  }
  print("community help texts added");
  print("\n\n Done \n\n");
}

function createAllCommunities(){
  createCommunity("OpenAIRE","openaire");
  createCommunity("Connect","connect");
  createCommunity("EGI Federation","egi");
  createCommunity("Environment and Economy ","ee");
  createCommunity("Fisheries and Aquaculture Management","fam");
  createCommunity("Instruct-ERIC","instruct");
  createCommunity("Elixir","elixir-gr");
  createCommunity("Marine Environmental Science","mes");
  createCommunity("EC post-grant Open Access Pilot","oa-pg");
  createCommunity("Neuroinformatics","ni");
  createCommunity("Research Data Alliance","rda");
  createCommunity("Digital Humanities and Cultural Heritage","dh-ch");
  createCommunity("AGINFRA+","aginfra");
  createCommunity("Clarin","clarin");
  createCommunity("DARIAH EU","dariah");
  createCommunity("Transport Research","beopen");
  createCommunity("European Plate Observing System","epos");
  createCommunity("RISIS","risis");
}

use openaire_admin;

dropDB();
createCollections();
initializeEntitiesPages();
createAllCommunities();
//addHelpTextsInHomePage();
//addHelpTextsInAboutLearnHowPage();
//addHelpTextsInAboutLearnInDepthPage();
//addHelpTextsInContactUsPage();
//addHelpTextsInContentPolicyPage();
