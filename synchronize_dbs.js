// Authentication???

// 1. copy database with name "test" in folder "/home/konstantina/mongo_backups"
// mongodump --db test --out /home/konstantina/mongo_backups

// 2. paste database with name "test", which is in folder "/home/konstantina/mongo_backups/test", into database with name "test2" | Drop collections if test2 already exists
// mongorestore --db test2 --drop /home/konstantina/mongo_backups/test

// Be careful with function parameters: names of dbs, name of portal
// 3. If needed run init script

// 4. Run synchronize_dbs.js

// 5. Run migrateCommunityIn_db.js


//version compatibility: 1.1.1-SNAPSHOT
print("synchronization of DBs script running...");

function keep_Beta_Production_DBs_synchronized(beta_db_name, production_db_name) {
	beta_db = db.getSiblingDB(beta_db_name);
  // db.auth("your_username", "your_password");
	prod_db = db.getSiblingDB(production_db_name);
  // db.auth("your_username", "your_password");

	print("keep_Beta_Production_DBs_synchronized - both dbs ("+beta_db_name+", "+production_db_name+") are here");

	// synchronize entities
	beta_db.entity.find().forEach(function(beta_entity){
		//var prod_entityId = prod_db.entity.find( { name: beta_entity.name }).map( function(prod_entity) { return prod_entity._id.str; } ).toString();
		var prod_entity = prod_db.entity.findOne( { pid: beta_entity.pid });
		if(!prod_entity) {
			prod_db.entity.save({"pid": beta_entity.pid, "name": beta_entity.name});

			print("Entity with pid: "+beta_entity.pid+" is created in "+production_db_name+" database");

			// synchronize this entity for all available communities
			//prod_entityId = prod_db.entity.find( { pid: beta_entity.pid }).map( function(prod_entity) { return prod_entity._id.str; } ).toString();
			var prod_entity_id = prod_db.entity.findOne( { pid: beta_entity.pid })._id.str;

			beta_db.portal.find().forEach(function(beta_portal){
				var prod_portal = prod_db.portal.findOne( { pid: beta_portal.pid } );
				if(prod_portal) {
					var prod_portal_entities = prod_portal.entities;
					prod_portal_entities[prod_entity_id] = beta_portal.entities[beta_entity._id.str];
					prod_db.portal.update({ pid: prod_portal.pid }, {$set: {entities: prod_portal_entities}})
				
					print("Entity was created for portal with pid: "+prod_portal.pid+" that exists on production db: "+production_db_name);
				} else {
					print("\nERROR: Portal with pid: "+ beta_portal.pid+" does not exist on production db: "+production_db_name+"\n");
				} 
			});
			print("");
		}
	});

	print("Entities are synchronized\n");

	// synchronize pages
	beta_db.page.find().forEach(function(beta_page){
		//var prod_pageId = prod_db.page.find( { route: beta_page.route }).map( function(prod_page) { return prod_page._id.str; } ).toString();
		var prod_page = prod_db.page.findOne( { route: beta_page.route, portalType: beta_page.portalType });//._id.str;
		if(!prod_page) {

			var prod_page_entities = [];
			beta_page.entities.forEach(function(beta_page_entity) {
				var beta_page_entity_ObjectId = ObjectId(beta_page_entity);
				//var beta_pageEntity_pid = beta_db.entity.find( { _id: beta_page_entity_ObjectId } ).map(function(entity) { return entity.pid; }).toString();
				var beta_pageEntity = beta_db.entity.findOne( { _id: beta_page_entity_ObjectId });
				if(beta_pageEntity) {
					//var prod_pageEntity_id = prod_db.entity.find( { pid: beta_pageEntity_pid } ).map(function(entity) { return entity._id.str; }).toString();
					var prod_pageEntity = prod_db.entity.findOne( { pid: beta_pageEntity.pid });//._id.str;
					if(prod_pageEntity) {
						prod_page_entities.push(prod_pageEntity._id.str);
					} else {
						print("\nERROR: Entity with pid: "+beta_pageEntity.pid+" does not exist on production db: "+production_db_name+"\n");
					}
				} else {
					print("\nERROR: Entity with id: "+beta_page_entity+" (of page: "+beta_page.name+") does not exist on beta db: "+beta_db_name+"\n");
				}
			})

			prod_db.page.save({
        "name": beta_page.name,
        "route": beta_page.route,
        "type": beta_page.type,
        "portalType": beta_page.portalType,
        "entities" : prod_page_entities,
        "top": beta_page.top, "bottom": beta_page.bottom, "left": beta_page.left, "right": beta_page.right
			});

			print("Page: "+beta_page.name+" is created in "+production_db_name+" database");

			// synchronize this page and its contents for all available communities
			//prod_pageId = prod_db.page.find( { route: beta_page.route }).map( function(prod_page) { return prod_page._id.str; } ).toString();
			var prod_pageId = prod_db.page.findOne( { route: beta_page.route, portalType: beta_page.portalType })._id.str;
			beta_db.portal.find({type: beta_page.portalType}).forEach(function(beta_portal){
				var prod_portal = prod_db.portal.findOne( { pid: beta_portal.pid } );
				if(prod_portal) {
					var prod_portal_pages = prod_portal.pages;
					prod_portal_pages[prod_pageId] = beta_portal.pages[beta_page._id.str];
					prod_db.portal.update({ pid: prod_portal.pid }, {$set: {pages: prod_portal_pages}})
					
					print("Page was created for portal: "+prod_portal.name+" that exists on production");

					beta_db.pageHelpContent.find({portal: beta_portal._id.str, page: beta_page._id.str}).forEach(function(beta_pageHelpContent) {
						prod_db.pageHelpContent.save({ "page" : prod_pageId, "portal" : prod_portal._id.str, "content" : beta_pageHelpContent.content, "placement": beta_pageHelpContent.placement, "order": beta_pageHelpContent.order, "isActive": beta_pageHelpContent.isActive, "isPriorTo": beta_pageHelpContent.isPriorTo });
					});
					print("PageHelpContents were created for portal: "+prod_portal.name+" that exists on production");
				}
				print(""); 
			});
			print("");
		}
	});

	print("Pages are synchronized\n");

	// synchronize divIds
	beta_db.divId.find().forEach(function(beta_divId){
		//var prod_divIdId = prod_db.divId.find( { name: beta_divId.name }).map( function(prod_divId) { return prod_divId._id.str; } ).toString();
		var prod_divId = prod_db.divId.findOne( { name: beta_divId.name, portalType: beta_divId.portalType });//._id.str;
		if(!prod_divId) {
			var prod_divId_pages = [];
			beta_divId.pages.forEach(function(beta_divId_page) {
				var beta_divId_page_ObjectId = ObjectId(beta_divId_page);
				//var beta_divIdPage_route = beta_db.page.find( { _id: beta_divId_page_ObjectId } ).map(function(page) { return page.route; }).toString();
				var beta_divIdPage = beta_db.page.findOne( { _id: beta_divId_page_ObjectId });//.route;
				if(beta_divIdPage) {
					//var prod_divIdPage_id = prod_db.page.find( { route: beta_divIdPage_route } ).map(function(page) { return page._id.str; }).toString();
					var prod_divIdPage = prod_db.page.findOne( { route: beta_divIdPage.route, portalType: beta_divIdPage.portalType });//._id.str;
					if(prod_divIdPage) {
						prod_divId_pages.push(prod_divIdPage._id.str);
					} else {
						print("\nERROR: Page with route: "+beta_divIdPage.route+" does not exist on production db: "+production_db_name+"\n");
					}
				} else {
					print("\nERROR: Page with id: "+beta_divId_page+" (of divId: "+beta_divId.name+") does not exist on beta db: "+beta_db_name+"\n");
				}
			})
			prod_db.divId.save({"name": beta_divId.name, "pages" : prod_divId_pages, "portalType": beta_divId.portalType});

			print("DivId: "+beta_divId.name+" is created in "+production_db_name+" database");
			
			// synchronize this divId's contents for all available communities
			//prod_divIdId = prod_db.divId.find( { name: beta_divId.name }).map( function(prod_divId) { return prod_divId._id.str; } ).toString();
			var prod_divIdId = prod_db.divId.findOne( { name: beta_divId.name, portalType: beta_divId.portalType })._id.str;
			beta_db.portal.find({type: beta_divId.portalType}).forEach(function(beta_portal){
				var prod_portal = prod_db.portal.findOne( { pid: beta_portal.pid } );
				if(prod_portal) {
					beta_db.divHelpContent.find({portal: beta_portal._id.str, divId: beta_divId._id.str}).forEach(function(beta_divHelpContent) {
						prod_db.divHelpContent.save({ "divId" : prod_divIdId, "portal" : prod_portal._id.str, "content" : beta_divHelpContent.content, "isActive" : beta_divHelpContent.isActive });
					});
					print("DivHelpContents were created for portal: "+prod_portal.name+" that exists on production");
				}
			});
			print("");
		}
	});

	print("DivIds are synchronized");
	
}

keep_Beta_Production_DBs_synchronized("openaireconnect_new", "openaireconnect");

