package eu.dnetlib.uoaadmintools;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.uoaadmintools.configuration.GlobalVars;
import eu.dnetlib.uoaadmintools.configuration.properties.APIProperties;
import eu.dnetlib.uoaadmintools.configuration.properties.BrowserCacheConfig;
import eu.dnetlib.uoaadmintools.configuration.properties.ManagersApiConfig;
import eu.dnetlib.uoaadmintools.configuration.properties.MongoConfig;
import eu.dnetlib.uoaadmintoolslibrary.UoaAdminToolsLibraryConfiguration;
import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.uoaadmintools"})
@PropertySources({
        @PropertySource("classpath:authorization.properties"),
        @PropertySource("classpath:admintoolslibrary.properties"),
        @PropertySource("classpath:admintools.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@EnableConfigurationProperties({MongoConfig.class, ManagersApiConfig.class, BrowserCacheConfig.class, GlobalVars.class, APIProperties.class})
@Import({AuthorizationConfiguration.class, UoaAdminToolsLibraryConfiguration.class})
public class UoaAdminToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UoaAdminToolsApplication.class, args);
    }

    @Bean
    RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new ObjectMapper());
        restTemplate.getMessageConverters().add(converter);
        return restTemplate;
    }
}
