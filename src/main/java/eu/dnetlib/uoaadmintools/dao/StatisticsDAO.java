package eu.dnetlib.uoaadmintools.dao;


import eu.dnetlib.uoaadmintools.entities.statistics.Statistics;

import java.util.List;

/**
 * Created by argirok on 5/3/2018.
 */
public interface StatisticsDAO {
    List<Statistics> findAll();

    Statistics findById(String Id);

    Statistics findByPid(String Pid);

    Statistics save(Statistics statistic);

    void deleteAll();

    void delete(String id);
}
