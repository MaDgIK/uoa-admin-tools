package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.subscriber.Subscriber;

import java.util.List;

public interface SubscriberDAO {

    List<Subscriber> findAll();

    Subscriber findById(String Id);

    Subscriber findByEmail(String email);

    Subscriber save(Subscriber subscriber);

    void deleteAll();

    void delete(String id);
}

