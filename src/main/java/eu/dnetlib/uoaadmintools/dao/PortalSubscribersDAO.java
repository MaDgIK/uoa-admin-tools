package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.subscriber.PortalSubscribers;


import java.util.List;

public interface PortalSubscribersDAO {

    List<PortalSubscribers> findAll();

    PortalSubscribers findById(String Id);

    PortalSubscribers findByPid(String Pid);

    PortalSubscribers save(PortalSubscribers portalSubscribers);

    void deleteAll();

    void delete(String id);
}

