package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.Notifications;

import java.util.List;

/**
 * Created by argirok on 6/7/2018.
 */
public interface NotificationsDAO {
    List<Notifications> findAll();

    Notifications findById(String Id);

    Notifications findByManagerEmailAndPortalPid(String managerEmail, String portalPid);
    List<Notifications> findByPortalPid(String portalPid);

    Notifications save(Notifications entity);

    void deleteAll();

    void delete(String id);
}
