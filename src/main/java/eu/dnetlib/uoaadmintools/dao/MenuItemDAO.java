package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.menu.MenuItem;

import java.util.List;

public interface MenuItemDAO {
    List<MenuItem> findAll();
    List<MenuItem> findByParentItemId(String parentId);

    MenuItem findById(String Id);

    List<MenuItem> findByPortalPid(String portalPid);
    List<MenuItem> findByParentItemIdAndPortalPid(String parentId, String portalPid);

    MenuItem save(MenuItem menuItem);

    void deleteAll();

    void delete(String id);
}
