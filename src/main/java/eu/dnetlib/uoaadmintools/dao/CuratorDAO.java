package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.curator.Curator;

import java.util.List;

public interface CuratorDAO {

    List<Curator> findAll();

    Curator findById(String Id);

    Curator findByEmail(String email);

    Curator save(Curator curator);

    void deleteAll();

    void delete(String id);
}
