package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.SubscriberDAO;
import eu.dnetlib.uoaadmintools.entities.subscriber.Subscriber;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBSubscriberDAO extends SubscriberDAO, MongoRepository<Subscriber, String> {

    List<Subscriber> findAll();

    Subscriber findById(String Id);

    Subscriber findByEmail(String email);

    Subscriber save(Subscriber subscriber);

    void deleteAll();

    void delete(String id);
}