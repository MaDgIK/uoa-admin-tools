package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.StatisticsDAO;
import eu.dnetlib.uoaadmintools.entities.statistics.Statistics;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBStatisticsDAO extends StatisticsDAO, MongoRepository<Statistics, String> {
    List<Statistics> findAll();

    Statistics findById(String Id);

    Statistics findByPid(String Pid);

    Statistics save(Statistics statistic);

    void deleteAll();

    void delete(String id);
}

