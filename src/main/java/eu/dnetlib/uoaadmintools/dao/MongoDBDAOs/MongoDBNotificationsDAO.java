package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.NotificationsDAO;
import eu.dnetlib.uoaadmintools.entities.Notifications;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by argirok on 6/7/2018.
 */
public interface MongoDBNotificationsDAO extends NotificationsDAO, MongoRepository<Notifications, String> {
    List<Notifications> findAll();

    Notifications findById(String Id);

    Notifications findByManagerEmailAndPortalPid(String managerEmail, String portalPid);
    List<Notifications> findByPortalPid(String portalPid);

    Notifications save(Notifications entity);

    void deleteAll();

    void delete(String id);
}


