package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.PortalSubscribersDAO;
import eu.dnetlib.uoaadmintools.entities.subscriber.PortalSubscribers;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBPortalSubscribersDAO extends PortalSubscribersDAO, MongoRepository<PortalSubscribers, String> {

    List<PortalSubscribers> findAll();

    PortalSubscribers findById(String Id);

    PortalSubscribers findByPid(String Pid);

    PortalSubscribers save(PortalSubscribers portalSubscribers);

    void deleteAll();

    void delete(String id);
}
