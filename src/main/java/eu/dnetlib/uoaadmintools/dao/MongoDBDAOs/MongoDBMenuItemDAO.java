package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.MenuItemDAO;
import eu.dnetlib.uoaadmintools.entities.menu.MenuItem;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBMenuItemDAO  extends MenuItemDAO, MongoRepository<MenuItem, String> {
    List<MenuItem> findAll();
    List<MenuItem> findByParentItemId(String parentId);

    MenuItem findById(String Id);

    List<MenuItem> findByPortalPid(String portalPid);
    List<MenuItem> findByParentItemIdAndPortalPid(String parentId, String portalPid);

    MenuItem save(MenuItem menuItem);

    void deleteAll();

    void delete(String id);
}
