package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.LayoutDAO;
import eu.dnetlib.uoaadmintools.entities.Layout;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBLayoutDAO extends LayoutDAO, MongoRepository<Layout, String> {

    List<Layout> findAll();

    Layout findById(String Id);

    List<Layout> findByPortalPid(String portalPid);

    Layout save(Layout layout);

    void deleteAll();

    void delete(String id);
}
