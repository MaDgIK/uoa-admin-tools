package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.MenuDAO;
import eu.dnetlib.uoaadmintools.entities.menu.Menu;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBMenuDAO  extends MenuDAO, MongoRepository<Menu, String> {
    List<Menu> findAll();
    Menu findByPortalPid(String portalPid);

    Menu save(Menu menu);

    void deleteAll();
    void deleteByPortalPid(String portalPid);
}
