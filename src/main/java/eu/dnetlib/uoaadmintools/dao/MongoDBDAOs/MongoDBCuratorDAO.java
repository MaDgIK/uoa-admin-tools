package eu.dnetlib.uoaadmintools.dao.MongoDBDAOs;

import eu.dnetlib.uoaadmintools.dao.CuratorDAO;
import eu.dnetlib.uoaadmintools.entities.curator.Curator;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBCuratorDAO extends CuratorDAO, MongoRepository<Curator, String> {

    List<Curator> findAll();

    Curator findById(String Id);

    Curator findByEmail(String email);

    Curator save(Curator curator);

    void deleteAll();

    void delete(String id);
}
