package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.Layout;

import java.util.List;

public interface LayoutDAO {
    List<Layout> findAll();

    Layout findById(String Id);

    List<Layout> findByPortalPid(String portalPid);

    Layout save(Layout layout);

    void deleteAll();

    void delete(String id);
}
