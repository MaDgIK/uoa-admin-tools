package eu.dnetlib.uoaadmintools.dao;

import eu.dnetlib.uoaadmintools.entities.menu.Menu;
import eu.dnetlib.uoaadmintools.entities.menu.MenuItem;

import java.util.List;

public interface MenuDAO {
    List<Menu> findAll();
    Menu findByPortalPid(String portalPid);

    Menu save(Menu menu);

    void deleteAll();
    void deleteByPortalPid(String portalPid);
}
