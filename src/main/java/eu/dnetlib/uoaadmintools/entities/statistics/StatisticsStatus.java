package eu.dnetlib.uoaadmintools.entities.statistics;

/**
 * Created by argirok on 5/3/2018.
 */
public class StatisticsStatus {
    Boolean showInMonitor = true;
    Boolean showInDashboard = false;

    public Boolean getShowInMonitor() {
        return showInMonitor;
    }

    public void setShowInMonitor(Boolean showInMonitor) {
        this.showInMonitor = showInMonitor;
    }

    public Boolean getShowInDashboard() {
        return showInDashboard;
    }

    public void setShowInDashboard(Boolean showInDashboard) {
        this.showInDashboard = showInDashboard;
    }

    @Override
    public String toString() {
        return "StatisticsStatus{" +
                "showInMonitor=" + showInMonitor +
                ", showInDashboard=" + showInDashboard +
                '}';
    }
}
