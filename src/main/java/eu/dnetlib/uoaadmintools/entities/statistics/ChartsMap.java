package eu.dnetlib.uoaadmintools.entities.statistics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by argirok on 15/3/2018.
 */
public class ChartsMap {
    Map<String,StatisticsStatus> map =  new HashMap<String, StatisticsStatus>();
    public ChartsMap(){
        map.put("timeline",new StatisticsStatus());
        map.put("graph",new StatisticsStatus());
        map.put("projectTable",new StatisticsStatus());
        map.put("projectColumn",new StatisticsStatus());
        map.put("projectPie",new StatisticsStatus());
     }

    public Map<String, StatisticsStatus> getMap() {
        return map;
    }

    public void setMap(Map<String, StatisticsStatus> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "ChartsMap{" +
                "entities=" + map +
                '}';
    }
}
