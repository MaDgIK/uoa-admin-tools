package eu.dnetlib.uoaadmintools.entities.statistics;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by argirok on 5/3/2018.
 */
public class Statistics {
    @Id
    @JsonProperty("_id")
    private String id;
    private String pid;
    private boolean isActive = true;
    Map<String,StatisticsEntity> entities =  new HashMap<String, StatisticsEntity>();
    public Statistics(String pid){
        this.pid = pid;
        this.isActive = true;
        entities.put("publication",new StatisticsEntity());
        entities.put("dataset",new StatisticsEntity());
        entities.put("software",new StatisticsEntity());
        entities.put("orp",new StatisticsEntity());
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Map<String, StatisticsEntity> getEntities() {
        return entities;
    }

    public void setEntities(Map<String, StatisticsEntity> entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "id='" + id + '\'' +
                ", pid='" + pid + '\'' +
                ", isActive= "+isActive +
                ", entities=" + entities +
                '}';
    }

    //    private Map<String, StatisticsStatus> statistics;
//
//    public Statistics(String pid){
//        this.setPid(pid);
//        this.statistics =  new HashMap<String, StatisticsStatus>();
//
//        this.statistics.put("publicationsNumber", new StatisticsStatus());
//        this.statistics.put("publicationsProjectNumber", new StatisticsStatus());
//        this.statistics.put("publicationsOpenNumber", new StatisticsStatus());
//        this.statistics.put("publicationsClosedNumber", new StatisticsStatus());
//        this.statistics.put("publicationsEmbargoNumber", new StatisticsStatus());
//
//        this.statistics.put("datasetsNumber", new StatisticsStatus());
//        this.statistics.put("datasetsProjectNumber", new StatisticsStatus());
//        this.statistics.put("datasetsOpenNumber", new StatisticsStatus());
//        this.statistics.put("datasetsClosedNumber", new StatisticsStatus());
//        this.statistics.put("datasetsEmbargoNumber", new StatisticsStatus());
//
//        this.statistics.put("softwareNumber", new StatisticsStatus());
//        this.statistics.put("softwareProjectNumber", new StatisticsStatus());
//        this.statistics.put("softwareOpenNumber", new StatisticsStatus());
//        this.statistics.put("softwareClosedNumber", new StatisticsStatus());
//        this.statistics.put("softwareEmbargoNumber", new StatisticsStatus());
//
//        this.statistics.put("publicationsTimeline", new StatisticsStatus());
//        this.statistics.put("datasetsTimeline", new StatisticsStatus());
//        this.statistics.put("softwareTimeline", new StatisticsStatus());
//
//
//        this.statistics.put("publicationsProjectColumn", new StatisticsStatus());
//        this.statistics.put("publicationsProjectPie", new StatisticsStatus());
//        this.statistics.put("publicationsProjectTable", new StatisticsStatus());
//
//        this.statistics.put("datasetsProjectColumn", new StatisticsStatus());
//        this.statistics.put("datasetsProjectPie", new StatisticsStatus());
//        this.statistics.put("datasetsProjectTable", new StatisticsStatus());
//
//        this.statistics.put("softwareProjectColumn", new StatisticsStatus());
//        this.statistics.put("softwareProjectPie", new StatisticsStatus());
//        this.statistics.put("softwareProjectTable", new StatisticsStatus());
//
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getPid() {
//        return pid;
//    }
//
//    public void setPid(String pid) {
//        this.pid = pid;
//    }
//
//    public Map<String, StatisticsStatus> getStatistics() {
//        return statistics;
//    }
//
//    public void setStatistics(Map<String, StatisticsStatus> statistics) {
//        this.statistics = statistics;
//    }
//
//    @Override
//    public String toString() {
//        return "Statistics{" +
//                "id='" + id + '\'' +
//                ", pid='" + pid + '\'' +
//                ", statistics=" + statistics +
//                '}';
//    }
}
