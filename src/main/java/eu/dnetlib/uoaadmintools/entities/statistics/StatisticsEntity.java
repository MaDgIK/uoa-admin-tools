package eu.dnetlib.uoaadmintools.entities.statistics;

/**
 * Created by argirok on 15/3/2018.
 */
public class StatisticsEntity {
    private ChartsMap charts = new ChartsMap();
    private NumbersMap numbers = new NumbersMap();

    public ChartsMap getCharts() {
        return charts;
    }

    public void setCharts(ChartsMap charts) {
        this.charts = charts;
    }

    public NumbersMap getNumbers() {
        return numbers;
    }

    public void setNumbers(NumbersMap numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "StatisticsEntity{" +
                "charts=" + charts +
                ", numbers=" + numbers +
                '}';
    }
}
