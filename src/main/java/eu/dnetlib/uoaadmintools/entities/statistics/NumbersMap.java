package eu.dnetlib.uoaadmintools.entities.statistics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by argirok on 15/3/2018.
 */
public class NumbersMap {
    Map<String,StatisticsStatus> map =  new HashMap<String, StatisticsStatus>();
    public NumbersMap(){
        map.put("total",new StatisticsStatus());
        map.put("project",new StatisticsStatus());
        map.put("open",new StatisticsStatus());
        map.put("closed",new StatisticsStatus());
        map.put("embargo",new StatisticsStatus());
        map.put("restricted",new StatisticsStatus());
    }

    public Map<String, StatisticsStatus> getMap() {
        return map;
    }

    public void setMap(Map<String, StatisticsStatus> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "ChartsMap{" +
                "entities=" + map +
                '}';
    }
}
