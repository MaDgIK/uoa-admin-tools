package eu.dnetlib.uoaadmintools.entities.menu;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    @Id
    @JsonProperty("portalPid")
    String portalPid;
    public boolean isFeaturedMenuEnabled;
    public boolean isMenuEnabled;
    public List<String> featuredMenuItems;
    public List<String> menuItems;
    public MenuAlignment featuredAlignment = MenuAlignment.CENTER;

    public Menu(String portalPid) {
        this.setPortalPid(portalPid);
        this.setMenuEnabled(true);
        this.setFeaturedMenuEnabled(true);
        this.setMenuItems(new ArrayList<>());
        this.setFeaturedMenuItems(new ArrayList<>());
        this.setFeaturedAlignment(MenuAlignment.CENTER.name());
    }

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public boolean getIsFeaturedMenuEnabled() {
        return isFeaturedMenuEnabled;
    }

    public void setFeaturedMenuEnabled(boolean featuredMenuEnabled) {
        isFeaturedMenuEnabled = featuredMenuEnabled;
    }

    public boolean getIsMenuEnabled() {
        return isMenuEnabled;
    }

    public void setMenuEnabled(boolean menuEnabled) {
        isMenuEnabled = menuEnabled;
    }

    public List<String> getFeaturedMenuItems() {
        return featuredMenuItems;
    }

    public void setFeaturedMenuItems(List<String> featuredMenuItems) {
        this.featuredMenuItems = featuredMenuItems;
    }

    public List<String> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<String> menuItems) {
        this.menuItems = menuItems;
    }

    public String getFeaturedAlignment() {
        if(featuredAlignment == null) {
            return null;
        }
        return featuredAlignment.name();
    }

    public void setFeaturedAlignment(String featuredAlignment) {
        if(featuredAlignment == null) {
            this.featuredAlignment = null;
        } else {
            MenuAlignment alignment = MenuAlignment.valueOf(featuredAlignment);
            this.featuredAlignment = alignment;
        }
    }
}
