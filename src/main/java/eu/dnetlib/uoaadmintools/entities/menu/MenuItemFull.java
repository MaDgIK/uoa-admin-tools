package eu.dnetlib.uoaadmintools.entities.menu;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.List;

public class MenuItemFull {
    @Id
    @JsonProperty("_id")
    private String id;  // for root menu in order to close the dropdown when clicked

    String title;
    String url;  // external url
    String route;   // internal  url - using angular routing and components
    String type; // internal or external or noAction
    String target; // _self or _blank
    List<MenuItemFull> items;
    String parentItemId;
    String portalPid;
    Boolean isFeatured = false;

    public MenuItemFull(){}
    public MenuItemFull(MenuItem menuItem){
        setId(menuItem.getId());
        setTitle(menuItem.getTitle());
        setUrl(menuItem.getUrl());
        setType(menuItem.getType());
        setTarget(menuItem.getTarget());
        setRoute(menuItem.getRoute());
        setParentItemId(menuItem.getParentItemId());
        setPortalPid(menuItem.getPortalPid());
        setIsFeatured(menuItem.getIsFeatured());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<MenuItemFull> getItems() {
        return items;
    }

    public void setItems(List<MenuItemFull> items) {
        this.items = items;
    }

    public String getParentItemId() {
        return parentItemId;
    }

    public void setParentItemId(String parentItemId) {
        this.parentItemId = parentItemId;
    }

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public Boolean getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    @Override
    public String toString() {
        return "MenuItemFull{" +
                "id='" + id + '\'' +
//                ", notifyForNewManagers=" + notifyForNewManagers +
                '}';
    }
}
