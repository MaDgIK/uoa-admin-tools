package eu.dnetlib.uoaadmintools.entities.menu;

public enum MenuAlignment {
    LEFT, CENTER, RIGHT
}
