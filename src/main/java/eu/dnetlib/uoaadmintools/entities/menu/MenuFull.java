package eu.dnetlib.uoaadmintools.entities.menu;

import java.util.List;

public class MenuFull {
    String portalPid;
    public boolean isFeaturedMenuEnabled;
    public boolean isMenuEnabled;
    public List<MenuItemFull> featuredMenuItems;
    public List<MenuItemFull> menuItems;
    public MenuAlignment featuredAlignment = MenuAlignment.CENTER;

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public boolean getIsFeaturedMenuEnabled() {
        return isFeaturedMenuEnabled;
    }

    public void setFeaturedMenuEnabled(boolean featuredMenuEnabled) {
        isFeaturedMenuEnabled = featuredMenuEnabled;
    }

    public boolean getIsMenuEnabled() {
        return isMenuEnabled;
    }

    public void setMenuEnabled(boolean menuEnabled) {
        isMenuEnabled = menuEnabled;
    }

    public List<MenuItemFull> getFeaturedMenuItems() {
        return featuredMenuItems;
    }

    public void setFeaturedMenuItems(List<MenuItemFull> featuredMenuItems) {
        this.featuredMenuItems = featuredMenuItems;
    }

    public List<MenuItemFull> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemFull> menuItems) {
        this.menuItems = menuItems;
    }

    public String getFeaturedAlignment() {
        if(featuredAlignment == null) {
            return null;
        }
        return featuredAlignment.name();
    }

    public void setFeaturedAlignment(String featuredAlignment) {
        if(featuredAlignment == null) {
            this.featuredAlignment = null;
        } else {
            MenuAlignment alignment = MenuAlignment.valueOf(featuredAlignment);
            this.featuredAlignment = alignment;
        }
    }
}
