package eu.dnetlib.uoaadmintools.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by argirok on 2/3/2018.
 */
public class Subscriber {
    @Id
    @JsonProperty("_id")
    private String id;

    private String email;

    public Subscriber() {}
    public Subscriber(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
