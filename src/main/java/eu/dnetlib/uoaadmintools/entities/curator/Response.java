package eu.dnetlib.uoaadmintools.entities.curator;

import eu.dnetlib.uoaadmintools.entities.Manager;

public class Response {
    private Manager[] response;

    public Response() {
    }

    public Manager[] getResponse() {
        return response;
    }

    public void setResponse(Manager[] response) {
        this.response = response;
    }
}
