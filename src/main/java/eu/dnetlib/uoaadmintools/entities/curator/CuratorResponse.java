package eu.dnetlib.uoaadmintools.entities.curator;

import eu.dnetlib.uoaadmintools.entities.Affiliation;

import java.util.List;

public class CuratorResponse {

    private String name;
    private List<Affiliation> affiliations;
    private String photo;
    private String bio;
    private boolean visible = true;

    public CuratorResponse() {
    }

    public CuratorResponse(Curator curator) {
        this.name = curator.getName();
        this.affiliations = curator.getAffiliations();
        this.photo = curator.getPhoto();
        this.bio = curator.getBio();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Affiliation> getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(List<Affiliation> affiliations) {
        this.affiliations = affiliations;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
