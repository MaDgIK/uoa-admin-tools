package eu.dnetlib.uoaadmintools.entities.curator;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintools.entities.Affiliation;
import org.springframework.data.annotation.Id;

import java.util.List;

public class Curator {

    @Id
    @JsonProperty("_id")
    private String id;  // THIS IS aaiId
    private String email;
    private String name;
    private List<Affiliation> affiliations;
    private String photo;
    private String bio;
    private boolean visible = true;

    public Curator() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Affiliation> getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(List<Affiliation> affiliations) {
        this.affiliations = affiliations;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
