package eu.dnetlib.uoaadmintools.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

/**
 * Created by argirok on 6/7/2018.
 */
public class Notifications {
    @Id
    @JsonProperty("_id")
    private String id;
    Boolean notifyForNewManagers = true;
    Boolean notifyForNewSubscribers = true;
    String managerEmail;
    String portalPid;
    String aaiId;

    public Notifications(){

    }
    public Notifications(String managerEmail, String portalPid){
        this();
        this.portalPid = portalPid;
        this.managerEmail = managerEmail;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getNotifyForNewManagers() {
        return notifyForNewManagers;
    }

    public void setNotifyForNewManagers(Boolean notifyForNewManagers) {
        this.notifyForNewManagers = notifyForNewManagers;
    }

    public Boolean getNotifyForNewSubscribers() {
        return notifyForNewSubscribers;
    }

    public void setNotifyForNewSubscribers(Boolean notifyForNewSubscribers) {
        this.notifyForNewSubscribers = notifyForNewSubscribers;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public String getAaiId() {
        return aaiId;
    }

    public void setAaiId(String aaiId) {
        this.aaiId = aaiId;
    }

    @Override
    public String toString() {
        return "Notifications{" +
                "id='" + id + '\'' +
                ", notifyForNewManagers=" + notifyForNewManagers +
                ", notifyForNewSubscribers=" + notifyForNewSubscribers +
                ", managerEmail='" + managerEmail + '\'' +
                ", portalPid='" + portalPid + '\'' +
                '}';
    }
}
