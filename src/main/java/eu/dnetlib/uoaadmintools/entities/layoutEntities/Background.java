package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Background {

    private String color;
    private String borderStyle;
    private String borderColor;
    private Integer borderWidth;

    public Background() {
    }

    public Background(String color, String borderStyle, String borderColor, Integer borderWidth) {
        this.color = color;
        this.borderStyle = borderStyle;
        this.borderColor = borderColor;
        this.borderWidth = borderWidth;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(String borderStyle) {
        this.borderStyle = borderStyle;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }
}
