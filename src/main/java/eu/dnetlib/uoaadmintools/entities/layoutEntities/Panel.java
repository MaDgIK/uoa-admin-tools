package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Panel {

    private Boolean onDarkBackground;
    private Background background;
    private Fonts fonts;
    private Fonts title;
    private PanelElements panelElements;

    public Panel() {
    }

    public Panel(Boolean onDarkBackground, Background background, Fonts fonts, Fonts title, PanelElements panelElements) {
        this.onDarkBackground = onDarkBackground;
        this.background = background;
        this.fonts = fonts;
        this.title = title;
        this.panelElements = panelElements;
    }

    public Boolean getOnDarkBackground() {
        return onDarkBackground;
    }

    public void setOnDarkBackground(Boolean onDarkBackground) {
        this.onDarkBackground = onDarkBackground;
    }

    public Background getBackground() {
        return background;
    }

    public void setBackground(Background background) {
        this.background = background;
    }

    public Fonts getFonts() {
        return fonts;
    }

    public void setFonts(Fonts fonts) {
        this.fonts = fonts;
    }

    public Fonts getTitle() {
        return title;
    }

    public void setTitle(Fonts title) {
        this.title = title;
    }

    public PanelElements getPanelElements() {
        return panelElements;
    }

    public void setPanelElements(PanelElements panelElements) {
        this.panelElements = panelElements;
    }
}
