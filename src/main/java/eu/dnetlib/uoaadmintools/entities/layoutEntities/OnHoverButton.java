package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class OnHoverButton {

    private String backgroundColor;
    private String color;
    private String borderColor;

    public OnHoverButton() {
    }

    public OnHoverButton(String backgroundColor, String color, String borderColor) {
        this.backgroundColor = backgroundColor;
        this.color = color;
        this.borderColor = borderColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }
}
