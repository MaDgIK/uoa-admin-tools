package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Buttons {

    private Button darkBackground;
    private Button lightBackground;

    public Buttons() {
    }

    public Buttons(Button darkBackground, Button lightBackground) {
        this.darkBackground = darkBackground;
        this.lightBackground = lightBackground;
    }

    public Button getDarkBackground() {
        return darkBackground;
    }

    public void setDarkBackground(Button darkBackground) {
        this.darkBackground = darkBackground;
    }

    public Button getLightBackground() {
        return lightBackground;
    }

    public void setLightBackground(Button lightBackground) {
        this.lightBackground = lightBackground;
    }
}
