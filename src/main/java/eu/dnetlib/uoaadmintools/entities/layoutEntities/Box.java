package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Box {

    private String borderColor;
    private String borderStyle;
    private String borderWidth;
    private Integer borderRadius;

    public Box() {
    }

    public Box(String borderColor, String borderStyle, String borderWidth, Integer borderRadius) {
        this.borderColor = borderColor;
        this.borderStyle = borderStyle;
        this.borderWidth = borderWidth;
        this.borderRadius = borderRadius;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(String borderStyle) {
        this.borderStyle = borderStyle;
    }

    public String getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(String borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Integer getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Integer borderRadius) {
        this.borderRadius = borderRadius;
    }
}
