package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Link {

    private String family;
    private Integer size;
    private Integer weight;
    private String color;
    private OnHoverLink onHover;

    public Link() {
    }

    public Link(String family, Integer size, Integer weight, String color, OnHoverLink onHover) {
        this.family = family;
        this.size = size;
        this.weight = weight;
        this.color = color;
        this.onHover = onHover;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public OnHoverLink getOnHover() {
        return onHover;
    }

    public void setOnHover(OnHoverLink onHover) {
        this.onHover = onHover;
    }
}
