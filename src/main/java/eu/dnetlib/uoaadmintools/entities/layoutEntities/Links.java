package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Links {

    private Link darkBackground;
    private Link lightBackground;

    public Links() {
    }

    public Links(Link darkBackground, Link lightBackground) {
        this.darkBackground = darkBackground;
        this.lightBackground = lightBackground;
    }

    public Link getDarkBackground() {
        return darkBackground;
    }

    public void setDarkBackground(Link darkBackground) {
        this.darkBackground = darkBackground;
    }

    public Link getLightBackground() {
        return lightBackground;
    }

    public void setLightBackground(Link lightBackground) {
        this.lightBackground = lightBackground;
    }
}
