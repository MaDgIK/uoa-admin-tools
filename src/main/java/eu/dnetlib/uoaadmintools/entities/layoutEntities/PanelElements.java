package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class PanelElements {

    private String backgroundColor;
    private String borderColor;
    private String color;

    public PanelElements() {
    }

    public PanelElements(String backgroundColor, String borderColor, String color) {
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;
        this.color = color;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
