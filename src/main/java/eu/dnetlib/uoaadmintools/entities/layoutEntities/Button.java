package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Button {

    private String backgroundColor;
    private String color;
    private String borderStyle;
    private String borderColor;
    private Integer borderWidth;
    private Integer borderRadius;
    private OnHoverButton onHover;

    public Button() {
    }

    public Button(String backgroundColor, String color, String borderStyle, String borderColor, Integer borderWidth, Integer borderRadius, OnHoverButton onHover) {
        this.backgroundColor = backgroundColor;
        this.color = color;
        this.borderStyle = borderStyle;
        this.borderColor = borderColor;
        this.borderWidth = borderWidth;
        this.borderRadius = borderRadius;
        this.onHover = onHover;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(String borderStyle) {
        this.borderStyle = borderStyle;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Integer getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Integer borderRadius) {
        this.borderRadius = borderRadius;
    }

    public OnHoverButton getOnHover() {
        return onHover;
    }

    public void setOnHover(OnHoverButton onHover) {
        this.onHover = onHover;
    }
}
