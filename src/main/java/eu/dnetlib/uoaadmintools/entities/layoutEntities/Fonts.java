package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class Fonts {

    private String color;
    private String family;
    private Integer size;
    private Integer weight;

    public Fonts() {
    }

    public Fonts(String color, String family, Integer size, Integer weight) {
        this.color = color;
        this.family = family;
        this.size = size;
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
