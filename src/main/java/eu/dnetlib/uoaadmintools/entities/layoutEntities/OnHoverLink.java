package eu.dnetlib.uoaadmintools.entities.layoutEntities;

public class OnHoverLink {

    private String color;

    public OnHoverLink() {
    }

    public OnHoverLink(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
