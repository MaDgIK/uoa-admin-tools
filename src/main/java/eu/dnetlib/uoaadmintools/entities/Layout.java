package eu.dnetlib.uoaadmintools.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoaadmintools.entities.layoutEntities.Box;
import eu.dnetlib.uoaadmintools.entities.layoutEntities.Buttons;
import eu.dnetlib.uoaadmintools.entities.layoutEntities.Links;
import eu.dnetlib.uoaadmintools.entities.layoutEntities.Panel;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Layout {

    @Id
    @JsonProperty("_id")
    private String id;
    private String portalPid;
    private Object layoutOptions;
    private Date date ;

    public Layout() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPortalPid() {
        return portalPid;
    }

    public void setPortalPid(String portalPid) {
        this.portalPid = portalPid;
    }

    public Object getLayoutOptions() {
        return layoutOptions;
    }

    public void setLayoutOptions(Object layoutOptions) {
        this.layoutOptions = layoutOptions;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
