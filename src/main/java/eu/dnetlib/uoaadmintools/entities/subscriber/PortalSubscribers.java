package eu.dnetlib.uoaadmintools.entities.subscriber;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by argirok on 2/3/2018.
 */
public class PortalSubscribers {
    @Id
    @JsonProperty("_id")
    private String id;
    private String pid;
    private List<Subscriber> subscribers = new ArrayList<Subscriber>();
    private List<Subscriber> pendingSubscribers = new ArrayList<Subscriber>();
    public PortalSubscribers(String pid){
        this.pid=pid;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<Subscriber> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Subscriber> subscribers) {
        this.subscribers = subscribers;
    }

    @Override
    public String toString() {
        return "PortalSubscribers{" +
                "id='" + id + '\'' +
                ", pid='" + pid + '\'' +
                ", subscribers=" + subscribers +
                '}';
    }
}
