package eu.dnetlib.uoaadmintools.configuration.properties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("admintool.cache")
public class BrowserCacheConfig {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
