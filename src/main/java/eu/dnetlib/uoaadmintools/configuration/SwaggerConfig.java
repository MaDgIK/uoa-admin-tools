package eu.dnetlib.uoaadmintools.configuration;

import eu.dnetlib.uoaadmintools.configuration.properties.APIProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

/**
 * Swagger configuration class
 */
@Configuration
@Profile({"swagger"})
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurerAdapter {

    private final APIProperties apiProperties;

    @Autowired
    public SwaggerConfig(APIProperties apiProperties) {
        this.apiProperties = apiProperties;
    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
//                .globalOperationParameters(globalParameterList())
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("eu.dnetlib.uoaadmintools.controllers"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createRestApiLibrary() {
        return new Docket(DocumentationType.SWAGGER_2)
//                .globalOperationParameters(globalParameterList())
                .apiInfo(apiInfo())
                .groupName("Library")
                .select()
                .apis(RequestHandlerSelectors.basePackage("eu.dnetlib.uoaadmintoolslibrary.controllers"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createRestApiAuthorizationLibrary() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("Authorization Library")
                .select()
                .apis(RequestHandlerSelectors.basePackage("eu.dnetlib.uoaauthorizationlibrary.controllers"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(this.apiProperties.getTitle())
                .description(this.apiProperties.getDescription())
                .version(this.apiProperties.getVersion())
                .build();
    }

    private List<Parameter> globalParameterList() {
        Parameter authTokenHeader = new ParameterBuilder()
                        .name("Session") // name of the header
                        .modelRef(new ModelRef("string")) // data-type of the header
                        .required(false)
                        .parameterType("header")
                        .description("Session ID")
                        .build();
        return Collections.singletonList(authTokenHeader);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/v2/api-docs", "/v2/api-docs");
        registry.addRedirectViewController("/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
        registry.addRedirectViewController("/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
        registry.addRedirectViewController("/swagger-resources", "/swagger-resources");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}

