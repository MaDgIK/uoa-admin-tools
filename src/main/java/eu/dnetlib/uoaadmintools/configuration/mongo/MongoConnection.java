package eu.dnetlib.uoaadmintools.configuration.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import eu.dnetlib.uoaadmintools.configuration.properties.MongoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collections;

@Configuration
@EnableMongoRepositories(basePackages = {"eu.dnetlib.uoaadmintools.dao", "eu.dnetlib.uoaadmintoolslibrary.dao"})
public class MongoConnection {

    @Autowired
    private MongoConfig mongoConfig;

    @Bean
    @Primary
    public MongoDbFactory mongoDbFactory() {
        return new SimpleMongoDbFactory(getMongoClient(), mongoConfig.getDatabase());
    }

    @Bean(name = "mongoTemplate")
    @Primary
    public MongoTemplate getMongoTemplate() {
        return new MongoTemplate(mongoDbFactory());
    }

    private MongoClient getMongoClient() {
        if(mongoConfig.getUsername() != null && mongoConfig.getPassword() != null){
            return new MongoClient(Collections.singletonList(
                    new ServerAddress(mongoConfig.getHost(), mongoConfig.getPort())),
                    Collections.singletonList(MongoCredential.createCredential(mongoConfig.getUsername(), mongoConfig.getDatabase(), mongoConfig.getPassword().toCharArray())));
        } else {
            return new MongoClient(Collections.singletonList(new ServerAddress(mongoConfig.getHost(), mongoConfig.getPort())));
        }
    }
}
