package eu.dnetlib.uoaadmintools.controllers;

import eu.dnetlib.uoaadmintools.dao.SubscriberDAO;
import eu.dnetlib.uoaadmintools.entities.subscriber.Subscriber;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by argirok on 2/3/2018.
 */
@RestController
@CrossOrigin(origins = "*")
public class SubscriberController {
    private final Logger log = LogManager.getLogger(this.getClass());
    @Autowired
    private SubscriberDAO subscriberDAO;
    @RequestMapping(value = "/subscriber", method = RequestMethod.GET)
    public List<Subscriber> getSubscriber() throws ContentNotFoundException {
        List<Subscriber> list = subscriberDAO.findAll();
        if(list == null){
            throw new ContentNotFoundException("Subscribers not found");
        }
        return subscriberDAO.findAll();
    }
    @RequestMapping(value = "/subscriber/{email}", method = RequestMethod.GET)
    public Subscriber getSubscriber(@PathVariable(value="email", required = true) String email) throws ContentNotFoundException {
        Subscriber subscriber = subscriberDAO.findByEmail(email);
        if(subscriber == null){
            throw new ContentNotFoundException("Subscribers not found");
        }
        return subscriber;
    }
//    @RequestMapping(value = "/subscriber", method = RequestMethod.POST)
//    public Subscriber saveSubscriber(@RequestBody Subscriber subscriber) {
//        return subscriberDAO.save(subscriber);
//    }
//    @RequestMapping(value = "/subscriber/{email}", method = RequestMethod.DELETE)
//    public void deleteSubscriber(@PathVariable(value="email", required = true) String email) throws ContentNotFoundException {
//        Subscriber subscriber = subscriberDAO.findByEmail(email);
//        if(subscriber == null){
//            throw new ContentNotFoundException("Subscribers not found");
//        }
//        subscriberDAO.delete(subscriber.getId());
//
//    }

}
