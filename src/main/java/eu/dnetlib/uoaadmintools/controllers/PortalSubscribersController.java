package eu.dnetlib.uoaadmintools.controllers;
import eu.dnetlib.uoaadmintools.dao.PortalSubscribersDAO;
import eu.dnetlib.uoaadmintools.dao.SubscriberDAO;
import eu.dnetlib.uoaadmintools.entities.subscriber.PortalSubscribers;
import eu.dnetlib.uoaadmintoolslibrary.dao.PortalDAO;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.responses.SingleValueWrapperResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by argirok on 2/3/2018.
 */
@RestController
@CrossOrigin(origins = "*")
public class PortalSubscribersController {
    @Autowired
    PortalSubscribersDAO portalSubscribersDAO;
    @Autowired
    SubscriberDAO subscriberDAO;
    @Autowired
    PortalDAO portalDAO;

//    @Autowired
//    private SecurityConfig securityConfig;

    private final Logger log = LogManager.getLogger(this.getClass());

    @RequestMapping(value = "/community/{pid}/subscribers/count", method = RequestMethod.GET)
    public SingleValueWrapperResponse<Integer> getNumberOfSubscribersPerPortal(@PathVariable(value="pid", required = true) String pid) throws ContentNotFoundException {
        SingleValueWrapperResponse<Integer> singleValueWrapperResponse = new SingleValueWrapperResponse(0);

        PortalSubscribers portalSubscribers = portalSubscribersDAO.findByPid(pid);
        if(portalSubscribers != null){
            if(portalSubscribers.getSubscribers() != null) {
                singleValueWrapperResponse.setValue(portalSubscribers.getSubscribers().size());
            }
        }else{
            throw new ContentNotFoundException("Portal Subscribers not found");

        }
        return singleValueWrapperResponse;
    }

    @RequestMapping(value = "/community/subscribers", method = RequestMethod.GET)
    public List<PortalSubscribers> getAllPortalSubscribers(){
        return portalSubscribersDAO.findAll();
    }

    @RequestMapping(value = "/community/{pid}/subscribers", method = RequestMethod.GET)
    public PortalSubscribers getSubscribersPerPortal(@PathVariable(value="pid", required = true) String pid) throws ContentNotFoundException {
        PortalSubscribers portalSubscribers = portalSubscribersDAO.findByPid(pid);
        if(portalSubscribers != null){
            return portalSubscribers;
        }else{
            throw new ContentNotFoundException("Portal Subscribers not found");

        }

    }
//    @RequestMapping(value = "/community/{pid}/subscribers", method = RequestMethod.POST)
//    public PortalSubscribers addSubscriberInPortal(@PathVariable(value="pid", required = true) String pid, @RequestBody Subscriber subscriber) throws ContentNotFoundException {
//        PortalSubscribers portalSubscribers = portalSubscribersDAO.findByPid(pid);
//        if(portalSubscribers == null){
//            throw new ContentNotFoundException("Portal Subscribers not found");
//        }
//
//        Subscriber savedSubscriber = subscriberDAO.findByEmail(subscriber.getEmail());
//        if(savedSubscriber==null){
//            savedSubscriber = subscriberDAO.save(subscriber);
//        }
//        for(Subscriber sub: portalSubscribers.getSubscribers()){
//            if(sub.getEmail().equals(subscriber.getEmail())){
//                //already subscribed
//                return portalSubscribers;
//            }
//        }
//        //not subscribed yet
//        portalSubscribers.getSubscribers().add(savedSubscriber);
//        return portalSubscribersDAO.save(portalSubscribers);
//
//    }

//    @RequestMapping(value = "/community/{pid}/is-subscriber", method = RequestMethod.GET)
//    public Boolean getIsSubscribedToPortal(@PathVariable(value="pid", required = true) String pid,
//                                              //@RequestBody String email,
//                                              @RequestHeader("X-XSRF-TOKEN") String token) throws ContentNotFoundException {
//        AuthorizationUtils helper = new AuthorizationUtils();
//        helper.setUserInfoUrl(securityConfig.getUserInfoUrl());
//        UserInfo userInfo = helper.getUserInfo(token);
//
//        if(userInfo != null) {
//            String email = userInfo.getEmail();
//            PortalSubscribers communitySubscribers = portalSubscribersDAO.findByPid(pid);
//            if (communitySubscribers != null) {
//                if (communitySubscribers.getSubscribers() != null) {
//                    for (Subscriber subscriber : communitySubscribers.getSubscribers()) {
//                        if (subscriber.getEmail().equals(email)) {
//                            return true;
//                        }
//                    }
//                }
//            } else {
//                throw new ContentNotFoundException("Portal Subscribers not found");
//
//            }
//        }
//        return false;
//    }
//
//    @RequestMapping(value = "/community/{pid}/subscriber", method = RequestMethod.POST)
//    public Boolean addSubscriberInPortal(@PathVariable(value="pid", required = true) String pid,
//                                         @RequestHeader("X-XSRF-TOKEN") String token) throws ContentNotFoundException {
//        AuthorizationUtils helper = new AuthorizationUtils();
//        helper.setUserInfoUrl(securityConfig.getUserInfoUrl());
//        UserInfo userInfo = helper.getUserInfo(token);
//
//        if(userInfo != null) {
//            String email = userInfo.getEmail();
//            Subscriber subscriber = new Subscriber(email);
//
//            PortalSubscribers communitySubscribers = portalSubscribersDAO.findByPid(pid);
//            if (communitySubscribers == null) {
//                throw new ContentNotFoundException("Community Subscribers not found");
//            }
//
//            Subscriber savedSubscriber = subscriberDAO.findByEmail(email);
//            if (savedSubscriber == null) {
//                savedSubscriber = subscriberDAO.save(subscriber);
//            }
//            for (Subscriber sub : communitySubscribers.getSubscribers()) {
//                if (sub.getEmail().equals(subscriber.getEmail())) {
//                    //already subscribed
//                    return false;
//                }
//            }
//            //not subscribed yet
//            communitySubscribers.getSubscribers().add(savedSubscriber);
//            portalSubscribersDAO.save(communitySubscribers);
//            return true;
//        }
//        return false;
//
//    }
//    @RequestMapping(value = "/community/{pid}/subscriber/delete", method = RequestMethod.POST)
//    public Boolean deleteSubscriberFromPortal(@PathVariable(value="pid", required = true) String pid,
//                                                 @RequestHeader("X-XSRF-TOKEN") String token) throws ContentNotFoundException {
//        AuthorizationUtils helper = new AuthorizationUtils();
//        helper.setUserInfoUrl(securityConfig.getUserInfoUrl());
//        UserInfo userInfo = helper.getUserInfo(token);
//
//        if(userInfo != null) {
//            String email = userInfo.getEmail();
//
//            PortalSubscribers communitySubscribers = portalSubscribersDAO.findByPid(pid);
//            if (communitySubscribers == null) {
//                throw new ContentNotFoundException("Community Subscribers not found");
//            }
//
//            Iterator<Subscriber> subscriberIterator = communitySubscribers.getSubscribers().iterator();
//            while(subscriberIterator.hasNext()) {
//                Subscriber subscriber = subscriberIterator.next();
//                if(subscriber.getEmail().equals(email)) {
//                    subscriberIterator.remove();
//                    portalSubscribersDAO.save(communitySubscribers);
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    @RequestMapping(value = "/community/{pid}/subscribers", method = RequestMethod.POST)
//    public PortalSubscribers addSubscriberInPortalByEmail(@PathVariable(value="pid", required = true) String pid, @RequestBody Subscriber subscriber) throws ContentNotFoundException {
//        PortalSubscribers communitySubscribers = portalSubscribersDAO.findByPid(pid);
//        if(communitySubscribers == null){
//            throw new ContentNotFoundException("Community Subscribers not found");
//        }
//
//        Subscriber savedSubscriber = subscriberDAO.findByEmail(subscriber.getEmail());
//        if(savedSubscriber==null){
//            savedSubscriber = subscriberDAO.save(subscriber);
//        }
//        for(Subscriber sub:communitySubscribers.getSubscribers()){
//            if(sub.getEmail().equals(subscriber.getEmail())){
//                //already subscribed
//                return communitySubscribers;
//            }
//        }
//        //not subscribed yet
//        communitySubscribers.getSubscribers().add(savedSubscriber);
//        return portalSubscribersDAO.save(communitySubscribers);
//
//    }
//    @RequestMapping(value = "/community/{pid}/subscribers/delete", method = RequestMethod.POST)
//    public PortalSubscribers deleteSubscriberFromPortalByEmail(@PathVariable(value="pid", required = true) String pid, @RequestBody List<String> emails) throws ContentNotFoundException {
//        PortalSubscribers communitySubscribers = portalSubscribersDAO.findByPid(pid);
//        if(communitySubscribers == null){
//            throw new ContentNotFoundException("Community Subscribers not found");
//        }
//        List<Subscriber> list = new ArrayList<>();
//        for(Subscriber s:communitySubscribers.getSubscribers()){
//            if(emails.indexOf(s.getEmail())==-1){
//                list.add(s);
//            }
//        }
//        communitySubscribers.setSubscribers(list);
//        return portalSubscribersDAO.save(communitySubscribers);
//    }
//
//    @RequestMapping(value = "/subscriber/communities", method = RequestMethod.GET)
//    public List<String> getPortalsPerSubcriber(//@RequestParam(value="email", required = true) String email,
//                                                   @RequestHeader("X-XSRF-TOKEN") String token) {
//        AuthorizationUtils helper = new AuthorizationUtils();
//        helper.setUserInfoUrl(securityConfig.getUserInfoUrl());
//        UserInfo userInfo = helper.getUserInfo(token);
//
//        List<String> list = new ArrayList<>();
//
//        if (userInfo != null) {
//            String email = userInfo.getEmail();
//            List<PortalSubscribers> communitySubscribers = portalSubscribersDAO.findAll();
//
//            for (PortalSubscribers s : communitySubscribers) {
//                for (Subscriber sub : s.getSubscribers()) {
//                    if (sub.getEmail().equals(email)) {
//                        list.add(s.getPid());
//                        break;
//                    }
//                }
//            }
//        }
//        return list;
//    }
}
