package eu.dnetlib.uoaadmintools.controllers;

import eu.dnetlib.uoaadmintools.entities.curator.Curator;
import eu.dnetlib.uoaadmintools.entities.curator.CuratorResponse;
import eu.dnetlib.uoaadmintools.services.CuratorService;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.utils.RolesUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class CuratorController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private CuratorService curatorService;

    @Autowired
    private RolesUtils rolesUtils;

    /**
     * Return a list with curator for a specific community
     *
     * @param pid
     * @return
     */
    @RequestMapping(value = "/{pid}/curator", method = RequestMethod.GET)
    public List<CuratorResponse> getCurators(@PathVariable String pid) {
        return curatorService.getCurators(pid);
    }

    /**
     * Return Curator info of logged in user.
     *
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/curator", method = RequestMethod.GET)
    public Curator getCuratorById() {
        Curator curator = curatorService.findById(getId());
        if(curator != null) {
            return curator;
        }
        throw new ContentNotFoundException("No curator found");
    }

    /**
     * Create or update a curator, base on Curator object given on Request Body.
     *
     * @param curator
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/curator", method = RequestMethod.POST)
    public Curator insertCurator(@RequestBody Curator curator) {
        curator.setId(getId());
        return curatorService.save(curator);
    }


    /**
     * Delete all curators for a spedific community.
     *
     * @param pid
     */
    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/{pid}/curator", method = RequestMethod.DELETE)
    public void deleteCurators(@PathVariable String pid) {
        curatorService.deleteCurators(pid);
    }

    private String getId() {
        String aaiId = rolesUtils.getAaiId();
        return  aaiId.substring(0, aaiId.indexOf("@"));
    }
}
