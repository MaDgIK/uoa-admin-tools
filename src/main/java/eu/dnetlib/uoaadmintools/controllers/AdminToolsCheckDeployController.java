package eu.dnetlib.uoaadmintools.controllers;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DBObject;
import eu.dnetlib.uoaadmintools.configuration.GlobalVars;
import eu.dnetlib.uoaadmintools.configuration.mongo.MongoConnection;
import eu.dnetlib.uoaadmintools.configuration.properties.BrowserCacheConfig;
import eu.dnetlib.uoaadmintools.configuration.properties.ManagersApiConfig;
import eu.dnetlib.uoaadmintools.configuration.properties.MongoConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class AdminToolsCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private MongoConnection mongoConnection;

    @Autowired
    private MongoConfig mongoConfig;

    @Autowired
    private ManagersApiConfig managersApiConfig;

    @Autowired
    private BrowserCacheConfig browserCacheConfig;

    @Autowired
    private GlobalVars globalVars;

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from uoa-admin-tools!");
        return "Hello from uoa-admin-tools!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();

        MongoTemplate mt = mongoConnection.getMongoTemplate();
        DBObject ping = new BasicDBObject("ping", "1");
        try {
            CommandResult answer = mt.getDb().command(ping);
            response.put("Mongo try: error", answer.getErrorMessage());
        } catch (Exception e) {
            response.put("Mongo catch: error", e.getMessage());
        }

        response.put("admintool.mongodb.database", mongoConfig.getDatabase());
        response.put("admintool.mongodb.host", mongoConfig.getHost());
        response.put("admintool.mongodb.port", mongoConfig.getPort()+"");
        response.put("admintool.mongodb.username", mongoConfig.getUsername() == null ? null : "[unexposed value]");
        response.put("admintool.mongodb.password", mongoConfig.getPassword() == null ? null : "[unexposed value]");
//        response.put("Define also", "admintool.mongodb.username, admintool.mongodb.password");

        response.put("admintool.managers.api.id", managersApiConfig.getId());
        response.put("admintool.managers.api.email", managersApiConfig.getEmail());

        response.put("admintool.cache.url", browserCacheConfig.getUrl());

        if(globalVars.date != null) {
            response.put("Date of deploy", globalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if(globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }

        return response;
    }
}
