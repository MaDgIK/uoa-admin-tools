package eu.dnetlib.uoaadmintools.controllers;

import eu.dnetlib.uoaadmintools.entities.Layout;
import eu.dnetlib.uoaadmintools.services.*;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PortalResponse;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.MismatchingContentException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.utils.RolesUtils;
import eu.dnetlib.uoaadmintoolslibrary.services.PageService;
import eu.dnetlib.uoaadmintoolslibrary.services.PortalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/community")
@CrossOrigin(origins = "*")
public class CommunityController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private RolesUtils rolesUtils;

    @Autowired
    private LayoutService layoutService;

    @Autowired
    private NotificationsService notificationsService;

    @Autowired
    private StatisticsService statisticsService;

    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private PortalService portalService;

    @Autowired
    private PageService pageService;

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public List<Portal> getAllCommunities() {
        return portalService.getAllPortalsByType("community");
    }

    @RequestMapping(value = {"/full"}, method = RequestMethod.GET)
    public List<PortalResponse> getAllCommunitiesFull() {
        return portalService.getAllPortalsFullByType("community");
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public PortalResponse updateCommunity(@RequestBody Portal portal) {
        if(!portal.getType().equals("community")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("Update Community: Portal with id: "+portal.getId()+" has type: "+portal.getType()+" instead of community");
        }

        String old_pid = portalService.getPortalById(portal.getId()).getPid();
        String new_pid = portal.getPid();

        PortalResponse portalResponse = portalService.updatePortal(portal);

        if(!old_pid.equals(new_pid)) {
            log.debug("update portal pid - old: "+old_pid + " - new: "+new_pid);
            statisticsService.updatePid(old_pid, new_pid);
            subscriberService.updatePid(old_pid, new_pid);
            layoutService.updatePid(old_pid, new_pid);
            notificationsService.updatePid(old_pid, new_pid);
            menuService.updatePid(old_pid, new_pid);
            pageService.updatePid(old_pid, new_pid, portal.getType());
        }

        return portalResponse;
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public PortalResponse insertCommunity(@RequestBody Portal portal) {
        if(!portal.getType().equals("community")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("Save Community: Portal with id: "+portal.getId()+" has type: "+portal.getType()+" instead of community");
        }

        PortalResponse portalResponse = portalService.insertPortal(portal);

        statisticsService.createPortalStatistics(portal.getPid());
        subscriberService.createPortalSubscribers(portal.getPid());

        return portalResponse;
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public Boolean deleteCommunities(@RequestBody List<String> portals) throws Exception {
        for (String id: portals) {
            Portal portal = portalService.getPortalById(id);
            if(portal == null) {
                // EXCEPTION - Entity Not Found
                throw new ContentNotFoundException("Delete community: Portal with id: " + id + " not found");
            }
            if(!portal.getType().equals("community")) {
                // EXCEPTION - MismatchingContent
                throw new MismatchingContentException("Delete Community: Portal with id: "+id+" has type: "+portal.getType()+" instead of community");
            }

            String pid = portalService.deletePortal(id);

            statisticsService.deleteByPid(pid);
            subscriberService.deletePortalSubscribers(pid);
            layoutService.deleteByPid(pid);
            notificationsService.deleteByPid(pid);
            menuService.deleteMenuByPortalPid(pid);
        }

        return true;
    }

    @RequestMapping(value = "/layouts", method = RequestMethod.GET)
    public List<Layout> getAvailableLayouts() {
        return this.layoutService.findAll();
    }

    @RequestMapping(value = "/{pid}/layout", method = RequestMethod.GET)
    public Layout getLayoutForCommunity(@PathVariable(value = "pid") String pid) {
        Portal portal = portalService.getPortal(pid);
        if(portal == null) {
            // EXCEPTION - Entity Not Found
            throw new ContentNotFoundException("CommunityController - Get layout: Portal with pid: " + pid + " not found");
        }
        if(!portal.getType().equals("community")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("CommunityController - Get layout: Portal with pid: "+pid+" has type: "+portal.getType()+" instead of community");
        }
        return layoutService.findByPid(pid);
    }

    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'), @AuthorizationService.manager('community', #pid))")
    @RequestMapping(value = "/{pid}/layout", method = RequestMethod.POST)
    public Layout updateLayoutForCommunity(@PathVariable(value = "pid") String pid, @RequestBody Layout layout) {
        Portal portal = portalService.getPortal(pid);
        if(portal == null) {
            // EXCEPTION - Entity Not Found
            throw new ContentNotFoundException("CommunityController - Update layout: Portal with pid: " + pid + " not found");
        }
        if(!portal.getType().equals("community")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("CommunityController - Update layout: Portal with pid: "+pid+" has type: "+portal.getType()+" instead of community");
        }
        if(!pid.equals(layout.getPortalPid())) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("CommunityController - Update layout: Portal has pid: "+pid+" while layout has portalPid: "+layout.getPortalPid());
        }
        return layoutService.save(layout);
    }

    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator('community'))")
    @RequestMapping(value = "/{pid}/layout", method = RequestMethod.DELETE)
    public boolean deleteLayoutForCommunity(@PathVariable(value = "pid") String pid) {
        Portal portal = portalService.getPortal(pid);
        if(portal == null) {
            // EXCEPTION - Entity Not Found
            throw new ContentNotFoundException("CommunityController - Delete layout: Portal with pid: " + pid + " not found");
        }
        if(!portal.getType().equals("community")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("CommunityController - Delete layout: Portal with pid: "+pid+" has type: "+portal.getType()+" instead of community");
        }
        return layoutService.deleteByPid(pid);
    }
}

