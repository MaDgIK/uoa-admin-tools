//package eu.dnetlib.uoaadmintools.controllers;
//
//import eu.dnetlib.uoaadmintools.dao.*;
//import eu.dnetlib.uoaadmintools.entities.statistics.*;
//import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.*;
//
//@RestController
//@CrossOrigin(origins = "*")
//public class StatisticsController {
//    private final Logger log = Logger.getLogger(this.getClass());
//
//    @Autowired
//    private StatisticsDAO statisticsDAO;
//
//    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
//    public List<Statistics> getAllStatistics() throws ContentNotFoundException {
//        log.info("getAllStatistics");
//            List<Statistics> statistics = statisticsDAO.findAll();
//         if(statistics == null){
//            throw new ContentNotFoundException("Statistics not found");
//        }
//            return statistics;
//    }
//
//
//
//    @RequestMapping(value = "/statistics/{pid}", method = RequestMethod.GET)
//    public Statistics getStatistics(@PathVariable(value = "pid") String pid) throws ContentNotFoundException {
//        Statistics statistics = statisticsDAO.findByPid(pid);
//        if(statistics == null){
//            throw new ContentNotFoundException("Statistics not found");
//        }
//        return statistics;
//    }
//
//    @RequestMapping(value = "/statistics/{id}/toggle", method = RequestMethod.POST)
//    public Boolean toggleStatistics(@PathVariable String id) throws Exception {
//        Statistics statistics = statisticsDAO.findById(id);
//        boolean status = statistics.getIsActive();
//        statistics.setIsActive(!status);
//        statisticsDAO.save(statistics);
//        return statistics.getIsActive();
//    }
//
//    @RequestMapping(value = "/statistics/save", method = RequestMethod.POST)
//    public Statistics insertStatistics(@RequestBody Statistics statistics) {
//        Statistics savedStatistics = statisticsDAO.save(statistics);
//        return savedStatistics;
//    }
//
//
//
//    @RequestMapping(value = "/statistics/delete", method = RequestMethod.POST)
//    public Boolean deleteStatistics(@RequestBody List<String> statistics) throws Exception {
//        for (String id: statistics) {
//            statisticsDAO.delete(id);
//         }
//        return true;
//    }
//
//
//
//
//    @RequestMapping(value = "statistics/{pid}/{entity}/charts", method = RequestMethod.POST)
//    public Statistics toggleCharts(@PathVariable(value = "pid") String pid, @PathVariable(value = "entity") String entity, @RequestBody String key, @RequestParam String status, @RequestParam String monitor) throws ContentNotFoundException {
//        Statistics statistics = statisticsDAO.findByPid(pid);
//        if(statistics == null){
//            throw new ContentNotFoundException("Statistics not found  for portal");
//        }
//        StatisticsEntity statisticsEntity = statistics.getEntities().get(entity);
//        if(statisticsEntity == null ){
//            throw new ContentNotFoundException("Statistics not found for entity");
//        }
//        ChartsMap charts = statisticsEntity.getCharts();
//        if(charts == null){
//            throw new ContentNotFoundException("Statistics not found - no charts");
//        }
//        StatisticsStatus statisticsStatus= charts.getMap().get(key);
//        if(statisticsStatus == null){
//            throw new ContentNotFoundException("Statistics not found for key");
//        }
//        if(Boolean.parseBoolean(monitor)){
//            statisticsStatus.setShowInMonitor(Boolean.parseBoolean(status));
//        }else{
//            statisticsStatus.setShowInDashboard(Boolean.parseBoolean(status));
//        }
////        stats.put(key,statisticsStatus);
//        return statisticsDAO.save(statistics);
//    }
//    @RequestMapping(value = "statistics/{pid}/{entity}/numbers", method = RequestMethod.POST)
//    public Statistics toggleNumber(@PathVariable(value = "pid") String pid, @PathVariable(value = "entity") String entity, @RequestBody String key, @RequestParam String status, @RequestParam String monitor) throws ContentNotFoundException {
//        Statistics statistics = statisticsDAO.findByPid(pid);
//        if(statistics == null){
//            throw new ContentNotFoundException("Statistics not found  for portal");
//        }
//        StatisticsEntity statisticsEntity = statistics.getEntities().get(entity);
//        if(statisticsEntity == null ){
//            throw new ContentNotFoundException("Statistics not found for entity");
//        }
//        NumbersMap numbers = statisticsEntity.getNumbers();
//        if(numbers == null){
//            throw new ContentNotFoundException("Statistics not found - no numbers");
//        }
//        StatisticsStatus statisticsStatus= numbers.getMap().get(key);
//        if(statisticsStatus == null){
//            throw new ContentNotFoundException("Statistics not found for key");
//        }
//        if(Boolean.parseBoolean(monitor)){
//            statisticsStatus.setShowInMonitor(Boolean.parseBoolean(status));
//        }else{
//            statisticsStatus.setShowInDashboard(Boolean.parseBoolean(status));
//        }
////        stats.put(key,statisticsStatus);
//        return statisticsDAO.save(statistics);
//    }
//
//
//}