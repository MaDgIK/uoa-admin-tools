package eu.dnetlib.uoaadmintools.controllers;

import eu.dnetlib.uoaadmintools.entities.Layout;
import eu.dnetlib.uoaadmintools.services.LayoutService;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.PortalResponse;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.MismatchingContentException;
import eu.dnetlib.uoaadmintoolslibrary.services.PageService;
import eu.dnetlib.uoaadmintoolslibrary.services.PortalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/connect")
@CrossOrigin(origins = "*")
public class ConnectController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private LayoutService layoutService;

    @Autowired
    private PortalService portalService;

    @Autowired
    private PageService pageService;

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public PortalResponse updateConnect(@RequestBody Portal portal) {
        if (!portal.getType().equals("connect")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("Update Connect: Portal with id: " + portal.getId() + " has type: " + portal.getType() + " instead of connect");
        }

        String old_pid = portalService.getPortalById(portal.getId()).getPid();
        String new_pid = portal.getPid();

        PortalResponse portalResponse = portalService.updatePortal(portal);

        if (!old_pid.equals(new_pid)) {
            layoutService.updatePid(old_pid, new_pid);
            pageService.updatePid(old_pid, new_pid, portal.getType());
        }

        return portalResponse;
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public PortalResponse insertConnect(@RequestBody Portal portal) {
        if (!portal.getType().equals("connect")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("Save Connect: Portal with id: " + portal.getId() + " has type: " + portal.getType() + " instead of connect");
        }

        PortalResponse portalResponse = portalService.insertPortal(portal);
        return portalResponse;
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public Boolean deleteConnect(@RequestBody List<String> portals) {
        for (String id : portals) {
            Portal portal = portalService.getPortalById(id);
            if (portal == null) {
                // EXCEPTION - Entity Not Found
                throw new ContentNotFoundException("Delete connect: Portal with id: " + id + " not found");
            }
            if (!portal.getType().equals("connect")) {
                // EXCEPTION - MismatchingContent
                throw new MismatchingContentException("Delete Connect: Portal with id: " + id + " has type: " + portal.getType() + " instead of connect");
            }

            String pid = portalService.deletePortal(id);
            layoutService.deleteByPid(pid);
        }

        return true;
    }

    // no authorization here, because it is called by server
    @RequestMapping(value = "/{pid}/layout", method = RequestMethod.GET)
    public Layout getLayoutForConnect(@PathVariable(value = "pid") String pid) {
        if(!pid.equals("connect") && !pid.equals("default")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("ConnectController - Get layout: Not accepted pid: "+pid);
        }
        return layoutService.findByPid(pid);
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/{pid}/layout", method = RequestMethod.POST)
    public Layout updateLayoutForConnect(@PathVariable(value = "pid") String pid, @RequestBody Layout layout) {
        if(!pid.equals("connect") && !pid.equals("default")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("ConnectController - Update layout: Not accepted pid: "+pid);
        }
        if(!pid.equals(layout.getPortalPid())) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("ConnectController - Update layout: Portal has pid: "+pid+" while layout has portalPid: "+layout.getPortalPid());
        }
        return layoutService.save(layout);
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/{pid}/layout", method = RequestMethod.DELETE)
    public boolean deleteLayoutForConnect(@PathVariable(value = "pid") String pid) {
        if(!pid.equals("connect") && !pid.equals("default")) {
            // EXCEPTION - MismatchingContent
            throw new MismatchingContentException("ConnectController - Delete layout: Not accepted pid: "+pid);
        }
        return layoutService.deleteByPid(pid);
    }
}

