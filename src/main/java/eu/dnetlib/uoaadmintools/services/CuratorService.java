package eu.dnetlib.uoaadmintools.services;


import eu.dnetlib.uoaadmintools.dao.CuratorDAO;
import eu.dnetlib.uoaadmintools.entities.Manager;
import eu.dnetlib.uoaadmintools.entities.curator.Curator;
import eu.dnetlib.uoaadmintools.entities.curator.CuratorResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CuratorService {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private CuratorDAO curatorDAO;

    @Autowired
    private ManagerService managerService;


    public List<CuratorResponse> getCurators(String pid) {
        List<CuratorResponse> curators = new ArrayList<>();
        for (Manager manager : managerService.getManagers(pid, ManagerService.Type.ID)) {
            Curator curator = curatorDAO.findById(parseId(manager.getId()));
            if (curator != null && curator.isVisible()) {
                curators.add(new CuratorResponse(curator));
            }
        }
        return curators;
    }

    public Curator findById(String id) {
        return curatorDAO.findById(id);
    }

    public Curator save(Curator curator) {
        return curatorDAO.save(curator);
    }

    public void deleteCurators(String pid) {
        for (Manager manager : managerService.getManagers(pid, ManagerService.Type.ID)) {
            Curator curator = curatorDAO.findById(parseId(manager.getId()));
            if (curator != null) {
                curatorDAO.delete(curator.getId());
            }
        }
    }

    private String parseId(String id) {
        return id.substring(0 , id.indexOf("@"));
    }

}