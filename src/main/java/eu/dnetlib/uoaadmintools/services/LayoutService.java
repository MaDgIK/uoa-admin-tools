package eu.dnetlib.uoaadmintools.services;

import eu.dnetlib.uoaadmintools.dao.LayoutDAO;
import eu.dnetlib.uoaadmintools.entities.Layout;
import eu.dnetlib.uoaadmintoolslibrary.handlers.MismatchingContentException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;

import java.util.List;

@Service
public class LayoutService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private LayoutDAO layoutDAO;

    public List<Layout> findAll() {
        return this.layoutDAO.findAll();
    }

    public void updatePid(String old_pid, String new_pid) {
        log.debug("layout service: updatePid");
        List<Layout> layouts = layoutDAO.findByPortalPid(old_pid);
        for(Layout layout : layouts) {
            log.debug("layout service: layout id: " + (layout != null ? layout.getId() : "not found"));
            if (layout != null) {
                layout.setPortalPid(new_pid);
                log.debug("layout layout: new layout pid: " + layout.getPortalPid());
                layoutDAO.save(layout);
                log.debug("layout saved!");
            }
        }
    }

    public boolean deleteByPid(String pid) {
        List<Layout> layouts = layoutDAO.findByPortalPid(pid);
        for(Layout layout : layouts) {
            if (layout != null) {
                if(!pid.equals(layout.getPortalPid())) {
                    // EXCEPTION - MismatchingContent
                    throw new MismatchingContentException("Delete layout by pid: Portal has pid: "+pid+" while layout has portalPid: "+layout.getPortalPid());
                }
                layoutDAO.delete(layout.getId());
            }
        }
        return true;
    }

    public Layout findByPid(String pid) {
        List<Layout> layouts = layoutDAO.findByPortalPid(pid);
        if(layouts != null && layouts.size() > 0) {
            return layouts.get(0);
        }
        return null;
    }

    public Layout save(Layout layout) {
        List<Layout> oldLayouts = layoutDAO.findByPortalPid(layout.getPortalPid());
        if(oldLayouts != null && oldLayouts.size() == 1) {
            layout.setId(oldLayouts.get(0).getId());    // set existing id to update layout for this pid
        }
        return layoutDAO.save(layout);
    }
}
