package eu.dnetlib.uoaadmintools.services;

import eu.dnetlib.uoaadmintools.configuration.properties.ManagersApiConfig;
import eu.dnetlib.uoaadmintools.entities.Manager;
import eu.dnetlib.uoaadmintools.entities.curator.Response;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ManagerService {

    enum Type {
        EMAIL,
        ID
    }

    @Autowired
    private ManagersApiConfig config;

    @Autowired
    private RestTemplate restTemplate;

    public Manager[] getManagers(String pid) {
        return getManagers(pid, Type.EMAIL);
    }

    public Manager[] getManagers(String pid, Type type) {
        String url = (type == Type.EMAIL)?config.getEmail():config.getId();
        ResponseEntity<Manager[]> responseEntity = restTemplate.getForEntity(url.replace("{community}", pid), Manager[].class);
        Manager[] managers = responseEntity.getBody();
        if (managers != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            return managers;
        } else {
            throw new ContentNotFoundException("No managers has been found for community " + pid);
        }
    }
}
