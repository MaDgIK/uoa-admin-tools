package eu.dnetlib.uoaadmintools.services;

import eu.dnetlib.uoaadmintools.dao.StatisticsDAO;
import eu.dnetlib.uoaadmintools.entities.statistics.Statistics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private StatisticsDAO statisticsDAO;

    public void updatePid(String old_pid, String new_pid) {
        log.debug("statistics service: updatePid");
        Statistics statistics = statisticsDAO.findByPid(old_pid);
        log.debug("statistics service: statistics id: "+(statistics != null ? statistics.getId() : "not found"));
        if(statistics != null) {
            statistics.setPid(new_pid);
            log.debug("statistics service: new statistics pid: " + statistics.getPid());
            statisticsDAO.save(statistics);
            log.debug("statistics saved!");
        }
    }

    public void createPortalStatistics(String pid) {
        Statistics statistics =  new Statistics(pid);
        statisticsDAO.save(statistics);
    }

    public void deleteByPid(String pid) {
        Statistics stats = statisticsDAO.findByPid(pid);
        if(stats != null) {
            statisticsDAO.delete(stats.getId());
        }
    }
}
