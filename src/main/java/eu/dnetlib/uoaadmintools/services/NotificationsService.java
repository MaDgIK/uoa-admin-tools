package eu.dnetlib.uoaadmintools.services;

import eu.dnetlib.uoaadmintools.dao.NotificationsDAO;
import eu.dnetlib.uoaadmintools.entities.Notifications;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationsService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private NotificationsDAO notificationsDAO;

    public void updatePid(String old_pid, String new_pid) {
        log.debug("notifications service: updatePid");
        List<Notifications> notifications = notificationsDAO.findByPortalPid(old_pid);
        if(notifications != null) {
            notifications.forEach(notification -> {
                        notification.setPortalPid(new_pid);
                        notificationsDAO.save(notification);
                    });
            log.debug("notifications saved!");
        }
    }

    public void deleteByPid(String pid) {
        // TODO check maybe we can delete by portalId without find first
        List<Notifications> notifications = notificationsDAO.findByPortalPid(pid);
        if(notifications != null) {
            notifications.forEach(notification -> notificationsDAO.delete(notification.getId()));
        }
    }
}
