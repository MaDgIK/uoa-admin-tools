package eu.dnetlib.uoaadmintools;

import eu.dnetlib.uoaadmintools.dao.*;
import eu.dnetlib.uoaadmintoolslibrary.dao.*;
import eu.dnetlib.uoaadmintoolslibrary.emailSender.EmailSender;
import eu.dnetlib.uoaadmintoolslibrary.entities.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UoaAdminToolsApplicationTests {

	@Autowired
	private PortalDAO communityDAO;

	@Autowired
	private EntityDAO entityDAO;

	@Autowired
	private PageDAO pageDAO;

	@Autowired
	private PageHelpContentDAO pageHelpContentDAO;

	@Autowired
	private DivIdDAO divIdDAO;

	@Autowired
	private StatisticsDAO statisticsDAO;

	@Autowired
	private SubscriberDAO subscriberDAO;

	@Autowired
	private PortalSubscribersDAO portalSubscribersDAO;

	@Autowired
	private EmailSender emailSender;
/*

	@Test
	public void createAllEntities() {
		Map<String, String> entities = new HashMap<>();
		entities.put("Publication", "publication");
		entities.put("Research Data", "dataset");
		entiites.put("Software", "software");
		entities.put("Project", "project");
		entities.put("Organization", "organization");
		entities.put("Content Provider", "datasource");

		for(entry<String, String> entr : entities.entrySet()) {
			Entity entity = new Entity();
			entity.setName(entr.getKey());
			entity.setPid(entr.getValue());
			entityDAO.save(entity);
		}
	}

	@Test
	public void createAllPages() {
		String publication = entityDAO.findByPid("publication").getId();
		String dataset = entityDAO.findByPid("dataset").getId();
		String software = entityDAO.findByPid("software").getId();
		String project = entityDAO.findByPid("project").getId();
		String organization = entityDAO.findByPid("organization").getId();
		String datasource = entityDAO.findByPid("datasource").getId();


		Map<String, List<String>> pages = new HashMap<>();
		pages.put("Search Publications", new ArrayList<>());
		pages.get("Search Publications").add()

		//entities.add("5a300ee4c97a7d2c000b9e9d");	// Publication
		entities.add("5a319f8bc97a7d241ebddf75");	// Research Data


		Page page = new Page();
		page.setName("Search Research Data");
		page.setRoute("/search/find/datasets");
		page.setType("search");
		page.setEntities(entities);
		pageDAO.save(page);
	}



	@Test
	public void deleteAllEntities() {
		List<Entity> entities = entityDAO.findAll();
		for(Entity entity : entities) {
			entityDAO.delete(entity.getId());
		}
	}

	@Test
	public void deleteAllCommunities() {
		List<Community> communities = communityDAO.findAll();
		for(Community community : communities) {
			communityDAO.delete(community.getId());
		}
	}

	@Test
	public void deleteAllPages() {
		List<Page> pages = pageDAO.findAll();
		for(Page page : pages) {
			pageDAO.delete(page.getId());
		}
	}

	@Test
	public void deleteAllPageContents() {
		List<PageHelpContent> pageHelpContents = pageHelpContentDAO.findAll();
		for(PageHelpContent pageHelpContent : pageHelpContents) {
			pageHelpContentDAO.delete(pageHelpContent.getId());
		}
	}

	@Test
	public void createEntity() {
		Entity entity = new Entity();
		entity.setName("Publication");
		entityDAO.save(entity);
	}

	@Test
	public void createPage() {
		List<String> entities = new ArrayList<String>();
		//entities.add("5a300ee4c97a7d2c000b9e9d");	// Publication
		entities.add("5a319f8bc97a7d241ebddf75");	// Research Data


		Page page = new Page();
		page.setName("Search Research Data");
		page.setRoute("/search/find/datasets");
		page.setType("search");
		page.setEntities(entities);
		pageDAO.save(page);
	}

	@Test
	public void createCommunity() {
		Map<String, Boolean> entities = new HashMap<String, Boolean>();
		//entities.put("5a300ee4c97a7d2c000b9e9d", true);	// Publication
		entities.put("5a319f8bc97a7d241ebddf75", false);	// Research Data
		//entities.put("5a3064f9c97a7d4964408ee0", false);	// Software
		//entities.put("5a30652ec97a7d4964408ee1", false);	// Organization
		//entities.put("5a306539c97a7d4964408ee2", false);	// Project
		//entities.put("5a30654dc97a7d4964408ee3", false);	// Content Provider

		Map<String, Boolean> pages = new HashMap<String, Boolean>();
		//pages.put("5a30191ac97a7d3a7fa77362", true);		// Search Publications
		pages.put("5a31a4cac97a7d28468f0f5c", false);		// Search Research Data
		//pages.put("5a30657ac97a7d4964408ee4", false);		// Search Software
		//pages.put("5a30eeffc97a7d74234434ad", false);		// Search Organizations
		//pages.put("5a30efd9c97a7d7609b77ca5", false);		// Search Projects
		//pages.put("5a30f08bc97a7d7609b77ca7", false);		// Search Content Providers

		//pages.put("5a300f19c97a7d2c6e084b9f", false);		// Test

		Community community = new Community();
		community.setName("Test Community");
		community.setEntities(entities);
		community.setPages(pages);
		communityDAO.save(community);
	}
*/

	@Test
	public void contextLoads() {
	}

//	@Test
//	public void createStatistics() {
//		statisticsDAO.deleteAll();
//		List<Community> communities =  communityDAO.findAll();
//		for(Community c:communities){
//
//			String pid = c.getPid();
//			System.out.println("Community :" +pid);
//			Statistics stats = statisticsDAO.findByPid(pid);
//			if(stats == null){
//				Statistics statistics = new Statistics(pid);
//				statisticsDAO.save(statistics);
//				System.out.println("Saved stats for :" +pid);
//			}
//		}
//
//	}

//		@Test
//	public void deleteStatistics() {
//		statisticsDAO.deleteAll();
//	}
		@Test
	public void createCommunitySubscribers() {

//			List<Community> communities =  communityDAO.findAll();
//			for(Community c:communities){
//				String pid = c.getPid();
//				System.out.println("Community :" +pid);
//				PortalSubscribers communitySubscribers= portalSubscribersDAO.findByPid(pid);
//				if(communitySubscribers == null){
//					communitySubscribers = new PortalSubscribers(pid);
//					portalSubscribersDAO.save(communitySubscribers);
//					System.out.println("Saved community subscribers for :" +pid);
//				}
//			}
	}

//	@Test
//	public void testPages() {
//
//		System.out.println( pageDAO.findByConnect(false).size());
//		for (Page p: pageDAO.findByConnect(false) ){
//			System.out.println(p.getRoute()+" Con: "+p.getConnect()+" Op: "+p.getOpenaire());
//		}
//	}

	@Test
	public void test() {
		String mail = "kgalouni@di.uoa.gr";
		List<String> mails = new ArrayList<>();
		mails.add(mail);
		emailSender.send(mails, "Test Subject", "Test Body", false);
	}
}
