package eu.dnetlib.uoaadmintools;

import eu.dnetlib.uoaadmintools.dao.*;
import eu.dnetlib.uoaadmintools.entities.Notifications;
import eu.dnetlib.uoaadmintoolslibrary.dao.PortalDAO;
import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NotificationsTests {

	@Autowired
	private PortalDAO communityDAO;


	@Autowired
	private NotificationsDAO notificationsDAO;

	@Test
	public void test() {
		String mail = "sofie.mpl@gmail.com";
		String id="ni";
			System.out.println(notificationsDAO.findByPortalPid(id));

		Notifications notifications = notificationsDAO.findByManagerEmailAndPortalPid(mail	, id);
		if(notifications == null){
			notifications = new Notifications();
			notifications.setPortalPid(id);
			notifications.setManagerEmail(mail);
		}
		notifications.setNotifyForNewManagers(false);
		notifications.setNotifyForNewSubscribers(false);

		notificationsDAO.save(notifications);
		System.out.println(notificationsDAO.findByPortalPid(id));




	}

	@Test
	public void remove() {
		List <Portal> communities = communityDAO.findAll();
		for(Portal com : communities){
			List <Notifications> notificationsList = notificationsDAO.findByPortalPid(com.getPid());
			for(Notifications notifications:notificationsList){
//				notificationsDAO.delete(notifications.getId());
			}
		}

	}

}
