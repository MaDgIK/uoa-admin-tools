//version compatibility: 1.1.1-SNAPSHOT
print("migrateCommunityIn_db script running...");

function migrate_portal(portalPid, beta_db_name, prod_db_name) {
	beta_db = db.getSiblingDB(beta_db_name);
  // db.auth("your_username", "your_password");
	prod_db = db.getSiblingDB(prod_db_name);
  // db.auth("your_username", "your_password");

	print("migrate_portal: "+portalPid+" - both dbs are here");

	var beta_portal = beta_db.portal.findOne({ pid: portalPid } );

	if(!beta_portal) {
		print("ERROR: Portal with pid: "+portalPid+" does not exist in (beta) database: "+ beta_db_name);
		return;
	}

	var prod_portalOld = prod_db.portal.findOne( { pid: portalPid } );
	var prod_portalOldId;
	if(prod_portalOld) {
		prod_portalOldId = prod_portalOld._id.str;
	}

	// delete portal from production db
	prod_db.portal.remove({"pid" : portalPid});
	print("Portal with pid: "+portalPid+" is deleted (if existed) from production db: "+prod_db_name);

	// migrate portal pages
	//beta_portal_pages = beta_db.portal.findOne({ pid: portalPid } ).map( function(portal) { return portal.pages } );
	var beta_portal_pages = beta_portal.pages;
	var prod_portal_pages = {}
	for (var beta_pageId in beta_portal_pages) {
		var beta_pageObjectId = ObjectId(beta_pageId);
		//var prod_pageId = prod_db.portal.find( { route: beta_db.page.find( { _id: beta_pageObjectId }).map( function(page) { return page.route; } ).toString() });
		var beta_page = beta_db.page.findOne( { _id: beta_pageObjectId } );
		if(beta_page) {
			var prod_page = prod_db.page.findOne( { route: beta_page.route, portalType: beta_page.portalType } );
			if(prod_page) {
				prod_portal_pages[prod_page._id.str] = beta_portal_pages[beta_pageId];
			} else {
				print("\nERROR: Page with route: "+beta_page.route+" does not exist on production db: "+prod_db_name+"\n");
			}
		} else {
			print("\nERROR: Page with id: "+beta_pageId+" does not exist on beta db: "+beta_db_name+"\n");
		}
	}
	print("Portal pages are ready to be migrated");

	// migrate portal entities
	var beta_portal_entities = beta_portal.entities;
	var prod_portal_entities = {}
	for (var beta_entityId in beta_portal_entities) {
		var beta_entityObjectId = ObjectId(beta_entityId);
		var beta_entity = beta_db.entity.findOne( { _id: beta_entityObjectId } );
		if(beta_entity) {
			var prod_entity = prod_db.entity.findOne( { pid: beta_entity.pid } );
			if(prod_entity) {
				prod_portal_entities[prod_entity._id.str] = beta_portal_entities[beta_entityId];
			} else {
				print("\nERROR: Entity with pid: "+beta_entity.pid+" does not exist on production db: "+prod_db_name+"\n");
			}
		} else {
			print("\nERROR: Entity with id: "+beta_entityId+" does not exist on beta db: "+beta_db_name+"\n");
		}
	}

	print("Portal entities are ready to be migrated");

 	prod_db.portal.save({
    "name" : beta_portal.name, "pid" : portalPid, "type": beta_portal.type,
    "pages" : prod_portal_pages, "entities" : prod_portal_entities
 	});

	print("Portal is migrated");

	// delete portal statistics from production db
	prod_db.statistics.remove({"pid" : portalPid});
	print("Statistics for portal with pid: "+portalPid+" are deleted (if existed) from production db: "+prod_db_name);
	// migrate statistics for portal
	var beta_statistics = beta_db.statistics.findOne({ pid: portalPid } );
	prod_db.statistics.save(beta_statistics);

	print("Statistics for portal were migrated");

	// migrate subscribers of portal
	var beta_portalSubscr = beta_db.portalSubscribers.findOne({ pid: portalPid } );
	if(beta_portalSubscr) {
		var beta_portalSubscribers = beta_portalSubscr.subscribers;
		var prod_portalSubscribers = [];
		var prod_portalSubscribersEmails = [];
		var prod_portalSubscr = prod_db.portalSubscribers.findOne({ "pid" : portalPid });
		if(prod_portalSubscr) {
			prod_portalSubscribers = prod_portalSubscr.subscribers;
			prod_portalSubscribersEmails = prod_portalSubscr.subscribers.map(function(subscriber){return subscriber.email});
		}

		beta_portalSubscribers.forEach(function(beta_portalSubscriber) {
			var addInPortal = true;

			// if user exists in portal subscribers already, do nothing
			if(prod_portalSubscribersEmails.indexOf(beta_portalSubscriber.email) >= 0) {
				addInPortal = false;
			} else {
				var prod_subscriber = prod_db.subscriber.findOne({email: beta_portalSubscriber.email});

				// if subscriber of beta does not exist in production, add him
				if(!prod_subscriber) {
					prod_subscriber = {email: beta_portalSubscriber.email};
					prod_db.subscriber.save(prod_subscriber);
					print("subscriber "+beta_portalSubscriber.email+" added in production DB");
				}
			}

			if(addInPortal) {
				var prod_portalSubscriber = prod_db.subscriber.findOne({email: beta_portalSubscriber.email});
				prod_portalSubscribers.push(prod_portalSubscriber);
			}
		})
		if(prod_portalSubscr) {
			prod_db.portalSubscribers.update({ "pid" : portalPid } , {$set: {subscribers : prod_portalSubscribers}});
		} else {
			prod_db.portalSubscribers.insert({ "pid" : portalPid, "subscribers" : prod_portalSubscribers });
		}

		print("subscribers of portal were migrated");
	} else {
		print("\nERROR: Subscribers not migrated. No PortalSubscribers for this portal on beta db: "+beta_db_name+"\n");
	}

	// migrate divHelpContents of portal
	var beta_portalId = beta_portal._id.str;//beta_db.portal.findOne( { pid: portalPid } )._id.str;
	var prod_portalId = prod_db.portal.findOne( { pid: portalPid })._id.str;

	if(prod_portalOldId) {
		prod_db.divHelpContent.remove({"portal": prod_portalOldId});
	}
	//var beta_divHelpContents = 
	beta_db.divHelpContent.find( { portal: beta_portalId } ).forEach(function(beta_divHelpContent){//.map( function(divHelpContent) { return divHelpContent; } );
	//for (var beta_divHelpContent in beta_divHelpContents) {
		var beta_beta_divHelpContent_divId_ObjectId = ObjectId(beta_divHelpContent.divId);
		var beta_divId = beta_db.divId.findOne( { _id: beta_beta_divHelpContent_divId_ObjectId } );//.name;;
		if(beta_divId) {
			var prod_divId = prod_db.divId.findOne( { name: beta_divId.name, portalType: beta_divId.portalType } );//._id.str;
			if(prod_divId) {
				prod_db.divHelpContent.save({ "divId" : prod_divId._id.str, "portal" : prod_portalId, "content" : beta_divHelpContent.content, "isActive" : beta_divHelpContent.isActive });
			} else {
				print("\nERROR: DivId with name: "+beta_divId.name+" does not exist on production db: "+prod_db_name+"\n");
			}
		} else {
			print("\nERROR: DivId with id: "+beta_divHelpContent.divId+" (of divHelpContent: "+beta_divHelpContent._id.str+") does not exist on beta db: "+beta_db_name+"\n");
		}
	//}
	});

	print("divHelpContents of portal were migrated");

	// migrate pageHelpContents of portal
	if(prod_portalOldId) {
		prod_db.pageHelpContent.remove({"portal": prod_portalOldId});
	}
	beta_db.pageHelpContent.find( { portal: beta_portalId } ).forEach(function(beta_pageHelpContent){
		var beta_beta_pageHelpContent_page_ObjectId = ObjectId(beta_pageHelpContent.page);
		var beta_page = beta_db.page.findOne( { _id: beta_beta_pageHelpContent_page_ObjectId } );//.route;
		if(beta_page) {
			var prod_page = prod_db.page.findOne( { route: beta_page.route, portalType: beta_page.portalType } );//._id.str;
			if(prod_page) {
				prod_db.pageHelpContent.save({ "page" : prod_page._id.str, "portal" : prod_portalId, "content" : beta_pageHelpContent.content, "placement": beta_pageHelpContent.placement, "order": beta_pageHelpContent.order, "isActive": beta_pageHelpContent.isActive, "isPriorTo": beta_pageHelpContent.isPriorTo });
			} else {
				print("\nERROR: Page with route: "+beta_page.route+" does not exist on production db: "+prod_db_name+"\n");
			}
		} else {
			print("\nERROR: Page with id: "+beta_pageHelpContent.page+" (of pageHelpContent: "+beta_pageHelpContent._id.str+") does not exist on beta db: "+beta_db_name+"\n");
		}
	});

	print("pageHelpContents of portal were migrated");

	// migrate notifications preferences of portal
	var prod_notifications = prod_db.notifications.find({ portalPid: portalPid } );
	var prod_notifications_emails = [];
	if(prod_notifications) {
		prod_notifications_emails = prod_notifications.map(function(preference){return preference.managerEmail;})
	}

	beta_db.notifications.find({ portalPid: portalPid } ).forEach(function(preference){
		if(prod_notifications_emails.indexOf(preference.managerEmail) >= 0) {
			prod_db.notifications.update({ managerEmail: preference.managerEmail, portalPid: portalPid }, {$set: {notifyForNewManagers: preference.notifyForNewManagers, notifyForNewSubscribers: preference.notifyForNewSubscribers}})
		} else {
			prod_db.notifications.insert(preference);
		}
	});

	print("Notification preferences of portal were migrated");

}

function migrateCurators(beta_db_name, prod_db_name) {
  beta_db = db.getSiblingDB(beta_db_name);
  // beta_db.auth("your_username", "your_password");

  prod_db = db.getSiblingDB(prod_db_name);
  // prod_db.auth("your_username", "your_password");

  print("migrateCurators: both dbs are here");

  // migrate curators of portal
  var beta_curators = beta_db.curator.find().forEach(function(beta_curator) {
    if (beta_curator) {
      prod_db.curator.remove({"email": beta_curator.email});
      curator = {};
      curator['_id'] = beta_curator._id;  // this is aaiId

      if(beta_curator.email) {
        curator['email'] = beta_curator.email;
      } else {
        curator['email'] = "";
      }

      if(beta_curator.name) {
        curator['name'] = beta_curator.name;
      } else {
        curator['name'] = "";
      }

      if(beta_curator.affiliations) {
        curator['affiliations'] = beta_curator.affiliations;
      } else {
        curator['affiliations'] = [];
      }

      if(beta_curator.photo) {
        curator['photo'] = beta_curator.photo;
      } else {
        curator['photo'] = "";
      }

      if(beta_curator.bio) {
        curator['bio'] = beta_curator.bio;
      } else {
        curator['bio'] = "";
      }
      // prod_db.curator.insert({"email": beta_curator.email,"name": beta_curator.name, "affiliations": beta_curator.affiliations, "photo": beta_curator.photo, "bio": beta_curator.bio});
      prod_db.curator.insert(curator);

      print("curator "+beta_curator.name + " is migrated");
    }
  });
}

// migrate_portal("aginfra", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("rural-digital-europe", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("clarin", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("enermaps", 'openaireconnect_new', 'openaireconnect');
// // migrate_portal("ifremer", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("instruct", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("risis", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("rda", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("science-innovation-policy", 'openaireconnect_new', 'openaireconnect');
// // migrate_portal("sobigdata", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("sdsn-gr", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("elixir-gr", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("beopen", 'openaireconnect_new', 'openaireconnect');
// // migrate_portal("euromarine", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("oa-pg", 'openaireconnect_new', 'openaireconnect');
//
//
// migrate_portal("covid-19", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("dariah", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("dh-ch", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("egi", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("mes", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("epos", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("fam", 'openaireconnect_new', 'openaireconnect');
// migrate_portal("ni", 'openaireconnect_new', 'openaireconnect');
//

migrateCurators("openaireconnect_new", "openaireconnect");
