//version compatibility: 2.0.0-SNAPSHOT

function removeStatisticsForDeletedCommunity(pid) {
  var countStatistics = db.statistics.find({pid: pid}).count();
  if(countStatistics > 0) {
    db.statistics.remove({pid: pid});
  }
  print("Removed " + countStatistics + " statistics entries for community: " + pid);
}

function removeSubscribersForDeletedCommunity(pid) {
  var countSubscribers = db.communitySubscribers.find({pid: pid}).count();
  if(countSubscribers > 0) {
    db.communitySubscribers.remove({pid: pid});
  }
  print("Removed " + countSubscribers + " communitySubscribers entries for community: " + pid);
}

use openaire_admin;


// 08-10-2019
removeStatisticsForDeletedCommunity("Transport Research");
removeSubscribersForDeletedCommunity("Transport Research");

removeStatisticsForDeletedCommunity("European Plate Observing System");
removeSubscribersForDeletedCommunity("European Plate Observing System");